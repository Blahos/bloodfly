
//#define USE_BMP180

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include "BMP180.h"

// 10 must be set for output
RF24 radio(9, 10); // CE, CSN

// something for the radio
// address for control to plane communication
const byte RF_ADDRESS_C2P[6] = "A_C2P";
// address for plane to control communication
const byte RF_ADDRESS_P2C[6] = "A_P2C";

unsigned long lastLoopTime = millis();

float ailerons = 0;
float elevator = 0;
float rudder = 0;
float throttle = 0;

// flags to trigger trim in the airplane, 0 means no trimming
byte aileronLeftTrim = 0;
byte aileronRightTrim = 0;
byte elevatorTrim = 0;
byte rudderTrim = 0;

byte aileronLeftAngle = 30;
byte aileronRightAngle = 30;
byte elevatorAngle = 30;
byte rudderAngle = 30;

byte signalByteAlpha = 0;

// flag to signal whether we should send updated values for max angles of control surfaces to the plane
bool updateControlAngles = false;
unsigned long lastUpdateControlAnglesTime = 0;

// flap mode offset (offset both ailerons in the same direction by given amount to create a flap effect (flaperon)
short flapOffset = 0;

// one byte for message type and then we do max of all possible message sizes
const int radioMessageSize = sizeof(byte) + 
  max(
    4 * sizeof(float) + 4 * sizeof(byte) + 1 * sizeof(short) + 1 * sizeof(byte),
    5 * sizeof(byte));

#define MESSAGE_TYPE_MAIN ((byte)1)
#define MESSAGE_TYPE_ANGLES ((byte)2)
    
byte radioBuffer[radioMessageSize];

#define MESSAGE_BACK_TYPE_MAIN ((byte)1)
#define MESSAGE_BACK_TYPE_GPS ((byte)2)
#define MESSAGE_BACK_TYPE_SLOW_INFO ((byte)3)

// message to send back to the control
// first byte will indicate message type (we will have "fast" messages and slow messages for instance (different frequency of sending))
// 1 byte for message type
// main message structure:
//  floats - pitch, roll, pressure0, pressure
//  shorts - battery voltage, loop frequency, temperature
const int radioMessageBackType1Size = 4 * sizeof(float) + 3 * sizeof(short);
// GPS message structure
//  floats - latitude, longitude, speed, course, altitude
const int radioMessageBackType2Size = 5 * sizeof(float);
// slow info message structure
//  floats - throttle minutes
const int radioMessageBackType3Size = 1 * sizeof(float);

const int radioMessageBackSize = 1 * sizeof(byte) + max(max(radioMessageBackType1Size, radioMessageBackType2Size), radioMessageBackType3Size);
byte radioBufferBack[radioMessageBackSize];

// values for boundary checks on messages coming from serial port
#define SERIAL_CHECK1 255
#define SERIAL_CHECK2 0
#define SERIAL_CHECK3 127
#define SERIAL_CHECK4 129
#define SERIAL_CHECK5 0
#define SERIAL_CHECK6 255

#define SERIAL_MESSAGE_SIZE (6 * sizeof(byte) + 4 * sizeof(float) + 4 * sizeof(byte) + 1 * sizeof(short) + 1 * sizeof(byte) + 4 * sizeof(byte) + sizeof(byte) + sizeof(byte) + sizeof(byte))
byte serialInputBuffer[SERIAL_MESSAGE_SIZE];
byte serialInputBuffer2[SERIAL_MESSAGE_SIZE];
int nextSerialMessageIndex = 0;

unsigned long lastValidSerialInput = millis();
int serialInactivityPeriod = 400; // if serial is inactive for longer than this, switch control to local hardware or none

// loop frequency counter
int loopFreqCounter = 0;
unsigned long lastLoopFreqReset = 0;
const int loopFreqMeasurePeriodMS = 2000;

byte enablePlaneFeedback = 0;

void initRadio()
{
  radio.begin();
  radio.failureDetected = false;
  radio.setAutoAck(false);
  radio.setChannel(79);
  //radio.closeReadingPipe(1);
  radio.stopListening();
  radio.openWritingPipe(RF_ADDRESS_C2P);   //Setting the address at which we will send the data
  radio.openReadingPipe(1, RF_ADDRESS_P2C);   //Setting the address at which we will receive the data
  radio.setPALevel(0);       //You can set this as minimum or maximum depending on the distance between the transmitter and receiver.
  //radio.powerUp();
  radio.setRetries(0, 0);
  radio.setDataRate(RF24_250KBPS);
  //radio.startListening();              //This sets the module as receiver

  Serial.println("Finished init");
}

void setup()
{
  Serial.begin(38400);
  Serial.println("Setup ...");
  initRadio();

  InitBMP180();

  lastLoopFreqReset = millis();
  Serial.println("Setup ... done");
}

void ProcessSerialInput()
{
  const int lCheckOffset = 3 * sizeof(byte);
  int lMessagesReadCount = 0;
  
  while (Serial.available() > 0)
  {
    const int lVal = Serial.read();

    if (lVal >= 0)
    {
      const byte lValByte = (byte)lVal;

      bool lMessageBroken = false;

      if (nextSerialMessageIndex == 0 && lValByte != SERIAL_CHECK1) lMessageBroken = true;
      else if (nextSerialMessageIndex == 1 && lValByte != SERIAL_CHECK2) lMessageBroken = true;
      else if (nextSerialMessageIndex == 2 && lValByte != SERIAL_CHECK3) lMessageBroken = true;
      else if (nextSerialMessageIndex == SERIAL_MESSAGE_SIZE - 3 && lValByte != SERIAL_CHECK4) lMessageBroken = true;
      else if (nextSerialMessageIndex == SERIAL_MESSAGE_SIZE - 2 && lValByte != SERIAL_CHECK5) lMessageBroken = true;
      else if (nextSerialMessageIndex == SERIAL_MESSAGE_SIZE - 1 && lValByte != SERIAL_CHECK6) lMessageBroken = true;

      if (!lMessageBroken)
      {
        serialInputBuffer[nextSerialMessageIndex] = lValByte;
        nextSerialMessageIndex++;
        
        if (nextSerialMessageIndex == SERIAL_MESSAGE_SIZE)
        {
          nextSerialMessageIndex = 0;
          memcpy(serialInputBuffer2, serialInputBuffer, SERIAL_MESSAGE_SIZE);
          lMessagesReadCount++;

          const int lTrimStartIndex = lCheckOffset + 4 * sizeof(float);

          // remember last nonzero trim values (out of all messages that we accept this cycle
          if (serialInputBuffer2[lTrimStartIndex + 0 * sizeof(byte)] != 0)
            elevatorTrim = serialInputBuffer2[lTrimStartIndex + 0 * sizeof(byte)];
            
          if (serialInputBuffer2[lTrimStartIndex + 1 * sizeof(byte)] != 0)
            rudderTrim = serialInputBuffer2[lTrimStartIndex + 1 * sizeof(byte)];
            
          if (serialInputBuffer2[lTrimStartIndex + 2 * sizeof(byte)] != 0)
            aileronLeftTrim = serialInputBuffer2[lTrimStartIndex + 2 * sizeof(byte)];
            
          if (serialInputBuffer2[lTrimStartIndex + 3 * sizeof(byte)] != 0)
            aileronRightTrim = serialInputBuffer2[lTrimStartIndex + 3 * sizeof(byte)];
        }
      }
      else
      {
        if (lValByte == SERIAL_CHECK1)
        {
          serialInputBuffer[0] = lValByte;
          nextSerialMessageIndex = 1;
        }
        else
        {
          nextSerialMessageIndex = 0;
        }
      }
    }
  }

  if (lMessagesReadCount > 0)
  {    
    memcpy(&elevator, serialInputBuffer2 + lCheckOffset + 0 * sizeof(float), sizeof(float));
    memcpy(&rudder,   serialInputBuffer2 + lCheckOffset + 1 * sizeof(float), sizeof(float));
    memcpy(&ailerons, serialInputBuffer2 + lCheckOffset + 2 * sizeof(float), sizeof(float));
    memcpy(&throttle, serialInputBuffer2 + lCheckOffset + 3 * sizeof(float), sizeof(float));

    // flap offset
    memcpy(&flapOffset, serialInputBuffer2 + lCheckOffset + 4 * sizeof(float) + 4 * sizeof(byte), sizeof(short));

    // enable plane feedback
    enablePlaneFeedback = serialInputBuffer2[lCheckOffset + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short)];

    // control surfaces max angles
    byte lTmpByte;
    lTmpByte = serialInputBuffer2[lCheckOffset + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short) + sizeof(byte) + 0 * sizeof(byte)];
    if (lTmpByte != aileronLeftAngle)
    {
      aileronLeftAngle = lTmpByte;
      updateControlAngles = true;
    }
    
    lTmpByte = serialInputBuffer2[lCheckOffset + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short) + sizeof(byte) + 1 * sizeof(byte)];
    if (lTmpByte != aileronRightAngle)
    {
      aileronRightAngle = lTmpByte;
      updateControlAngles = true;
    }
    
    lTmpByte = serialInputBuffer2[lCheckOffset + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short) + sizeof(byte) + 2 * sizeof(byte)];
    if (lTmpByte != elevatorAngle)
    {
      elevatorAngle = lTmpByte;
      updateControlAngles = true;
    }
    
    lTmpByte = serialInputBuffer2[lCheckOffset + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short) + sizeof(byte) + 3 * sizeof(byte)];
    if (lTmpByte != rudderAngle)
    {
      rudderAngle = lTmpByte;
      updateControlAngles = true;
    }
    
    const byte lRadioPALevelTmp = serialInputBuffer2[lCheckOffset + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short) + sizeof(byte) + 4 * sizeof(byte)];
    rf24_pa_dbm_e lRadioPALevelEnumVal = RF24_PA_MIN;
    if (lRadioPALevelTmp == 1) lRadioPALevelEnumVal = RF24_PA_LOW;
    if (lRadioPALevelTmp == 2) lRadioPALevelEnumVal = RF24_PA_HIGH;
    if (lRadioPALevelTmp == 3) lRadioPALevelEnumVal = RF24_PA_MAX;
    
    if (lRadioPALevelEnumVal != radio.getPALevel())
    {
      radio.setPALevel(lRadioPALevelEnumVal);
    }
    
    const byte lChannelNumberTmp = serialInputBuffer2[lCheckOffset + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short) + sizeof(byte) + 4 * sizeof(byte) + sizeof(byte)];
    
    if (lChannelNumberTmp != radio.getChannel() && lChannelNumberTmp >= 0 && lChannelNumberTmp < 126)
    {
      radio.setChannel(lChannelNumberTmp);
    }

    signalByteAlpha = serialInputBuffer2[lCheckOffset + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short) + sizeof(byte) + 4 * sizeof(byte) + sizeof(byte) + sizeof(byte)];
    
    lastValidSerialInput = millis();

    for (int i = 0; i < lMessagesReadCount; i++)
      Serial.print("!");
  }
}

void loop()
{
  aileronLeftTrim = 0;
  aileronRightTrim = 0;
  elevatorTrim = 0;
  rudderTrim = 0;
  
  ProcessSerialInput();

  unsigned long lCurrentTime = millis();
  
  UpdateBMP180(lCurrentTime);
  
  if (lCurrentTime - lastValidSerialInput > serialInactivityPeriod)
  {
    ailerons = 0;
    elevator = 0;
    rudder   = 0;
    throttle = 0;
  }

  if ((updateControlAngles || (signalByteAlpha != 0) || millis() - lastUpdateControlAnglesTime > 500) && throttle == 0)
  {
    radioBuffer[0] = MESSAGE_TYPE_ANGLES;
  
    radioBuffer[sizeof(byte) + 0 * sizeof(byte)] = aileronLeftAngle;
    radioBuffer[sizeof(byte) + 1 * sizeof(byte)] = aileronRightAngle;
    radioBuffer[sizeof(byte) + 2 * sizeof(byte)] = elevatorAngle;
    radioBuffer[sizeof(byte) + 3 * sizeof(byte)] = rudderAngle;
    radioBuffer[sizeof(byte) + 4 * sizeof(byte)] = signalByteAlpha;

    updateControlAngles = false;
    signalByteAlpha = 0;
    
    lastUpdateControlAnglesTime = millis();
  }
  else
  {
    radioBuffer[0] = MESSAGE_TYPE_MAIN;

    memcpy(radioBuffer + sizeof(byte) + 0 * sizeof(float), &elevator, sizeof(float));
    memcpy(radioBuffer + sizeof(byte) + 1 * sizeof(float), &rudder,   sizeof(float));
    memcpy(radioBuffer + sizeof(byte) + 2 * sizeof(float), &ailerons, sizeof(float));
    memcpy(radioBuffer + sizeof(byte) + 3 * sizeof(float), &throttle, sizeof(float));
  
    // trim values
    radioBuffer[sizeof(byte) + 4 * sizeof(float) + 0 * sizeof(byte)] = elevatorTrim;
    radioBuffer[sizeof(byte) + 4 * sizeof(float) + 1 * sizeof(byte)] = rudderTrim;
    radioBuffer[sizeof(byte) + 4 * sizeof(float) + 2 * sizeof(byte)] = aileronLeftTrim;
    radioBuffer[sizeof(byte) + 4 * sizeof(float) + 3 * sizeof(byte)] = aileronRightTrim;
  
    // flap offset value
    memcpy(radioBuffer + sizeof(byte) + 4 * sizeof(float) + 4 * sizeof(byte), &flapOffset, sizeof(short));
  
    // enable plane feedback
    radioBuffer[sizeof(byte) + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short)] = enablePlaneFeedback;
  
    if (elevatorTrim == 127)
      Serial.println("Elevator trim down");
    if (elevatorTrim == 255)
      Serial.println("Elevator trim up");
    if (rudderTrim == 127)
      Serial.println("Rudder trim left");
    if (rudderTrim == 255)
      Serial.println("Elevator trim right");
    if (aileronLeftTrim == 127)
      Serial.println("Aileron left trim down");
    if (aileronLeftTrim == 255)
      Serial.println("Aileron left trim up");
    if (aileronRightTrim == 127)
      Serial.println("Aileron right trim down");
    if (aileronRightTrim == 255)
      Serial.println("Aileron right trim up");
  }

  if (radio.failureDetected)
  {
    Serial.print("?");
    initRadio();
  }

  delay(7);
  radio.stopListening();

  //radio.flush_tx();
  radio.startWrite(&radioBuffer, radioMessageSize, false);
  
  if (enablePlaneFeedback != 0)
  {
    delay(7);
    radio.startListening();
    
    int readCounter = 0;
    
    while(readCounter <= 3 && radio.available())
    {
      readCounter++;
      
      radio.read(&radioBufferBack, radioMessageBackSize);
  
      Serial.print(":");

      const byte messageBackType = radioBufferBack[0];

      if (messageBackType == MESSAGE_BACK_TYPE_MAIN)
      {      
        float pitch, roll;
        memcpy(&pitch, radioBufferBack + sizeof(byte) + 0 * sizeof(float), sizeof(float));
        memcpy(&roll, radioBufferBack + sizeof(byte) + 1 * sizeof(float), sizeof(float));
        Serial.print("<PR;");
        Serial.print(pitch, 4);
        Serial.print(";");
        Serial.print(roll, 4);
        Serial.print(">");
    
        float pressure0, pressure;
        memcpy(&pressure0, radioBufferBack + sizeof(byte) + 2 * sizeof(float), sizeof(float));
        memcpy(&pressure, radioBufferBack + sizeof(byte) + 3 * sizeof(float), sizeof(float));
    
        bool usedLocal0Pressure = false;
               
        Serial.print("<P0;");
#ifdef USE_BMP180
        if (BMP180Present)
        {
          Serial.print(level0Pressure, 4);
          usedLocal0Pressure = true;
        }
#endif
        if (!usedLocal0Pressure)
        {
          Serial.print(pressure0, 4);
        }
        Serial.print(">");
        
        Serial.print("<P;");
        Serial.print(pressure, 4);
        Serial.print(">");
    
        short batteryVoltage = -1;
        memcpy(&batteryVoltage, radioBufferBack + sizeof(byte) + 4 * sizeof(float), sizeof(short));
        Serial.print("<B;");
        Serial.print(batteryVoltage);
        Serial.print(">");
        
        short planeArduinoLoopFreq;
        memcpy(&planeArduinoLoopFreq, radioBufferBack + sizeof(byte) + 4 * sizeof(float) + 1 * sizeof(short), sizeof(short));
        
        Serial.print("<PALF;");
        Serial.print(planeArduinoLoopFreq);
        Serial.print(">");
    
        short temperature;
        memcpy(&temperature, radioBufferBack + sizeof(byte) + 4 * sizeof(float) + 2 * sizeof(short), sizeof(short));
        
        Serial.print("<T;");
        Serial.print((float)temperature / 100, 2);
        Serial.print(">");
      }
      else if (messageBackType == MESSAGE_BACK_TYPE_GPS)
      {
        float gpslatitude, gpslongitude, gpsspeed, gpscourse, gpsaltitude;
        
        memcpy(&gpslatitude,  radioBufferBack + sizeof(byte) + 0 * sizeof(float), sizeof(float));
        memcpy(&gpslongitude, radioBufferBack + sizeof(byte) + 1 * sizeof(float), sizeof(float));
        memcpy(&gpsspeed,     radioBufferBack + sizeof(byte) + 2 * sizeof(float), sizeof(float));
        memcpy(&gpscourse,    radioBufferBack + sizeof(byte) + 3 * sizeof(float), sizeof(float));
        memcpy(&gpsaltitude,  radioBufferBack + sizeof(byte) + 4 * sizeof(float), sizeof(float));
        
        Serial.print("<GPS;");
        Serial.print(gpslatitude, 7);
        Serial.print(";");
        Serial.print(gpslongitude, 7);
        Serial.print(";");
        Serial.print(gpsspeed, 2);
        Serial.print(";");
        Serial.print(gpscourse, 2);
        Serial.print(";");
        Serial.print(gpsaltitude, 1);
        Serial.print(">");
      }
      else if (messageBackType == MESSAGE_BACK_TYPE_SLOW_INFO)
      {
        float throttleMinutes;

        memcpy(&throttleMinutes, radioBufferBack + sizeof(byte) + 0 * sizeof(float), sizeof(float));

        Serial.print("<TM;");
        Serial.print(throttleMinutes, 3);
        Serial.print(">");
      }
      else Serial.println("UNRECOGNISED MESSAGE RECEIVED FROM PLANE!");
    }
  }

  loopFreqCounter++;

  unsigned long nowTime = millis();
  if (nowTime - lastLoopFreqReset >= loopFreqMeasurePeriodMS)
  {
    const int timeChange = nowTime - lastLoopFreqReset;
    const float lLoopFreq = 1000 * ((float)loopFreqCounter / timeChange);
    
    loopFreqCounter = 0;
    lastLoopFreqReset = nowTime;
    
    Serial.print("<LF;");
    Serial.print(lLoopFreq, 4);
    Serial.print(">");
  }
}
