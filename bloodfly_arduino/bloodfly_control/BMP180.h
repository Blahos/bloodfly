
#ifdef USE_BMP180
#include <Adafruit_BMP085.h>
#endif

// BMP280 data
// these variables will stay even if the sensor is not used, cause they are sent over radio
float level0Pressure = 0;
#ifdef USE_BMP180
const int BMP180_ADD = 0x77; // BMP180 address
bool BMP180Present = false;
unsigned long lastBMP180ReadTime = 0;
Adafruit_BMP085 bmp;
#endif

void InitBMP180()
{
#ifdef USE_BMP180
  if (!bmp.begin()) 
  {
    Serial.println("BMP180 not initialised properly!");
    return;
  }
  BMP180Present = true;
#endif
}

void UpdateBMP180(const unsigned long aCurrentTime)
{
#ifdef USE_BMP180
	if (aCurrentTime - lastBMP180ReadTime > 500 && BMP180Present)
	{
		level0Pressure = (float)bmp.readPressure();
    
		lastBMP180ReadTime = aCurrentTime;
	}
#endif
}
