
#include <Servo.h>


Servo motor;
#define motorPin A0

#define THROTTLE_MIN 1000
#define THROTTLE_MAX 2000

void setup() 
{
  motor.writeMicroseconds(THROTTLE_MAX);
  
  motor.attach(motorPin);
  
  motor.writeMicroseconds(THROTTLE_MAX);
  delay(3000);
  motor.writeMicroseconds(THROTTLE_MIN);
}

void loop() 
{
  motor.writeMicroseconds(THROTTLE_MIN);
}
