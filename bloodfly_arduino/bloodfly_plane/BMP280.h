
#ifdef USE_BMP280
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>
#endif

// BMP280 data
// these variables will stay even if the sensor is not used, cause they are sent over radio
float currentPressure = 0;
float level0Pressure = 0;
float currentTemperature = 0;
#ifdef USE_BMP280
const int BMP280_ADD = 0x76; // BMP280 address
bool BMP280Present = false;
unsigned long lastBMP280ReadTime = 0;
Adafruit_BMP280 bmp;
#endif

void InitBMP280()
{
#ifdef USE_BMP280
	Wire.beginTransmission(BMP280_ADD);
	byte lError = Wire.endTransmission();

	if (lError == 0)
  {
		BMP280Present = true;
  }
	else
	{
    //Serial.println("BMP280 ERROR: " + String(lError));
		return;
	}

	bmp.begin(BMP280_ADD);

	bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
		Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
		Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
		Adafruit_BMP280::FILTER_OFF,      /* Filtering. */
		Adafruit_BMP280::STANDBY_MS_1); /* Standby time. */

	const int sampleCount = 100;
	for (int i = 0; i < sampleCount; i++)
	{
		level0Pressure += bmp.readPressure();
		delay(100);
	}
	level0Pressure /= sampleCount;
  
  //Serial.println("BMP280 initialised");
#endif
}

void UpdateBMP280(const unsigned long aCurrentTime)
{
#ifdef USE_BMP280
	if (aCurrentTime - lastBMP280ReadTime > 250 && BMP280Present)
	{
		currentPressure = bmp.readPressure();
		currentTemperature = bmp.readTemperature();
		lastBMP280ReadTime = aCurrentTime;
	}
#endif
}
