
//#define USE_MPU6050
//#define USE_BMP280
//#define USE_GPS
//#define USE_VOLTAGE_METER
//#define WAVE
//#define DEBUG_TO_SERIAL

#define RADIO_CHANNEL  111

#include <Wire.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Servo.h>
#include <EEPROM.h>

#include "MPU6050.h"
#include "BMP280.h"
#include "GPS.h"

#define STATUS_LED_PIN 8
#define VOLTAGE_METER_PIN A7

// 10 must be set for output
RF24 radio(9, 10); // CE, CSN
// something for the radio
//const byte address[6] = "00001";
// address for control to plane communication
const byte RF_ADDRESS_C2P[6] = "A_C2P";
// address for plane to control communication
const byte RF_ADDRESS_P2C[6] = "A_P2C";

// EEPROM ADDRESSES
const int throttleMinutesEEPROMAddress = 0;
const int trimBytesStartAddress = 0 + 4;

// throttle minutes
float throttleMinutes = 0;
unsigned long long throttleMinutesLastEEPROMUpdate = 0;
unsigned long long throttleMinutesLastUpdate = 0;

#define SERVO_GLOBAL_MIN_ANGLE 10
#define SERVO_GLOBAL_MAX_ANGLE 170

// parameter wrap for SetServoAngle function
#define AILERON_LEFT_PARAMS aileronLeftServo, aileronLeftCentreAngle, aileronLeftTrim, aileronLeftRangeAngle, (double)flapOffset
#define AILERON_RIGHT_PARAMS aileronRightServo, aileronRightCentreAngle, aileronRightTrim, aileronRightRangeAngle, -(double)flapOffset
#define ELEVATOR_PARAMS elevatorServo, elevatorCentreAngle, elevatorTrim, elevatorRangeAngle, 0
#define RUDDER_PARAMS rudderServo, rudderCentreAngle, rudderTrim, rudderRangeAngle, 0

Servo aileronLeftServo;
Servo aileronRightServo;
Servo elevatorServo;
Servo rudderServo;

Servo motor;

const int aileronLeftServoPin = 5;
const int aileronRightServoPin = 3;
const int elevatorServoPin = 6;
const int rudderServoPin = A2;

#define motorPin A0
const int motorMicrosecondsMin = 1000;
const int motorMicrosecondsMax = 2000;

// angles that are considered to be neutral position for the servo
#define aileronLeftCentreAngle 90.0
#define aileronRightCentreAngle 90.0
#define elevatorCentreAngle 90.0
#define rudderCentreAngle 90.0

// max range for the servos
// the servo will go from c - r to c + r, c being centre angle, r being range
int aileronLeftRangeAngle = 30;
int aileronRightRangeAngle = 30;
int elevatorRangeAngle = 30;
int rudderRangeAngle = 30;

// trimming (offsets for centre values)
char aileronLeftTrim = 0;
char aileronRightTrim = 0;
char elevatorTrim = 0;
char rudderTrim = 0;

#define TRIM_STEP 2

unsigned long lastReceiveTime = 0;

float aileronLeft = 0;
float aileronRight = 0;
float elevator = 0;
float rudder = 0;
float throttle = 0;

// flap offset value
short flapOffset = 0;

// enable sending data from the plane back to the control
bool enablePlaneFeedback = false;

// one byte for message type and then we do max of all possible message sizes
const int radioMessageSize = sizeof(byte) + 
  max(
    4 * sizeof(float) + 4 * sizeof(byte) + 1 * sizeof(short) + 1 * sizeof(byte),
    5 * sizeof(byte));
    
#define MESSAGE_TYPE_MAIN ((byte)1)
#define MESSAGE_TYPE_ANGLES ((byte)2)
    
byte radioBuffer[radioMessageSize];

#define MESSAGE_BACK_TYPE_MAIN ((byte)1)
#define MESSAGE_BACK_TYPE_GPS ((byte)2)
#define MESSAGE_BACK_TYPE_SLOW_INFO ((byte)3)

// message to send back to the control
// first byte will indicate message type (we will have "fast" messages and slow messages for instance (different frequency of sending))
// 1 byte for message type
// main message structure:
//  floats - pitch, roll, pressure0, pressure
//  shorts - battery voltage, loop frequency, temperature
const int radioMessageBackType1Size = 4 * sizeof(float) + 3 * sizeof(short);
// GPS message structure
//  floats - latitude, longitude, speed, course, altitude
const int radioMessageBackType2Size = 5 * sizeof(float);
// slow info message structure
//  floats - throttle minutes
const int radioMessageBackType3Size = 1 * sizeof(float);

const int radioMessageBackSize = 1 * sizeof(byte) + max(max(radioMessageBackType1Size, radioMessageBackType2Size), radioMessageBackType3Size);;
byte radioBufferBack[radioMessageBackSize];

const int noSignalResetTime = (int)(0.200 * 1000);

// to indicate plane status with the on board led
// primarily to indicate whether the radio is working (receiving data from controller)
bool statusValue = false;
unsigned long lastStatusProcessingTime = 0;

// raw value read from the voltage meter pin
short batteryVoltage = -1;

unsigned long lastMessageType1ToControl = 0;
unsigned long lastMessageType2ToControl = 0;
unsigned long lastMessageType3ToControl = 0;

bool firstLoop = true;

// loop frequency counter
int loopFreqCounter = 0;
unsigned long lastLoopFreqReset = 0;
const int loopFreqMeasurePeriodMS = 4000;
short loopFrequency = 0;

void initRadio()
{
  radio.begin();
  radio.failureDetected = false;
  radio.setAutoAck(false);
  radio.setChannel(RADIO_CHANNEL);
  //radio.closeReadingPipe(0);
  radio.stopListening();
  radio.openWritingPipe(RF_ADDRESS_P2C);   //Setting the address at which we will send the data
  radio.openReadingPipe(1, RF_ADDRESS_C2P);   //Setting the address at which we will receive the data
  radio.setPALevel(RF24_PA_LOW);       //You can set this as minimum or maximum depending on the distance between the transmitter and receiver.
  //radio.powerUp();
  radio.setRetries(0, 0);
  radio.setDataRate(RF24_250KBPS);
  radio.startListening();              //This sets the module as receiver

  //Serial.println("Finished radio init");
}

void ProcessVoltageMeter()
{
#ifdef USE_VOLTAGE_METER
  batteryVoltage = analogRead(VOLTAGE_METER_PIN);
#endif
}

void ProcessRadio()
{
  const unsigned long currentTime = millis();

  if (radio.failureDetected)
  {
    //Serial.println("F");
    statusValue = false;
    initRadio();
  }
  if (radio.available())
  {
#ifdef DEBUG_TO_SERIAL
    Serial.println("Radio available");
#endif
    //Serial.println("R");
    statusValue = true;
    
    radio.read(&radioBuffer, radioMessageSize);

    
    //radio.flush_rx();

    const byte messageType = radioBuffer[0];
    
#ifdef DEBUG_TO_SERIAL
    Serial.println("Message type: " + String(messageType));
#endif

    if (messageType == MESSAGE_TYPE_MAIN)
    {
#ifdef DEBUG_TO_SERIAL
    Serial.println("Message received");
#endif
    
      lastReceiveTime = currentTime;
      
      float val1;
      float val2;
      float val3;
      float val4;
  
      memcpy(&val1, radioBuffer + sizeof(byte) + 0 * sizeof(float), sizeof(float));
      memcpy(&val2, radioBuffer + sizeof(byte) + 1 * sizeof(float), sizeof(float));
      memcpy(&val3, radioBuffer + sizeof(byte) + 2 * sizeof(float), sizeof(float));
      memcpy(&val4, radioBuffer + sizeof(byte) + 3 * sizeof(float), sizeof(float));
  
      elevator = -val1;
      rudder = -val2;
      aileronLeft = -val3;
      aileronRight = -val3;
      throttle = val4;
  
      // trim
      // elevator trim
      byte lTmpByte;
      lTmpByte = radioBuffer[sizeof(byte) + 4 * sizeof(float) + 0 * sizeof(byte)];
      if (lTmpByte == 127 && elevatorTrim <= 127 - TRIM_STEP)
      {
        elevatorTrim += TRIM_STEP;
      }
      if (lTmpByte == 255 && elevatorTrim >= -127 + TRIM_STEP)
      {
        elevatorTrim -= TRIM_STEP;
      }
      // rudder trim
      lTmpByte = radioBuffer[sizeof(byte) + 4 * sizeof(float) + 1 * sizeof(byte)];
      if (lTmpByte == 127 && rudderTrim <= 127 - TRIM_STEP)
      {
        rudderTrim += TRIM_STEP;
      }
      if (lTmpByte == 255 && rudderTrim >= -127 + TRIM_STEP)
      {
        rudderTrim -= TRIM_STEP;
      }
      // left aileron trim
      lTmpByte = radioBuffer[sizeof(byte) + 4 * sizeof(float) + 2 * sizeof(byte)];
      if (lTmpByte == 127 && aileronLeftTrim >= -127 + TRIM_STEP)
      {
        aileronLeftTrim -= TRIM_STEP;
      }
      if (lTmpByte == 255 && aileronLeftTrim <= 127 - TRIM_STEP)
      {
        aileronLeftTrim += TRIM_STEP;
      }
      // right aileron trim
      lTmpByte = radioBuffer[sizeof(byte) + 4 * sizeof(float) + 3 * sizeof(byte)];
      if (lTmpByte == 127 && aileronRightTrim <= 127 - TRIM_STEP)
      {
        aileronRightTrim += TRIM_STEP;
      }
      if (lTmpByte == 255 && aileronRightTrim >= -127 + TRIM_STEP)
      {
        aileronRightTrim -= TRIM_STEP;
      }
      
      // flap offset value
      memcpy(&flapOffset, radioBuffer + sizeof(byte) + 4 * sizeof(float) + 4 * sizeof(byte), sizeof(short));
  
      // enable plane feedback
      enablePlaneFeedback = (radioBuffer[sizeof(byte) + 4 * sizeof(float) + 4 * sizeof(byte) + sizeof(short)] != 0);

      //Serial.println("Enabled plane feedback received: " + String(enablePlaneFeedback));
    }
    else if (messageType == MESSAGE_TYPE_ANGLES)
    {
      lastReceiveTime = currentTime;

      aileronLeftRangeAngle   = radioBuffer[sizeof(byte) + 0 * sizeof(byte)];
      aileronRightRangeAngle  = radioBuffer[sizeof(byte) + 1 * sizeof(byte)];
      elevatorRangeAngle      = radioBuffer[sizeof(byte) + 2 * sizeof(byte)];
      rudderRangeAngle        = radioBuffer[sizeof(byte) + 3 * sizeof(byte)];
      const byte lSignalByteAlpha = radioBuffer[sizeof(byte) + 4 * sizeof(byte)];
      
      if (lSignalByteAlpha & (1 << 0))
      {
        // reset throttle minutes to 0
        throttleMinutes = 0.0;
        EEPROM.put(throttleMinutesEEPROMAddress, throttleMinutes);
      }

      if (lSignalByteAlpha & (1 << 1))
      {
        // save trim values to EEPROM
        EEPROM.put(trimBytesStartAddress + 0, elevatorTrim);
        EEPROM.put(trimBytesStartAddress + 1, rudderTrim);
        EEPROM.put(trimBytesStartAddress + 2, aileronLeftTrim);
        EEPROM.put(trimBytesStartAddress + 3, aileronRightTrim);
      }      

      if (aileronLeftRangeAngle <  0) aileronLeftRangeAngle = 0;
      if (aileronLeftRangeAngle > 90) aileronLeftRangeAngle = 0;
      
      if (aileronRightRangeAngle <  0) aileronRightRangeAngle = 0;
      if (aileronRightRangeAngle > 90) aileronRightRangeAngle = 0;
      
      if (elevatorRangeAngle <  0) elevatorRangeAngle = 0;
      if (elevatorRangeAngle > 90) elevatorRangeAngle = 0;
      
      if (rudderRangeAngle <  0) rudderRangeAngle = 0;
      if (rudderRangeAngle > 90) rudderRangeAngle = 0;
    }
    else
    {
      // unrecognised message type byte value
    }
  }
  
  if (currentTime - lastReceiveTime > noSignalResetTime)
  {
    //Serial.println("No signal");
    elevator = 0;
    rudder = 0;
    aileronLeft = 0;
    aileronRight = 0;
    throttle = 0;
    
    statusValue = false;
  }

  if (aileronLeft < -1.0) aileronLeft = -1.0;
  if (aileronLeft > 1.0) aileronLeft = 1.0;
  
  if (aileronRight < -1.0) aileronRight = -1.0;
  if (aileronRight > 1.0) aileronRight = 1.0;
  
  if (elevator < -1.0) elevator = -1.0;
  if (elevator > 1.0) elevator = 1.0;
  
  if (rudder < -1.0) rudder = -1.0;
  if (rudder > 1.0) rudder = 1.0;

  if (throttle < 0.0) throttle = 0.0;
  if (throttle > 1.0) throttle = 1.0;

  //Serial.println("Plane feedback: " + String(enablePlaneFeedback));
  // send feedback to the control
  if (enablePlaneFeedback)
  {
    int sentMessageType = -1;

    if (sentMessageType < 0 && currentTime - lastMessageType3ToControl > 1000)
    {
      radioBufferBack[0] = MESSAGE_BACK_TYPE_SLOW_INFO;
      
      memcpy(radioBufferBack + sizeof(byte) + 0 * sizeof(float), &throttleMinutes, sizeof(float));
      
      sentMessageType = (int)MESSAGE_BACK_TYPE_SLOW_INFO;
    }
    
#ifdef USE_GPS
    if (sentMessageType < 0 && currentTime - lastMessageType2ToControl > 300)
    {
      radioBufferBack[0] = MESSAGE_BACK_TYPE_GPS;
      
      memcpy(radioBufferBack + sizeof(byte) + 0 * sizeof(float), &GPSLatitude, sizeof(float));
      memcpy(radioBufferBack + sizeof(byte) + 1 * sizeof(float), &GPSLongitude, sizeof(float));
      memcpy(radioBufferBack + sizeof(byte) + 2 * sizeof(float), &GPSSpeed, sizeof(float));
      memcpy(radioBufferBack + sizeof(byte) + 3 * sizeof(float), &GPSCourse, sizeof(float));
      memcpy(radioBufferBack + sizeof(byte) + 4 * sizeof(float), &GPSAltitude, sizeof(float));
      
      sentMessageType = (int)MESSAGE_BACK_TYPE_GPS;
    }
#endif

    if (sentMessageType < 0 && currentTime - lastMessageType1ToControl > 50)
    {
      const short currentTemperatureShort = (short)(currentTemperature * 100);
      
      radioBufferBack[0] = MESSAGE_BACK_TYPE_MAIN;
      
      memcpy(radioBufferBack + sizeof(byte) + 0 * sizeof(float) + 0 * sizeof(short), &Pitch, sizeof(float));
      memcpy(radioBufferBack + sizeof(byte) + 1 * sizeof(float) + 0 * sizeof(short), &Roll, sizeof(float));
      memcpy(radioBufferBack + sizeof(byte) + 2 * sizeof(float) + 0 * sizeof(short), &level0Pressure, sizeof(float));
      memcpy(radioBufferBack + sizeof(byte) + 3 * sizeof(float) + 0 * sizeof(short), &currentPressure, sizeof(float));
      memcpy(radioBufferBack + sizeof(byte) + 4 * sizeof(float) + 0 * sizeof(short), &batteryVoltage, sizeof(short));
      memcpy(radioBufferBack + sizeof(byte) + 4 * sizeof(float) + 1 * sizeof(short), &loopFrequency, sizeof(short));
      memcpy(radioBufferBack + sizeof(byte) + 4 * sizeof(float) + 2 * sizeof(short), &currentTemperatureShort, sizeof(short));
      
      sentMessageType = (int)MESSAGE_BACK_TYPE_MAIN;
    }

    if (sentMessageType >= 0)
    {
      radio.stopListening();
      delay(8);
      
      // send data to the control
      //radio.flush_tx();
      radio.startWrite(&radioBufferBack, radioMessageBackSize, false);
      
      if (sentMessageType == MESSAGE_BACK_TYPE_MAIN) lastMessageType1ToControl = currentTime;
      if (sentMessageType == MESSAGE_BACK_TYPE_GPS) lastMessageType2ToControl = currentTime;
      if (sentMessageType == MESSAGE_BACK_TYPE_SLOW_INFO) lastMessageType3ToControl = currentTime;
    }
  }
  
  delay(8);
  radio.startListening();
}

void ProcessStatusLed()
{
  unsigned long lCurrentTime = millis();
  if (lCurrentTime - lastStatusProcessingTime > 100)
  {
    lastStatusProcessingTime = lCurrentTime;
    
    //Serial.println("Signal: " + String(statusValue));
    digitalWrite(STATUS_LED_PIN, statusValue ? HIGH : LOW);
  }
}

void SetServoAngle(const double aAngleNormalised, Servo& aServo, const double aCentre, const char aTrim, const double aRange, const double aOffset)
{
  int lFinalAngle = (int)round(aCentre + aTrim + aRange * aAngleNormalised + aOffset);
  
#ifdef DEBUG_TO_SERIAL
  //Serial.println("Setting angle to servo: " + String(lFinalAngle));
#endif
  
  if (lFinalAngle < SERVO_GLOBAL_MIN_ANGLE) lFinalAngle = SERVO_GLOBAL_MIN_ANGLE;
  if (lFinalAngle > SERVO_GLOBAL_MAX_ANGLE) lFinalAngle = SERVO_GLOBAL_MAX_ANGLE;

  aServo.write(lFinalAngle);
}

void Wave()
{
#ifdef WAVE
#ifdef DEBUG_TO_SERIAL
  Serial.println("Performing wave ... ");
#endif
  SetServoAngle(-1, AILERON_LEFT_PARAMS);
  SetServoAngle(1, AILERON_RIGHT_PARAMS);
  SetServoAngle(-1, ELEVATOR_PARAMS);
  delay(200);
  
  SetServoAngle(1, AILERON_LEFT_PARAMS);
  SetServoAngle(-1, AILERON_RIGHT_PARAMS);
  SetServoAngle(1, ELEVATOR_PARAMS);
  delay(200);
  
  SetServoAngle(-1, AILERON_LEFT_PARAMS);
  SetServoAngle(1, AILERON_RIGHT_PARAMS);
  SetServoAngle(-1, ELEVATOR_PARAMS);
  delay(200);
  
  SetServoAngle(1, AILERON_LEFT_PARAMS);
  SetServoAngle(-1, AILERON_RIGHT_PARAMS);
  SetServoAngle(1, ELEVATOR_PARAMS);
  delay(200);
  
  SetServoAngle(0, AILERON_LEFT_PARAMS);
  SetServoAngle(0, AILERON_RIGHT_PARAMS);
  SetServoAngle(0, ELEVATOR_PARAMS);
  delay(500);
  
  SetServoAngle(1, AILERON_LEFT_PARAMS);
  SetServoAngle(1, AILERON_RIGHT_PARAMS);
  SetServoAngle(1, RUDDER_PARAMS);
  delay(200);
  
  SetServoAngle(-1, AILERON_LEFT_PARAMS);
  SetServoAngle(-1, AILERON_RIGHT_PARAMS);
  SetServoAngle(-1, RUDDER_PARAMS);
  delay(200);
  
  SetServoAngle(1, AILERON_LEFT_PARAMS);
  SetServoAngle(1, AILERON_RIGHT_PARAMS);
  SetServoAngle(1, RUDDER_PARAMS);
  delay(200);
  
  SetServoAngle(-1, AILERON_LEFT_PARAMS);
  SetServoAngle(-1, AILERON_RIGHT_PARAMS);
  SetServoAngle(-1, RUDDER_PARAMS);
  delay(500);
  
  SetServoAngle(0, AILERON_LEFT_PARAMS);
  SetServoAngle(0, AILERON_RIGHT_PARAMS);
  SetServoAngle(0, RUDDER_PARAMS);
#ifdef DEBUG_TO_SERIAL
  Serial.println("Performing wave ... finished");
#endif
#endif
}

void UpdateThrottleMinutes()
{
  const unsigned long long lNow = millis();
  const unsigned long long timeDiff = lNow - throttleMinutesLastUpdate;
  
  if (timeDiff > 100)
  {
    // we consider throttle to be in range from 0 to 100 rather than from 0 to 1
    // the dividing factors are split to have 2 numbers roughly around 1 multiplied together, rather than one large and one small number
    throttleMinutes += ((float)timeDiff / 600) * (throttle * 10);
    throttleMinutesLastUpdate = lNow;
  }

  const unsigned long long timeDiff2 = lNow - throttleMinutesLastEEPROMUpdate;
  // only save the throttle minutes to eeprom when throttle is off, meaning the plane is probably not flying
  // this is to avoid unnecessary delays of the main loop while in the air
  if (timeDiff2 > 20 * 1000 && throttle <= 0.0)
  {
    EEPROM.put(throttleMinutesEEPROMAddress, throttleMinutes);
    throttleMinutesLastEEPROMUpdate = lNow;
  }
}

void setup()
{
  pinMode(A1, OUTPUT);
  analogWrite(A1, 255);

  
#ifdef DEBUG_TO_SERIAL
  Serial.begin(9600);
  Serial.println("setup started");
#endif

  initRadio();
  
  aileronLeftServo.write((int)round(aileronLeftCentreAngle));
  aileronRightServo.write((int)round(aileronRightCentreAngle));
  elevatorServo.write((int)round(elevatorCentreAngle));
  rudderServo.write((int)round(rudderCentreAngle));

  motor.writeMicroseconds(motorMicrosecondsMin);
  
  aileronLeftServo.attach(aileronLeftServoPin);
  aileronRightServo.attach(aileronRightServoPin);
  elevatorServo.attach(elevatorServoPin);
  rudderServo.attach(rudderServoPin);

  motor.attach(motorPin);
  
  aileronLeftServo.write((int)round(aileronLeftCentreAngle));
  aileronRightServo.write((int)round(aileronRightCentreAngle));
  elevatorServo.write((int)round(elevatorCentreAngle));
  rudderServo.write((int)round(rudderCentreAngle));

  motor.writeMicroseconds(motorMicrosecondsMin);
  
  statusValue = false;
  pinMode(STATUS_LED_PIN, OUTPUT);
  digitalWrite(STATUS_LED_PIN, statusValue ? HIGH : LOW);

  //analogReference(EXTERNAL);
  
  Wire.begin(); // Initialize comunication
  
  // wait for a while to give the plane time to be steady 
  // it can be shaken after connecting battery
  delay(3000);

  InitMPU();
  InitBMP280();
  InitGPS();

  // do the greetings procedure
  Wave();
  
  // load trim from EEPROM
  EEPROM.get(trimBytesStartAddress + 0, elevatorTrim);
  if (isnan(elevatorTrim))
  {
    elevatorTrim = 0;
    EEPROM.put(trimBytesStartAddress + 0, elevatorTrim);
  }
  
  EEPROM.get(trimBytesStartAddress + 1, rudderTrim);
  if (isnan(rudderTrim))
  {
    rudderTrim = 0;
    EEPROM.put(trimBytesStartAddress + 1, rudderTrim);
  }
  
  EEPROM.get(trimBytesStartAddress + 2, aileronLeftTrim);
  if (isnan(aileronLeftTrim))
  {
    aileronLeftTrim = 0;
    EEPROM.put(trimBytesStartAddress + 2, aileronLeftTrim);
  }

  EEPROM.get(trimBytesStartAddress + 3, aileronRightTrim);
  if (isnan(aileronRightTrim))
  {
    aileronRightTrim = 0;
    EEPROM.put(trimBytesStartAddress + 3, aileronRightTrim);
  }

  lastLoopFreqReset = millis();

  // read throttle minutes from EEPROM
  EEPROM.get(throttleMinutesEEPROMAddress, throttleMinutes);
  if (isnan(throttleMinutes))
  {
    throttleMinutes = 0.0;
    EEPROM.put(throttleMinutesEEPROMAddress, throttleMinutes);
  }
  throttleMinutesLastEEPROMUpdate = millis();
  throttleMinutesLastUpdate = throttleMinutesLastEEPROMUpdate;
}

void loop()
{
#ifdef DEBUG_TO_SERIAL
  //Serial.println("Main loop start");
#endif
  unsigned long lNow = millis();

  UpdateMPU6050(lNow, firstLoop);
  UpdateBMP280(lNow);
  UpdateGPS(lNow);
  UpdateThrottleMinutes();
  
  ProcessVoltageMeter();
  ProcessRadio();
  ProcessStatusLed();

  //Serial.println(aileronLeft);

  //Serial.println("Loop: " + String(aileronLeft));
  
  SetServoAngle(aileronLeft, AILERON_LEFT_PARAMS);
  SetServoAngle(aileronRight, AILERON_RIGHT_PARAMS);
  SetServoAngle(elevator, ELEVATOR_PARAMS);
  SetServoAngle(rudder, RUDDER_PARAMS);

  int lThrottle = (int)(motorMicrosecondsMin + throttle * (motorMicrosecondsMax - motorMicrosecondsMin));
  motor.writeMicroseconds(lThrottle);

  //Serial.println(String(aileronLeftAngle) + ", " + String(aileronRightAngle) + ", " + String(elevatorAngle) + ", " + String(lThrottle));
  firstLoop = false;

  loopFreqCounter++;

  lNow = millis();
  if (lNow - lastLoopFreqReset >= loopFreqMeasurePeriodMS)
  {
    const int timeChange = lNow - lastLoopFreqReset;
    loopFrequency = (short)(1000 * ((float)loopFreqCounter / timeChange));
    loopFreqCounter = 0;
    lastLoopFreqReset = lNow;
  }
}
