
#ifdef USE_GPS
#include <TinyGPS++.h>
#endif

// GPS module data
float GPSLatitude, GPSLongitude;
float GPSSpeed, GPSCourse, GPSAltitude;
#ifdef USE_GPS
const int GPSBaud = 9600;
TinyGPSPlus GPS;
unsigned long lastGPSReadTime = 0;
#endif

void InitGPS()
{
#ifdef USE_GPS
  Serial.begin(GPSBaud);
  //Serial.println("GPS initialised");
#endif
}

void UpdateGPS(const unsigned long aCurrentTime)
{
#ifdef USE_GPS
	if (aCurrentTime - lastGPSReadTime > 200)
	{
		while (Serial.available() > 0)
		{
			GPS.encode(Serial.read());
			if (GPS.location.isUpdated())
			{
				GPSLatitude = GPS.location.lat();
				GPSLongitude = GPS.location.lng();
        GPSSpeed = GPS.speed.mps();
        GPSCourse = GPS.course.deg();
        GPSAltitude = GPS.altitude.meters();
			}
		}
		lastGPSReadTime = aCurrentTime;
	}
#endif
}
