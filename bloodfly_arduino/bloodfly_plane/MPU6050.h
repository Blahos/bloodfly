
#ifdef USE_MPU6050
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#endif

// MPU6050 data
// these 2 will stay out of the ifdef clause cause they are sent to the control arduino
// so they will be used as 0s if the MPU unit is not present
float Pitch = 0, Roll = 0;
#ifdef USE_MPU6050
const int MPU = 0x68; // MPU6050 I2C address
bool MPUPresent = false;
float AccX, AccY, AccZ;
float GyroX, GyroY, GyroZ;
float GyroErrorX, GyroErrorY, GyroErrorZ;
float AccScale;
float GyroScale;
float AccPitch, AccRoll;
unsigned long currentGyroTime = 0, previousGyroTime = 0;
float elapsedGyroTime = 0;
unsigned long lastMPU6050UpdateTime = 0;
// value from 0 to 1, higher means stronger filtering
float AccLowPassCoeff = 0.05;
float GyroLowPassCoeff = 0.01;
#endif


void InitMPU()
{
#ifdef USE_MPU6050

  //Serial.println("Initialising MPU ... ");
	Wire.beginTransmission(MPU);
	byte lError = Wire.endTransmission();

	if (lError == 0)
  {
		MPUPresent = true;
  }
	else
  {
    //Serial.println("MPU ERROR: " + String(lError));
		return;
  }

	GyroScale = 65.5;
	AccScale = 8192.0;

	Wire.beginTransmission(MPU);       // Start communication with MPU6050 // MPU=0x68
	Wire.write(0x6B);                  // Talk to the register 6B
	Wire.write(0x00);                  // Make reset - place a 0 into the 6B register
	Wire.endTransmission(true);        //end the transmission

	Wire.beginTransmission(MPU);       // Start communication with MPU6050 // MPU=0x68
	Wire.write(0x1B);                  // Set gyro sensitivity
	Wire.write(((unsigned byte)1) << 3);
	Wire.endTransmission(true);        //end the transmission

	Wire.beginTransmission(MPU);       // Start communication with MPU6050 // MPU=0x68
	Wire.write(0x1C);                  // Set acc sensitivity
	Wire.write(((unsigned byte)1) << 3);
	Wire.endTransmission(true);        //end the transmission
  
  //Serial.println("Calibratin MPU");
  
  // calibrate gyroscope
  GyroErrorX = 0;
  GyroErrorY = 0;
  GyroErrorZ = 0;
  
	const int lLoopCount = 600;
	for (int i = 0; i < lLoopCount; i++)
	{
		Wire.beginTransmission(MPU);
		Wire.write(0x43);
		Wire.endTransmission(false);
		Wire.requestFrom(MPU, 6, true);
		GyroX = Wire.read() << 8 | Wire.read();
		GyroY = Wire.read() << 8 | Wire.read();
		GyroZ = Wire.read() << 8 | Wire.read();

		GyroErrorX = GyroErrorX + (GyroX / GyroScale);
		GyroErrorY = GyroErrorY + (GyroY / GyroScale);
		GyroErrorZ = GyroErrorZ + (GyroZ / GyroScale);
    
    delay(2);
	}
	//Divide the sum by loop count to get the error value
	GyroErrorX = GyroErrorX / lLoopCount;
	GyroErrorY = GyroErrorY / lLoopCount;
	GyroErrorZ = GyroErrorZ / lLoopCount;
 
  //Serial.println("MPU initialised");
#endif
}

void ReadAccAndGyroData()
{
#ifdef USE_MPU6050
	if (!MPUPresent) return;
	// === Read acceleromter data === //
	Wire.beginTransmission(MPU);
	Wire.write(0x3B); // Start with register 0x3B (ACCEL_XOUT_H)
	Wire.endTransmission(false);
	Wire.requestFrom(MPU, 6, true); // Read 6 registers total, each axis value is stored in 2 registers

	float accXRaw = (Wire.read() << 8 | Wire.read()) / AccScale; // X-axis value
	float accYRaw = (Wire.read() << 8 | Wire.read()) / AccScale; // Y-axis value
	float accZRaw = (Wire.read() << 8 | Wire.read()) / AccScale; // Z-axis value

  AccX = AccLowPassCoeff * AccX + (1 - AccLowPassCoeff) * accXRaw;
  AccY = AccLowPassCoeff * AccY + (1 - AccLowPassCoeff) * accYRaw;
  AccZ = AccLowPassCoeff * AccZ + (1 - AccLowPassCoeff) * accZRaw;
 
  //Serial.println("( " + String(AccX) + ", " + String(AccY) + ", " + String(AccZ) + " )");

	// === Read gyroscope data === //
	previousGyroTime = currentGyroTime;        // Previous time is stored before the actual time read
	currentGyroTime = millis();            // Current time actual time read
	elapsedGyroTime = (float)(currentGyroTime - previousGyroTime) / 1000.0f; // Divide by 1000 to get seconds
	Wire.beginTransmission(MPU);
	Wire.write(0x43); // Gyro data first register address 0x43
	Wire.endTransmission(false);
	Wire.requestFrom(MPU, 6, true); // Read 4 registers total, each axis value is stored in 2 registers

	float gyroXRaw = (Wire.read() << 8 | Wire.read()) / GyroScale;
	float gyroYRaw = (Wire.read() << 8 | Wire.read()) / GyroScale;
	float gyroZRaw = (Wire.read() << 8 | Wire.read()) / GyroScale;

  // Correct the outputs with the calculated error values
  gyroXRaw -= GyroErrorX;
  gyroYRaw -= GyroErrorY;
  gyroZRaw -= GyroErrorZ;
    
	GyroX = GyroLowPassCoeff * GyroX + (1 - GyroLowPassCoeff) * gyroXRaw;
	GyroY = GyroLowPassCoeff * GyroY + (1 - GyroLowPassCoeff) * gyroYRaw;
	GyroZ = GyroLowPassCoeff * GyroZ + (1 - GyroLowPassCoeff) * gyroZRaw;
#endif
}

void ComputePitchRollAcc()
{
#ifdef USE_MPU6050
	if (!MPUPresent) return;
	AccPitch = 0;
	AccRoll = 0;

	AccRoll = -180.0 / PI * atan2f(AccY, AccZ);
	AccPitch = 180.0 / PI * atan2f(AccX, sqrt(AccY * AccY + AccZ * AccZ));
#endif
}

void ComputePitchRollTotal(const bool aFirstLoop)
{
#ifdef USE_MPU6050
	if (!MPUPresent) return;
	if (aFirstLoop)
	{
		Pitch = AccPitch;
		Roll = AccRoll;
	}
	else
	{
		if (AccRoll > 90 && Roll < -90)
			AccRoll -= 360;

		if (AccRoll < -90 && Roll > 90)
			AccRoll += 360;

		Roll = 0.998 * (Roll - GyroX * elapsedGyroTime) + 0.002 * AccRoll;
		Pitch = 0.998 * (Pitch + (-cos(Roll / 180.0 * PI) * GyroY - sin(Roll / 180.0 * PI) * GyroZ) * elapsedGyroTime) + 0.002 * AccPitch;

		if (Pitch > 90)
		{
			Pitch = 180 - Pitch;
			Roll += 180;
		}
		if (Pitch < -90)
		{
			Pitch = -Pitch - 180;
			Roll += 180;
		}

		if (Roll < -180) Roll += 360;
		if (Roll > 180) Roll -= 360;
	}
#endif
}

void UpdateMPU6050(const unsigned long aCurrentTime, const bool aFirstLoop)
{
#ifdef USE_MPU6050
	if (aCurrentTime - lastMPU6050UpdateTime > 0 && MPUPresent)
	{
		ReadAccAndGyroData();
		ComputePitchRollAcc();
		ComputePitchRollTotal(aFirstLoop);

		lastMPU6050UpdateTime = aCurrentTime;
	}
#endif
}
