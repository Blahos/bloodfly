﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace BloodflyControlUI
{
    public enum CompassGaugeDataSource
    {
        MagnetometerHeading,
        GSPCourse
    }

    public class GPSData : BaseViewModel
    {
        private double mLatitude = Double.NaN;

        public double Latitude
        {
            get { return mLatitude; }
            set
            {
                if (value == mLatitude) return;
                mLatitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        private double mLongitude = Double.NaN;

        public double Longitude
        {
            get { return mLongitude; }
            set
            {
                if (value == mLongitude) return;
                mLongitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        private double mSpeed = Double.NaN;

        public double Speed
        {
            get { return mSpeed; }
            set
            {
                if (value == mSpeed) return;
                mSpeed = value;
                OnPropertyChanged(nameof(Speed));
            }
        }

        private double mCourse = Double.NaN;

        public double Course
        {
            get { return mCourse; }
            set
            {
                if (value == mCourse) return;
                mCourse = value;
                OnPropertyChanged(nameof(Course));
            }
        }

        private double mAltitude = Double.NaN;

        public double Altitude
        {
            get { return mAltitude; }
            set
            {
                if (value == mAltitude) return;
                mAltitude = value;
                OnPropertyChanged(nameof(Altitude));
            }
        }

        public void Invalidate()
        {
            Latitude = Double.NaN;
            Longitude = Double.NaN;
            Speed = Double.NaN;
            Course = Double.NaN;
            Altitude = Double.NaN;
        }
    }

    public class OrientationControlViewModel : BaseViewModel
    {
        private readonly Point3D cZeroPoint = new Point3D(0, 0, 0);
        private readonly Timer mKeyDownTimer = null;
        private HashSet<Key> mKeysDown = new HashSet<Key>();
        private bool disposing = false;

        public OrientationControlViewModel()
        {
            mKeyDownTimer = new Timer(KeyDownTimerCallbackFcn, null, 100, Timeout.Infinite);
            GPSData.PropertyChanged += GPSData_PropertyChanged;

            Task.Run(() =>
            {
                var lPlaneModelsData = ObjParser.Load("Models/plane5.obj");
                var lArrowModelsData = ObjParser.Load("Models/arrow.obj");

                Application.Current.Dispatcher.Invoke(() =>
                {
                    Lights.Add(new AmbientLight(Colors.DarkGray));
                    Lights.Add(new DirectionalLight(Colors.LightGray, new Vector3D(0, -1, 0)));

                    foreach (var nModelData in lPlaneModelsData)
                    {
                        var lTexMinX = nModelData.TextureCoordinates.Select(a => a.X).Min();
                        var lTexMaxX = nModelData.TextureCoordinates.Select(a => a.X).Max();
                        var lTexMinY = nModelData.TextureCoordinates.Select(a => a.Y).Min();
                        var lTexMaxY = nModelData.TextureCoordinates.Select(a => a.Y).Max();

                        var lTexRangeX = lTexMaxX - lTexMinX;
                        var lTexRangeY = lTexMaxY - lTexMinY;

                        var lModel = nModelData.CreateModel();
                        var lMaterials = new MaterialGroup();

                        var lBitmap = new BitmapImage(new Uri("pack://application:,,,/Images/plane3_texture.jpg"));
                        var lBrush = new ImageBrush(lBitmap)
                        {
                            // the source image has to be cropped because WPF rescales the UV texture coordinates
                            // from their min to max values (so min value becomes 0 and max value becomes 1)
                            Viewbox = new Rect(lTexMinX, lTexMinY, lTexRangeX, lTexRangeY),
                            ViewboxUnits = BrushMappingMode.RelativeToBoundingBox,
                            Stretch = Stretch.Fill
                        };
                        lMaterials.Children.Add(new DiffuseMaterial(lBrush));
                        //lMaterials.Children.Add(new SpecularMaterial(Brushes.White, 2));
                        lModel.Material = lMaterials;

                        PlaneModels.Add(lModel);
                    }

                    foreach (var nModelData in lArrowModelsData)
                    {
                        var lModel = nModelData.CreateModel();
                        var lMaterials = new MaterialGroup();
                        lMaterials.Children.Add(new DiffuseMaterial(new SolidColorBrush(Colors.Red)));
                        lModel.Material = lMaterials;
                        ArrowModels.Add(lModel);
                    }
                });
            });
        }

        private void GPSData_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(GPSData.Course))
            {
                OnPropertyChanged(nameof(CompassGaugeDirection));
            }

            if (e.PropertyName == nameof(GPSData.Latitude) || e.PropertyName == nameof(GPSData.Longitude) ||
                e.PropertyName == nameof(GPSData.Course))
            {
                OnPropertyChanged(nameof(ArrowDirection));
                OnPropertyChanged(nameof(ShowArrow));
            }
        }

        private ProjectionCamera mCameraBehind = new PerspectiveCamera
        {
            FieldOfView = 70,
            FarPlaneDistance = 20,
            NearPlaneDistance = 1,
            Position = new Point3D(0, 0, 6),
            LookDirection = new Vector3D(0, 0, -1)
        };

        //private ProjectionCamera mCameraBehind = new OrthographicCamera
        //{
        //	FarPlaneDistance = 40,
        //	NearPlaneDistance = 1,
        //	Position = new Point3D(0, 0, 8),
        //	LookDirection = new Vector3D(0, 0, -1),
        //	Width = 8
        //};
        public ProjectionCamera CameraBehind
        {
            get { return mCameraBehind; }
            set
            {
                if (value == mCameraBehind) return;
                mCameraBehind = value;
                OnPropertyChanged(nameof(FlashCapCamera));
            }
        }

        private ProjectionCamera mCameraSide = new PerspectiveCamera
        {
            FieldOfView = 70,
            FarPlaneDistance = 20,
            NearPlaneDistance = 1,
            Position = new Point3D(6, 0, 0),
            LookDirection = new Vector3D(-1, 0, 0)
        };

        //private ProjectionCamera mCameraSide = new OrthographicCamera
        //{
        //	FarPlaneDistance = 40,
        //	NearPlaneDistance = 1,
        //	Position = new Point3D(8, 0, 0),
        //	LookDirection = new Vector3D(-1, 0, 0),
        //	Width = 8
        //};
        public ProjectionCamera CameraSide
        {
            get { return mCameraSide; }
            set
            {
                if (value == mCameraSide) return;
                mCameraSide = value;
                OnPropertyChanged(nameof(FlashCapCamera));
            }
        }

        private Model3DCollection mPlaneModels = new Model3DCollection();

        public Model3DCollection PlaneModels
        {
            get { return mPlaneModels; }
            set
            {
                if (value == mPlaneModels) return;
                mPlaneModels = value;
                OnPropertyChanged(nameof(PlaneModels));
            }
        }

        private Model3DCollection mArrowModels = new Model3DCollection();

        public Model3DCollection ArrowModels
        {
            get { return mArrowModels; }
            set
            {
                if (value == mArrowModels) return;
                mArrowModels = value;
                OnPropertyChanged(nameof(ArrowModels));
            }
        }


        private Model3DCollection mLights = new Model3DCollection();

        public Model3DCollection Lights
        {
            get { return mLights; }
            set
            {
                if (value == mLights) return;
                mLights = value;
                OnPropertyChanged(nameof(Lights));
            }
        }


        private IPlaneControlValues mPlaneControlValues;

        public IPlaneControlValues PlaneControlValues
        {
            get { return mPlaneControlValues; }
            set
            {
                if (value == mPlaneControlValues) return;
                mPlaneControlValues = value;
                OnPropertyChanged(nameof(PlaneControlValues));
            }
        }

        private double mPitch = 0;

        public double Pitch
        {
            get { return mPitch; }
            set
            {
                if (value == mPitch) return;
                mPitch = value;
                OnPropertyChanged(nameof(Pitch));
            }
        }

        private double mRoll = 0;

        public double Roll
        {
            get { return mRoll; }
            set
            {
                if (value == mRoll) return;
                mRoll = value;
                OnPropertyChanged(nameof(Roll));
            }
        }

        private double mPitchRaw = 0;

        public double PitchRaw
        {
            get { return mPitchRaw; }
            set
            {
                if (value == mPitchRaw) return;
                mPitchRaw = value;
                OnPropertyChanged(nameof(PitchRaw));
            }
        }

        private double mRollRaw = 0;

        public double RollRaw
        {
            get { return mRollRaw; }
            set
            {
                if (value == mRollRaw) return;
                mRollRaw = value;
                OnPropertyChanged(nameof(RollRaw));
            }
        }

        private double mHeight = 0;

        public double Height
        {
            get { return mHeight; }
            set
            {
                if (value == mHeight) return;
                mHeight = value;
                OnPropertyChanged(nameof(Height));
            }
        }

        private double mHeightRaw;

        public double HeightRaw
        {
            get { return mHeightRaw; }
            set
            {
                if (value == mHeightRaw) return;
                mHeightRaw = value;
                OnPropertyChanged(nameof(HeightRaw));
            }
        }

        private double mTemperatureRaw;

        public double TemperatureRaw
        {
            get { return mTemperatureRaw; }
            set
            {
                if (value == mTemperatureRaw) return;
                mTemperatureRaw = value;
                OnPropertyChanged(nameof(TemperatureRaw));
            }
        }

        private double mPressureLevel0;

        public double PressureLevel0
        {
            get { return mPressureLevel0; }
            set
            {
                if (value == mPressureLevel0) return;
                mPressureLevel0 = value;
                OnPropertyChanged(nameof(PressureLevel0));
            }
        }

        private double mPressureRaw;

        public double PressureRaw
        {
            get { return mPressureRaw; }
            set
            {
                if (value == mPressureRaw) return;
                mPressureRaw = value;
                OnPropertyChanged(nameof(PressureRaw));
            }
        }

        private double mHeading;

        public double Heading
        {
            get { return mHeading; }
            set
            {
                if (value == mHeading) return;
                mHeading = value;
                OnPropertyChanged(nameof(Heading));
                OnPropertyChanged(nameof(CompassGaugeDirection));
                OnPropertyChanged(nameof(ArrowDirection));
            }
        }

        public double CompassGaugeDirection
        {
            get
            {
                if (mCompassGaugeDataSource == CompassGaugeDataSource.GSPCourse)
                {
                    if (Double.IsNaN(GPSData.Course))
                        return 0.0;

                    return GPSData.Course;
                }
                else if (mCompassGaugeDataSource == CompassGaugeDataSource.MagnetometerHeading)
                    return Heading;
                else throw new Exception("Invalid source selected!");
            }
        }

        public GPSData GPSData { get; }
            = new GPSData();

        private string mVideoDevice;

        public string VideoDevice
        {
            get { return mVideoDevice; }
            set
            {
                if (value == mVideoDevice) return;
                mVideoDevice = value;
                OnPropertyChanged(nameof(VideoDevice));
            }
        }

        private bool mShowVideo = true;

        public bool ShowVideo
        {
            get { return mShowVideo; }
            set
            {
                if (value == mShowVideo) return;
                mShowVideo = value;
                OnPropertyChanged(nameof(ShowVideo));
            }
        }

        private Stretch mVideoFill;

        public Stretch VideoFill
        {
            get { return mVideoFill; }
            set
            {
                if (value == mVideoFill) return;
                mVideoFill = value;
                OnPropertyChanged(nameof(VideoFill));
            }
        }

        private ImageSource mCameraImage;

        public ImageSource CameraImage
        {
            get { return mCameraImage; }
            set
            {
                if (value == mCameraImage) return;
                mCameraImage = value;
                OnPropertyChanged(nameof(CameraImage));
            }
        }


        private bool mHUDVisible = true;

        public bool HUDVisible
        {
            get { return mHUDVisible; }
            set
            {
                if (value == mHUDVisible) return;
                mHUDVisible = value;
                OnPropertyChanged(nameof(HUDVisible));
            }
        }

        public double ArrowDirection
        {
            get
            {
                if (Double.IsNaN(GPSData.Latitude) || Double.IsNaN(GPSData.Longitude) || Double.IsNaN(GPSData.Course))
                {
                    return Double.NaN;
                }

                if (NavigationPlan == NavigationPlan.Base ||
                    NavigationPlan == NavigationPlan.NextCheckpointOrBase)
                {
                    if (!Double.IsNaN(BaseLatitude) && !Double.IsNaN(BaseLongitude))
                    {
                        var lBearingToBase = GPSHelp.ComputeBearing(
                            GPSData.Latitude, GPSData.Longitude,
                            BaseLatitude, BaseLongitude);

                        var lReturnValue = CompassGaugeDirection - lBearingToBase;

                        while (lReturnValue < 0) lReturnValue += 360;
                        while (lReturnValue >= 360) lReturnValue -= 360;
                        return lReturnValue;
                    }
                }

                return Double.NaN;
            }
        }

        private double mBaseLatitude = double.NaN;

        public double BaseLatitude
        {
            get { return mBaseLatitude; }
            set
            {
                if (value == mBaseLatitude) return;
                mBaseLatitude = value;
                OnPropertyChanged(nameof(BaseLatitude));
                OnPropertyChanged(nameof(ArrowDirection));
            }
        }

        private double mBaseLongitude = double.NaN;

        public double BaseLongitude
        {
            get { return mBaseLongitude; }
            set
            {
                if (value == mBaseLongitude) return;
                mBaseLongitude = value;
                OnPropertyChanged(nameof(BaseLongitude));
                OnPropertyChanged(nameof(ArrowDirection));
            }
        }

        /// <summary>
        /// This is to be bound to a transform that shifts the arrow out of the camera view
        /// if it's supposed to be hidden.
        /// There is no direct way to hide a 3d model in wpf, so this is a workaround
        /// </summary>
        public double ShowArrow
        {
            get
            {
                var lHidden = -10000;
                var lShown = -0.5;

                if (Double.IsNaN(GPSData.Latitude) || Double.IsNaN(GPSData.Longitude))
                {
                    return lHidden;
                }

                if (NavigationPlan == NavigationPlan.None)
                {
                    return lHidden;
                }
                else if (NavigationPlan == NavigationPlan.NextCheckpoint)
                {
                    return lHidden;
                }
                else if (NavigationPlan == NavigationPlan.NextCheckpointOrBase ||
                         NavigationPlan == NavigationPlan.Base)
                {
                    if (Double.IsNaN(BaseLongitude) || Double.IsNaN(BaseLatitude))
                        return lHidden;
                }
                else throw new Exception("Invalid enum value!");

                return lShown;
            }
        }

        public List<CompassGaugeDataSource> CompassGaugeDataSourceOptions { get; } =
            Enum.GetValues(typeof(CompassGaugeDataSource)).Cast<CompassGaugeDataSource>().ToList();

        private CompassGaugeDataSource mCompassGaugeDataSource = CompassGaugeDataSource.GSPCourse;

        public CompassGaugeDataSource CompassGaugeDataSource
        {
            get { return mCompassGaugeDataSource; }
            set
            {
                if (value == mCompassGaugeDataSource) return;
                mCompassGaugeDataSource = value;
                OnPropertyChanged(nameof(CompassGaugeDataSource));
                OnPropertyChanged(nameof(CompassGaugeDirection));
            }
        }

        public List<NavigationPlan> NavigationPlanOptions { get; } =
            Enum.GetValues(typeof(NavigationPlan)).Cast<NavigationPlan>().ToList();


        private NavigationPlan mNavigationPlan = NavigationPlan.NextCheckpointOrBase;

        public NavigationPlan NavigationPlan
        {
            get { return mNavigationPlan; }
            set
            {
                if (value == mNavigationPlan) return;
                mNavigationPlan = value;
                OnPropertyChanged(nameof(NavigationPlan));
                OnPropertyChanged(nameof(ArrowDirection));
                OnPropertyChanged(nameof(ShowArrow));
            }
        }

        private void KeyDownTimerCallbackFcn(object aArg)
        {
            try
            {
                const double lVerticalAngleMax = 60 * Math.PI / 180;
                const double lVerticalAngleMin = -60 * Math.PI / 180;

                double lHorizontalAngleChange = 0;
                double lVerticalAngleChange = 0;

                HashSet<Key> lKeysDown = null;
                Application.Current.Dispatcher.Invoke(() => { lKeysDown = mKeysDown.ToHashSet(); });

                if (lKeysDown.Contains(Key.A))
                    lHorizontalAngleChange += 2;
                if (lKeysDown.Contains(Key.D))
                    lHorizontalAngleChange += -2;
                if (lKeysDown.Contains(Key.W))
                    lVerticalAngleChange += 2;
                if (lKeysDown.Contains(Key.S))
                    lVerticalAngleChange += -2;

                if (lHorizontalAngleChange != 0 || lVerticalAngleChange != 0)
                {
                    lHorizontalAngleChange *= Math.PI / 180;
                    lVerticalAngleChange *= Math.PI / 180;

                    Vector3D lDirToCamera = new Vector3D();

                    Application.Current.Dispatcher.Invoke(() => { lDirToCamera = CameraBehind.Position - cZeroPoint; });
                    var lHorizontalVector = new Vector3D(lDirToCamera.X, 0, lDirToCamera.Z);

                    var lNorm = lDirToCamera.Length;
                    var lHorizontalNorm = lHorizontalVector.Length;

                    var lHorizonalAngle = Math.Atan2(lDirToCamera.Z, lDirToCamera.X);
                    var lHorizontalAngleNew = lHorizonalAngle + lHorizontalAngleChange;

                    var lVerticalAngle = Math.Atan2(lDirToCamera.Y, lHorizontalNorm);
                    var lVerticalAngleNew = lVerticalAngle + lVerticalAngleChange;

                    if (lVerticalAngleNew >= lVerticalAngleMax)
                        lVerticalAngleNew = lVerticalAngleMax;
                    if (lVerticalAngleNew < lVerticalAngleMin)
                        lVerticalAngleNew = lVerticalAngleMin;

                    var lNewY = lNorm * Math.Sin(lVerticalAngleNew);
                    var lHorizonatlNormNew = Math.Sqrt(lNorm * lNorm - lNewY * lNewY);

                    var lNewX = lHorizonatlNormNew * Math.Cos(lHorizontalAngleNew);
                    var lNewZ = lHorizonatlNormNew * Math.Sin(lHorizontalAngleNew);

                    var lNewPosition = new Point3D(lNewX, lNewY, lNewZ);

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        CameraBehind.Position = lNewPosition;
                        CameraBehind.LookDirection = cZeroPoint - lNewPosition;
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"KeyDownTimerCallbackFcn error: {ex.Message}");
            }
            finally
            {
                lock (mKeyDownTimer)
                {
                    if (!disposing)
                    {
                        mKeyDownTimer.Change(20, Timeout.Infinite);
                    }
                }
            }
        }

        public void KeyDown(KeyEventArgs aKeyArgs)
        {
            mKeysDown.Add(aKeyArgs.Key);
        }

        public void KeyUp(KeyEventArgs aKeyArgs)
        {
            if (mKeysDown.Contains(aKeyArgs.Key))
                mKeysDown.Remove(aKeyArgs.Key);
        }

        public void WindowClosing()
        {
            lock (mKeyDownTimer)
            {
                disposing = true;
                mKeyDownTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }
        }
    }
}