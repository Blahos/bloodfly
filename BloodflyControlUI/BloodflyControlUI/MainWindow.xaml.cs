﻿using System.Windows;
using System.Windows.Controls;

namespace BloodflyControlUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (!mScrollViewer.IsEnabled)
            {
                mScrollViewer.ScrollToBottom();
            }
        }
    }
}