﻿using System;
using SharpDX.DirectInput;
using System.Threading;

namespace BloodflyControlUI
{
    public class JoystickWrapper : IDisposable
    {
        [Flags]
        public enum JoystickButtons : ushort
        {
            None = 0,
            One = 1 << 0,
            Two = 1 << 1,
            Three = 1 << 2,
            Four = 1 << 3,
            Five = 1 << 4,
            Six = 1 << 5,
            Seven = 1 << 6,
            Eight = 1 << 7,
            Nine = 1 << 8,
            Ten = 1 << 9,
            Eleven = 1 << 10,
            Twelve = 1 << 11,
            DUp = 1 << 12,
            DDown = 1 << 13,
            DLeft = 1 << 14,
            DRight = 1 << 15,
        }

        private Joystick mJoystick = null;
        private readonly DirectInput mDirectInput = new DirectInput();
        private readonly Timer mTimer = null;
        private bool mDispose = false;
        private bool[] mButtons = null;
        private JoystickButtons mButtonsDown = JoystickButtons.None;

        private readonly object mThreadLock = new object();
        private bool mIsConnected = false;

        public bool IsConnected
        {
            get
            {
                lock (mThreadLock)
                {
                    return mIsConnected;
                }
            }
        }

        public JoystickWrapper()
        {
            mTimer = new Timer(UpdateState, null, 0, Timeout.Infinite);
        }

        private void UpdateState(object aArg)
        {
            try
            {
                bool lFound = false;

                Guid lGuid = Guid.Empty;

                foreach (var nDevice in mDirectInput.GetDevices(SharpDX.DirectInput.DeviceType.Joystick,
                             SharpDX.DirectInput.DeviceEnumerationFlags.AttachedOnly))
                {
                    lFound = true;
                    lGuid = nDevice.InstanceGuid;
                    break;
                }

                lock (mThreadLock)
                {
                    if (lFound)
                    {
                        if (mJoystick == null)
                        {
                            mJoystick = new Joystick(mDirectInput, lGuid);
                            mJoystick.Acquire();
                            mIsConnected = true;

                            //if (mJoystick != null) Console.WriteLine("Joystick connected");
                        }
                    }
                    else
                    {
                        if (mJoystick != null)
                        {
                            //if (mJoystick != null) Console.WriteLine("Joystick disconnected");
                            mJoystick.Dispose();
                            mJoystick = null;
                            mIsConnected = false;
                        }
                    }
                }
            }
            catch (Exception lEx)
            {
                Console.WriteLine("Exception in UpdateState of JoystickWrapper! {0}", lEx.Message);
            }

            if (!mDispose)
                mTimer.Change(200, Timeout.Infinite);
        }

        public JoystickButtons GetButtons()
        {
            Joystick lJoystick = null;
            JoystickState lState = null;

            JoystickButtons lButtons = JoystickButtons.None;

            lock (mThreadLock)
            {
                lJoystick = mJoystick;
                if (lJoystick == null || lJoystick.IsDisposed) return lButtons;
                if (!mDirectInput.IsDeviceAttached(lJoystick.Information.InstanceGuid)) return lButtons;

                try
                {
                    lState = lJoystick.GetCurrentState();
                }
                catch (Exception lEx)
                {
                    Console.WriteLine("Error reading joystick state! {0}", lEx.Message);
                    lState = null;
                }

                if (lState == null) return lButtons;
            }

            if (mButtons == null || mButtons.Length != lState.Buttons.Length)
                mButtons = new bool[lState.Buttons.Length];

            Array.Copy(lState.Buttons, mButtons, lState.Buttons.Length);

            for (int i = 0; i < 12; i++)
            {
                if (mButtons[i])
                    lButtons |= (JoystickButtons)(1 << i);
            }

            if (lState.PointOfViewControllers.Length <= 0) return lButtons;
            var lDPadVal = lState.PointOfViewControllers[0];

            switch (lDPadVal)
            {
                case 0:
                    lButtons |= JoystickButtons.DUp;
                    break;
                case 4500:
                    lButtons |= JoystickButtons.DUp | JoystickButtons.DRight;
                    break;
                case 9000:
                    lButtons |= JoystickButtons.DRight;
                    break;
                case 13500:
                    lButtons |= JoystickButtons.DRight | JoystickButtons.DDown;
                    break;
                case 18000:
                    lButtons |= JoystickButtons.DDown;
                    break;
                case 22500:
                    lButtons |= JoystickButtons.DDown | JoystickButtons.DLeft;
                    break;
                case 27000:
                    lButtons |= JoystickButtons.DLeft;
                    break;
                case 31500:
                    lButtons |= JoystickButtons.DLeft | JoystickButtons.DUp;
                    break;
                default:
                    break;
            }

            return lButtons;
        }

        public JoystickButtons GetButtonsPressed(bool aUpdateButtons = true)
        {
            var lNewButtonsDown = GetButtons();
            var lReturnValue = lNewButtonsDown & ~mButtonsDown;
            if (aUpdateButtons)
                mButtonsDown = lNewButtonsDown;

            return lReturnValue;
        }

        public double GetAxisValue(JoystickAxes aAxis, bool aInvert = false, double aDeadband = 0.0)
        {
            double lReturnValue = 0;

            Joystick lJoystick = null;
            JoystickState lState = null;

            lock (mThreadLock)
            {
                lJoystick = mJoystick;
                if (lJoystick == null) return lReturnValue;

                try
                {
                    lState = lJoystick.GetCurrentState();
                }
                catch (Exception lEx)
                {
                    Console.WriteLine("Error reading joystick state! {0}", lEx.Message);
                    lState = null;
                }
            }

            if (lJoystick == null || lState == null) return lReturnValue;
            if (lJoystick.IsDisposed) return lReturnValue;
            if (!mDirectInput.IsDeviceAttached(lJoystick.Information.InstanceGuid)) return lReturnValue;

            switch (aAxis)
            {
                case JoystickAxes.StickX:
                    lReturnValue = (double)(lState.X - Int16.MaxValue) / Int16.MaxValue;
                    break;
                case JoystickAxes.StickY:
                    lReturnValue = (double)(lState.Y - Int16.MaxValue) / Int16.MaxValue;
                    break;
                case JoystickAxes.StickTwist:
                    lReturnValue = (double)(lState.RotationZ - Int16.MaxValue) / Int16.MaxValue;
                    break;
                case JoystickAxes.Cradle:
                    lReturnValue = (double)(lState.Sliders[0] - Int16.MaxValue) / Int16.MaxValue;
                    break;
                case JoystickAxes.ThrottleLever:
                    lReturnValue = 1.0 - (double)(lState.Z) / 2.0 / Int16.MaxValue;
                    break;
                default:
                    break;
            }

            if (lReturnValue > 1.0) lReturnValue = 1.0;

            if (aAxis != JoystickAxes.ThrottleLever)
            {
                if (lReturnValue < -1.0) lReturnValue = -1.0;
                if (aInvert) lReturnValue = -lReturnValue;
            }
            else
            {
                if (lReturnValue < 0.0) lReturnValue = 0.0;
                if (aInvert) lReturnValue = 1.0 - lReturnValue;
            }

            if (Math.Abs(lReturnValue) <= aDeadband) lReturnValue = 0;

            return lReturnValue;
        }

        public void Dispose()
        {
            mDispose = true;
            mTimer.Dispose();
        }
    }
}