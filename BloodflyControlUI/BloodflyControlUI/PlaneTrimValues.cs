﻿using LibraryWpf;
using System.Windows.Input;

namespace BloodflyControlUI
{
    public class PlaneTrimValues : BaseViewModel
    {
        public const byte cTrimLeftValue = 127;
        public const byte cTrimRightValue = 255;
        public const byte cTrimDownValue = cTrimLeftValue;
        public const byte cTrimUpValue = cTrimRightValue;

        private byte mElevatorTrim;

        public byte ElevatorTrim
        {
            get { return mElevatorTrim; }
            set
            {
                if (value == mElevatorTrim) return;
                mElevatorTrim = value;
                OnPropertyChanged(nameof(ElevatorTrim));
            }
        }

        private byte mRudderTrim;

        public byte RudderTrim
        {
            get { return mRudderTrim; }
            set
            {
                if (value == mRudderTrim) return;
                mRudderTrim = value;
                OnPropertyChanged(nameof(RudderTrim));
            }
        }

        private byte mAileronLeftTrim;

        public byte AileronLeftTrim
        {
            get { return mAileronLeftTrim; }
            set
            {
                if (value == mAileronLeftTrim) return;
                mAileronLeftTrim = value;
                OnPropertyChanged(nameof(AileronLeftTrim));
            }
        }


        private byte mAileronRightTrim;

        public byte AileronRightTrim
        {
            get { return mAileronRightTrim; }
            set
            {
                if (value == mAileronRightTrim) return;
                mAileronRightTrim = value;
                OnPropertyChanged(nameof(AileronRightTrim));
            }
        }

        public void ResetTrims()
        {
            ElevatorTrim = 0;
            RudderTrim = 0;
            AileronLeftTrim = 0;
            AileronRightTrim = 0;
        }

        private ICommand mElevatorTrimDown;

        public ICommand ElevatorTrimDown
        {
            get
            {
                if (mElevatorTrimDown == null)
                {
                    mElevatorTrimDown = new RelayCommand(a => { ElevatorTrim = cTrimDownValue; });
                }

                return mElevatorTrimDown;
            }
        }

        private ICommand mElevatorTrimUp;

        public ICommand ElevatorTrimUp
        {
            get
            {
                if (mElevatorTrimUp == null)
                {
                    mElevatorTrimUp = new RelayCommand(a => { ElevatorTrim = cTrimUpValue; });
                }

                return mElevatorTrimUp;
            }
        }

        private ICommand mRudderTrimLeft;

        public ICommand RudderTrimLeft
        {
            get
            {
                if (mRudderTrimLeft == null)
                {
                    mRudderTrimLeft = new RelayCommand(a => { RudderTrim = cTrimLeftValue; });
                }

                return mRudderTrimLeft;
            }
        }

        private ICommand mRudderTrimRight;

        public ICommand RudderTrimRight
        {
            get
            {
                if (mRudderTrimRight == null)
                {
                    mRudderTrimRight = new RelayCommand(a => { RudderTrim = cTrimRightValue; });
                }

                return mRudderTrimRight;
            }
        }

        private ICommand mAileronLeftTrimDown;

        public ICommand AileronLeftTrimDown
        {
            get
            {
                if (mAileronLeftTrimDown == null)
                {
                    mAileronLeftTrimDown = new RelayCommand(a => { AileronLeftTrim = cTrimDownValue; });
                }

                return mAileronLeftTrimDown;
            }
        }

        private ICommand mAileronLeftTrimUp;

        public ICommand AileronLeftTrimUp
        {
            get
            {
                if (mAileronLeftTrimUp == null)
                {
                    mAileronLeftTrimUp = new RelayCommand(a => { AileronLeftTrim = cTrimUpValue; });
                }

                return mAileronLeftTrimUp;
            }
        }

        private ICommand mAileronRightTrimDown;

        public ICommand AileronRightTrimDown
        {
            get
            {
                if (mAileronRightTrimDown == null)
                {
                    mAileronRightTrimDown = new RelayCommand(a => { AileronRightTrim = cTrimDownValue; });
                }

                return mAileronRightTrimDown;
            }
        }

        private ICommand mAileronRightTrimUp;

        public ICommand AileronRightTrimUp
        {
            get
            {
                if (mAileronRightTrimUp == null)
                {
                    mAileronRightTrimUp = new RelayCommand(a => { AileronRightTrim = cTrimUpValue; });
                }

                return mAileronRightTrimUp;
            }
        }

        private ICommand mAileronsBothTrimLeft;

        public ICommand AileronsBothTrimLeft
        {
            get
            {
                if (mAileronsBothTrimLeft == null)
                {
                    mAileronsBothTrimLeft = new RelayCommand(a =>
                    {
                        AileronLeftTrimUp.Execute(null);
                        AileronRightTrimDown.Execute(null);
                    });
                }

                return mAileronsBothTrimLeft;
            }
        }

        private ICommand mAileronsBothTrimRight;

        public ICommand AileronsBothTrimRight
        {
            get
            {
                if (mAileronsBothTrimRight == null)
                {
                    mAileronsBothTrimRight = new RelayCommand(a =>
                    {
                        AileronLeftTrimDown.Execute(null);
                        AileronRightTrimUp.Execute(null);
                    });
                }

                return mAileronsBothTrimRight;
            }
        }
    }
}