﻿namespace BloodflyControlUI
{
    public enum JoystickAxes
    {
        ThrottleLever,
        StickX,
        StickY,
        StickTwist,
        Cradle
    }
}