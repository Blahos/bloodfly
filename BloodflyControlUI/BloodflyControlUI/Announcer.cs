﻿using System;
using System.Speech.Synthesis;

namespace BloodflyControlUI
{
    public class Announcer
    {
        private readonly SpeechSynthesizer mSynthesizer = null;

        public Announcer()
        {
            mSynthesizer = new SpeechSynthesizer();
            mSynthesizer.SetOutputToDefaultAudioDevice();

            var lVoices = mSynthesizer.GetInstalledVoices();

            if (lVoices.Count > 0)
            {
                var lName = lVoices[0].VoiceInfo.Name;

                foreach (var lVoice in lVoices)
                {
                    if (lVoice.VoiceInfo.Culture.ToString().ToLower().Contains("gb"))
                        lName = lVoice.VoiceInfo.Name;
                }

                mSynthesizer.SelectVoice(lName);
            }

            mSynthesizer.Rate = 2;
        }

        private void CancelAllRunningAnnouncements()
        {
            mSynthesizer.SpeakAsyncCancelAll();
        }

        private void AnnounceImmediate(string message)
        {
            CancelAllRunningAnnouncements();
            mSynthesizer.SpeakAsync(message);
        }

        private void AnnounceInQueue(string message)
        {
            mSynthesizer.SpeakAsync(message);
        }

        public void AnnounceLeftAileronTrim()
        {
            AnnounceImmediate("Left aileron trim");
        }

        public void AnnounceRightAileronTrim()
        {
            AnnounceImmediate("Right aileron trim");
        }

        public void AnnounceBothAileronsTrim()
        {
            AnnounceImmediate("Both ailerons trim");
        }

        public void AnnounceElevatorTrim()
        {
            AnnounceImmediate("Elevator trim");
        }

        public void AnnounceRudderTrim()
        {
            AnnounceImmediate("Rudder trim");
        }

        public void AnnounceBatteryLevel(double level)
        {
            AnnounceInQueue($"Battery {100 * level:F0} percent");
        }

        public void AnnounceTimeCheckpoint()
        {
            AnnounceImmediate("Time checkpoint");
        }

        public void AnnounceFlapsNeutral()
        {
            AnnounceImmediate("Flaps neutral");
        }

        public void AnnounceFlapsDown1()
        {
            AnnounceImmediate("Flaps down 1");
        }

        public void AnnounceFlapsDown2()
        {
            AnnounceImmediate("Flaps down 2");
        }

        public void AnnounceThrottle(double throttle)
        {
            AnnounceImmediate(((int)Math.Round(throttle) * 100).ToString());
        }

        public void AnnounceThrottleLockState(bool isThrottleLocked)
        {
            AnnounceImmediate(isThrottleLocked ? "Throttle locked" : "Throttle unlocked");
        }
    }
}