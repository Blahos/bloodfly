﻿namespace BloodflyControlUI
{
    public interface IPlaneControlValues
    {
        // current control values
        double Ailerons { get; }
        double Elevator { get; }
        double Rudder { get; }
        double Throttle { get; }

        // axes settings
        AxisControlSettings AileronsAxis { get; }
        AxisControlSettings ElevatorAxis { get; }
        AxisControlSettings RudderAxis { get; }
        AxisControlSettings ThrottleAxis { get; }

        // max angles
        int AileronLeftAngle { get; }
        int AileronRightAngle { get; }
        int ElevatorAngle { get; }
        int RudderAngle { get; }

        // flaperon modes
        bool FlapModeDown1 { get; }
        bool FlapModeDown2 { get; }
        bool FlapModeNeutral { get; }
        int FlapOffset { get; }
        int FlapOffsetDown1 { get; }
        int FlapOffsetDown2 { get; }
        int FlapOffsetNeutral { get; }

        // throttle lock
        bool ThrottleLocked { get; }
    }
}