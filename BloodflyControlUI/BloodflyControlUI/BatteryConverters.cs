﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace BloodflyControlUI
{
    public class BatteryToColourConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
            {
                var lValue = (double)value;

                if (lValue >= 0)
                {
                    if (lValue >= 0.3) return Brushes.Green;
                    else if (lValue >= 0.15) return Brushes.Orange;
                    else return Brushes.Red;
                }
                else return Brushes.Gray;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BatteryToBarScaleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
            {
                var lValue = (double)value;

                if (lValue >= 0)
                {
                    if (lValue <= 1.0) return lValue;
                    else return 1.0;
                }
                else
                {
                    return 0.1;
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BatteryToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
            {
                var lValue = (double)value;

                if (lValue >= 0)
                {
                    return lValue;
                }
                else return "unknown";
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}