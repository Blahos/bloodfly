﻿using SharpDX.XInput;
using System;

namespace BloodflyControlUI
{
    [Flags]
    public enum SquareStick
    {
        None = 0,
        Left = 1,
        Right = 2
    }

    public class GamepadWrapper(UserIndex aUserIndex = UserIndex.Any) : Controller(aUserIndex)
    {
        private GamepadButtonFlags mButtonsDown = GamepadButtonFlags.None;

        public GamepadButtonFlags GetButtonsPressed(bool aUpdateButtons = true)
        {
            var lNewButtonsDown = GetState().Gamepad.Buttons;
            var lReturnValue = lNewButtonsDown & ~mButtonsDown;
            if (aUpdateButtons)
                mButtonsDown = lNewButtonsDown;

            return lReturnValue;
        }

        public double GetAxisValue(GamepadAxes aAxis, bool aInvert = false, double aDeadband = 0.0,
            SquareStick aSquareStick = SquareStick.None)
        {
            double lReturnValue = 0;

            if (!IsConnected) return lReturnValue;

            switch (aAxis)
            {
                case GamepadAxes.LeftThumbX:
                    lReturnValue = (double)GetState().Gamepad.LeftThumbX / short.MaxValue;

                    if ((aSquareStick & SquareStick.Left) != SquareStick.None)
                    {
                        var lOtherValue = (double)GetState().Gamepad.LeftThumbY / short.MaxValue;
                        lReturnValue = SquareTheCircle(lReturnValue, lOtherValue);
                    }

                    break;
                case GamepadAxes.LeftThumbY:
                    lReturnValue = (double)GetState().Gamepad.LeftThumbY / short.MaxValue;

                    if ((aSquareStick & SquareStick.Left) != SquareStick.None)
                    {
                        var lOtherValue = (double)GetState().Gamepad.LeftThumbX / short.MaxValue;
                        lReturnValue = SquareTheCircle(lReturnValue, lOtherValue);
                    }

                    break;
                case GamepadAxes.RightThumbX:
                    lReturnValue = (double)GetState().Gamepad.RightThumbX / short.MaxValue;

                    if ((aSquareStick & SquareStick.Right) != SquareStick.None)
                    {
                        var lOtherValue = (double)GetState().Gamepad.RightThumbY / short.MaxValue;
                        lReturnValue = SquareTheCircle(lReturnValue, lOtherValue);
                    }

                    break;
                case GamepadAxes.RightThumbY:
                    lReturnValue = (double)GetState().Gamepad.RightThumbY / short.MaxValue;

                    if ((aSquareStick & SquareStick.Right) != SquareStick.None)
                    {
                        var lOtherValue = (double)GetState().Gamepad.RightThumbX / short.MaxValue;
                        lReturnValue = SquareTheCircle(lReturnValue, lOtherValue);
                    }

                    break;
                case GamepadAxes.Trigger:
                    lReturnValue = (double)(GetState().Gamepad.RightTrigger - GetState().Gamepad.LeftTrigger) /
                                   byte.MaxValue;
                    break;
                default:
                    break;
            }

            if (aInvert) lReturnValue = -lReturnValue;
            if (Math.Abs(lReturnValue) <= aDeadband) lReturnValue = 0;
            return lReturnValue;
        }

        private double SquareTheCircle(double aValue, double aOtherValue)
        {
            var lMagnitude = Math.Sqrt(aValue * aValue + aOtherValue * aOtherValue);
            if (Math.Abs(aValue) >= Math.Abs(aOtherValue)) return Math.Sign(aValue) * lMagnitude;
            else
            {
                return aValue / Math.Abs(aOtherValue);
            }
        }
    }
}