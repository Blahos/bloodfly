﻿using System.Collections.Generic;

namespace BloodflyControlUI
{
    public interface ICameraSelector
    {
        List<string> GetAvailableCameras();
        ICamera GetCamera(string id);
    }
}