﻿using System.Windows;
using System.Windows.Controls;

namespace BloodflyControlUI
{
    /// <summary>
    /// Interaction logic for AltimeterControl.xaml
    /// </summary>
    public partial class AltimeterControl : UserControl
    {
        public double HeightValue
        {
            get { return (double)GetValue(HeightValueProperty); }
            set { SetValue(HeightValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HeightValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HeightValueProperty =
            DependencyProperty.Register("HeightValue", typeof(double), typeof(AltimeterControl),
                new PropertyMetadata(0.0));

        public AltimeterControl()
        {
            InitializeComponent();
        }

        private void mAltimeterImage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateSize();
        }

        void UpdateSize()
        {
            mHandGrid.Height = mAltimeterImage.ActualHeight;
            mHandGrid.Width = mAltimeterImage.ActualWidth;

            var lRatio = mAltimeterImage.ActualHeight / mAltimeterImage.Source.Height;
            if (lRatio > 0)
            {
                mHeightBox.FontSize = lRatio * 24;
                mHeightBox.Margin = new Thickness(lRatio * 3);
                mHeightBoxBorder.BorderThickness = new Thickness(lRatio * 5);
                mCenterEllipse.Height = lRatio * 32;
                mCenterEllipse.Width = lRatio * 32;
            }
        }
    }
}