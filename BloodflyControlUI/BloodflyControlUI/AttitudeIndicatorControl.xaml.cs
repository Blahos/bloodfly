﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace BloodflyControlUI
{
    public class PitchConverter : IMultiValueConverter
    {
        public double Multiplier { get; set; } = 1.0;

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values != null && values.Length == 2 && values[0] is double && values[1] is double)
            {
                return (double)values[0] * (double)values[1] * Multiplier;
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Interaction logic for AttitudeIndicatorControl.xaml
    /// </summary>
    public partial class AttitudeIndicatorControl : UserControl
    {
        public double PitchValue
        {
            get { return (double)GetValue(PitchValueProperty); }
            set { SetValue(PitchValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PitchValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PitchValueProperty =
            DependencyProperty.Register("PitchValue", typeof(double), typeof(AttitudeIndicatorControl),
                new PropertyMetadata(0.0));

        public double RollValue
        {
            get { return (double)GetValue(RollValueProperty); }
            set { SetValue(RollValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RollValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RollValueProperty =
            DependencyProperty.Register("RollValue", typeof(double), typeof(AttitudeIndicatorControl),
                new PropertyMetadata(0.0));


        public AttitudeIndicatorControl()
        {
            InitializeComponent();
        }

        private void mStaticPartImage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            mHorizonGrid.Height = mStaticPartImage.ActualHeight;
            mHorizonGrid.Width = mStaticPartImage.ActualWidth;
        }
    }
}