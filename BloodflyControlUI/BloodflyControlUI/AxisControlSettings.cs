﻿using LibraryWpf;

namespace BloodflyControlUI
{
    public class AxisControlSettings : BaseViewModel
    {
        private object mAxis = null;

        public object Axis
        {
            get { return mAxis; }
            set
            {
                if (value == mAxis) return;
                mAxis = value;
                OnPropertyChanged(nameof(Axis));
            }
        }

        private bool mInverted = false;

        public bool Inverted
        {
            get { return mInverted; }
            set
            {
                if (value == mInverted) return;
                mInverted = value;
                OnPropertyChanged(nameof(Inverted));
            }
        }


        private double mDeadband = 0.0;

        public double Deadband
        {
            get { return mDeadband; }
            set
            {
                if (value == mDeadband) return;
                mDeadband = value;
                OnPropertyChanged(nameof(Deadband));
            }
        }

        public AxisControlSettings GetCopy()
        {
            return new AxisControlSettings
            {
                Axis = Axis,
                Deadband = Deadband,
                Inverted = Inverted
            };
        }
    }
}