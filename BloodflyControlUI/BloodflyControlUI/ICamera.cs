﻿using System;
using System.Drawing;

namespace BloodflyControlUI
{
    public interface ICamera : IDisposable
    {
        event Action<Bitmap, DateTime> NewFrame;
    }
}