﻿using FlashCap;
using System.Collections.Generic;
using System.Linq;

namespace BloodflyControlUI
{
    public class FlashCapCameraSelector : ICameraSelector
    {
        public List<string> GetAvailableCameras()
        {
            var devices = new CaptureDevices();
            var descriptors = devices.EnumerateDescriptors();
            return descriptors.Select(a => a.Name).ToList();
        }

        public ICamera GetCamera(string id)
        {
            var devices = new CaptureDevices();
            var descriptor = devices.EnumerateDescriptors().First(a => a.Name == id);
            var characteristics = descriptor.Characteristics.MaxBy(a => a.Width);

            return new FlashCapCamera(descriptor, characteristics);
        }
    }
}