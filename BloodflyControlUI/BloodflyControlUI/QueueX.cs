﻿using System;
using System.Collections.Generic;

namespace BloodflyControlUI
{
    public class QueueX<T> : Queue<T>
    {
        private T mLastAdded = default;

        public T End()
        {
            if (Count <= 0) throw new InvalidOperationException();
            else return mLastAdded;
        }

        public new void Enqueue(T aItem)
        {
            base.Enqueue(aItem);
            mLastAdded = aItem;
        }

        public new T Dequeue()
        {
            T lReturn = base.Dequeue();
            if (Count == 1) mLastAdded = default;
            return lReturn;
        }

        public new void Clear()
        {
            base.Clear();
            mLastAdded = default;
        }
    }
}