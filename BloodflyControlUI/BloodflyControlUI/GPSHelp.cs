﻿using System;

namespace BloodflyControlUI
{
    public class GPSHelp
    {
        public static double ComputeDistance(double aLatitude1, double aLongitude1, double aLatitude2,
            double aLongitude2, double aEarthRadius = 6371e3)
        {
            var lLat1Rad = aLatitude1 * Math.PI / 180;
            var lLong1Rad = aLongitude1 * Math.PI / 180;

            var lLat2Rad = aLatitude2 * Math.PI / 180;
            var lLong2Rad = aLongitude2 * Math.PI / 180;

            var lLatDiffRad = (aLatitude2 - aLatitude1) * Math.PI / 180;
            var lLongDiffRad = (aLongitude2 - aLongitude1) * Math.PI / 180;

            var lSinLatHalf = Math.Sin(lLatDiffRad / 2);
            var lSinLongHalf = Math.Sin(lLongDiffRad / 2);

            var a = lSinLatHalf * lSinLatHalf + Math.Cos(lLat1Rad) * Math.Cos(lLat2Rad) * lSinLongHalf * lSinLongHalf;
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var lDistance = aEarthRadius * c;

            return lDistance;
        }

        /// <summary>
        /// from https://stackoverflow.com/questions/3932502/calculate-angle-between-two-latitude-longitude-points
        /// </summary>
        /// <param name="aLatitude1"></param>
        /// <param name="aLongitude1"></param>
        /// <param name="aLatitude2"></param>
        /// <param name="aLongitude2"></param>
        /// <param name="aEarthRadius"></param>
        /// <returns></returns>
        public static double ComputeBearing(double aLatitude1, double aLongitude1, double aLatitude2,
            double aLongitude2)
        {
            var lLat1Rad = aLatitude1 / 180.0 * Math.PI;
            var lLon1Rad = aLongitude1 / 180.0 * Math.PI;
            var lLat2Rad = aLatitude2 / 180.0 * Math.PI;
            var lLon2Rad = aLongitude2 / 180.0 * Math.PI;

            double dLon = (lLon2Rad - lLon1Rad);

            double y = Math.Sin(dLon) * Math.Cos(lLat2Rad);
            double x = Math.Cos(lLat1Rad) * Math.Sin(lLat2Rad) - Math.Sin(lLat1Rad)
                * Math.Cos(lLat2Rad) * Math.Cos(dLon);

            double brng = Math.Atan2(y, x);

            brng *= 180.0 / Math.PI;
            brng = (brng + 360) % 360;
            brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

            return brng;
        }
    }
}