﻿using LibraryWpf;
using System.Collections.ObjectModel;
using System.Windows;

namespace BloodflyControlUI
{
    public class MapControlViewModel : BaseViewModel
    {
        private MapType mMapType = MapType.GoogleNormal;

        public MapType MapType
        {
            get { return mMapType; }
            set
            {
                if (value == mMapType) return;
                mMapType = value;
                OnPropertyChanged(nameof(MapType));
            }
        }

        private ObservableCollection<Vector> mMapPoints;

        public ObservableCollection<Vector> MapPoints
        {
            get { return mMapPoints; }
            set
            {
                if (value == mMapPoints) return;
                mMapPoints = value;
                OnPropertyChanged(nameof(MapPoints));
            }
        }


        private double mMinPositionUpdateDistance = 1;

        /// <summary>
        /// minimum position change (in meters) to update the current position (add new point to path)
        /// </summary>
        public double MinPositionUpdateDistance
        {
            get { return mMinPositionUpdateDistance; }
            set
            {
                if (value == mMinPositionUpdateDistance) return;
                mMinPositionUpdateDistance = value;
                OnPropertyChanged(nameof(MinPositionUpdateDistance));
            }
        }
    }
}