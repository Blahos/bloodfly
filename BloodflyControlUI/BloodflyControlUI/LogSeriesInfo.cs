﻿using System;
using System.Collections.Generic;

namespace BloodflyControlUI
{
    public class LogSeriesInfo(Func<double> aGetValue, bool aIsCumulativeFlag = false)
    {
        private Dictionary<int, double> mLastValue = new Dictionary<int, double>();
        private Func<double> GetValueFunc { get; } = aGetValue;
        public bool IsCumulativeFlag { get; } = aIsCumulativeFlag;

        public double GetLastValue(int aId = 0)
        {
            if (mLastValue.ContainsKey(aId))
                return mLastValue[aId];
            else return Double.NaN;
        }

        public double GetValue(int aId = 0)
        {
            var lValue = GetValueFunc();
            mLastValue[aId] = lValue;
            return lValue;
        }
    }
}