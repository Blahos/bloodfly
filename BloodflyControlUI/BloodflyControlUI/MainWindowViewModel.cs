﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows;
using SharpDX.XInput;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.IO;
using System.Globalization;
using System.Drawing;
using OxyPlot;
using OxyPlot.Series;

namespace BloodflyControlUI
{
    public class MainWindowViewModel : BaseViewModel
    {
        private List<string> mPortLines = new List<string>();
        private readonly Timer mReadTimer = null;
        private readonly Timer mWriteTimer = null;
        private readonly Timer mCameraFastTimer = null;
        private readonly Timer mCameraSlowTimer = null;
        private readonly Timer mLogTimer = null;

        private readonly Announcer mAnnouncer = new Announcer();

        private bool mAppClosing = false;

        private SerialPort mPort = null;
        private const int mBaudRate = 38400;
        private readonly Queue<DateTime> mWriteTimerTimes = new Queue<DateTime>(200);
        const int mWriteSuccessFeedbackTimesSize = 200;

        private readonly Queue<DateTime>
            mWriteSuccesFeedbackTimes = new Queue<DateTime>(mWriteSuccessFeedbackTimesSize);

        private readonly Queue<DateTime> mReadTimerTimes = new Queue<DateTime>(200);
        const int cRadioFeedbackTimesSize = 200;
        private readonly Queue<DateTime> mRadioFeedbackTimes = new Queue<DateTime>(cRadioFeedbackTimesSize);
        private readonly QueueX<Tuple<DateTime, Bitmap>> mNewCameraFrames = new QueueX<Tuple<DateTime, Bitmap>>();
        private readonly Queue<Tuple<DateTime, double[]>> mFileLogDataBuffer = new Queue<Tuple<DateTime, double[]>>();

        private readonly Dictionary<string, Queue<DataPoint>> mPlotDataBuffer =
            new Dictionary<string, Queue<DataPoint>>();

        private const string cOrientationTabHeader = "Orientation";

        private readonly ICameraSelector mCameraSelector = new FlashCapCameraSelector();

        public PlaneController Controller { get; } = new PlaneController();

        private readonly Dictionary<PlaneController.ControllerType, List<AxisControlSettings>> mAxisSettingsStorage =
            new Dictionary<PlaneController.ControllerType, List<AxisControlSettings>>
            {
                {
                    PlaneController.ControllerType.Gamepad,
                    new List<AxisControlSettings>
                    {
                        new AxisControlSettings { Axis = GamepadAxes.LeftThumbX, Deadband = 0.1 },
                        new AxisControlSettings { Axis = GamepadAxes.LeftThumbY, Deadband = 0.1, Inverted = true },
                        new AxisControlSettings { Axis = GamepadAxes.RightThumbX, Deadband = 0.1 },
                        new AxisControlSettings { Axis = GamepadAxes.Trigger, Deadband = 0.1 },
                    }
                },
                {
                    PlaneController.ControllerType.Joystick,
                    new List<AxisControlSettings>
                    {
                        new AxisControlSettings { Axis = JoystickAxes.StickX },
                        new AxisControlSettings { Axis = JoystickAxes.StickY },
                        new AxisControlSettings { Axis = JoystickAxes.StickTwist },
                        new AxisControlSettings { Axis = JoystickAxes.ThrottleLever },
                    }
                },
            };

        private bool mAllowEditControllerButtons = false;

        public bool AllowEditControllerButtons
        {
            get { return mAllowEditControllerButtons; }
            set
            {
                if (value == mAllowEditControllerButtons) return;
                mAllowEditControllerButtons = value;
                OnPropertyChanged(nameof(AllowEditControllerButtons));
            }
        }

        private readonly TimeSpan mPortListUpdatePeriod = TimeSpan.FromSeconds(2);
        private DateTime mLastPortListUpdateTime = DateTime.MinValue;

        private readonly TimeSpan mCameraListUpdatePeriod = TimeSpan.FromSeconds(2);
        private DateTime mLastCameraListUpdateTime = DateTime.MinValue;

        private DateTime mLastGPSUpdate = DateTime.MinValue;

        private const int cReadTimerPeriod = 45;
        private const int cWriteTimerPeriod = 10;
        private const int cCameraFastTimerPeriod = 50;
        private const int cCameraSlowTimerPeriod = 800;
        private const int cLogTimerPeriod = 50;

        private const int cFileLogWritePeriod = 2000;

        private int mCameraFileIndex = 0;

        DateTime mLastBatteryAnnounceTime = DateTime.Now;

        DateTime mLastControlArduinoFeedbackTime = DateTime.MinValue;

        // flag that will be raised by read timer for the write timer to lock throttle (so we don't access throttle lock variables directly in read timer)
        bool mLockThrottle = false;

        private readonly DateTime cAppStartTime = DateTime.Now;

        private DateTime mLastLogWindowTimeStampTime = DateTime.MinValue;

        private readonly HashSet<Key> mKeysDown = new HashSet<Key>();

        private readonly string cLogDirectory = "log";
        private readonly StreamWriter mLogFile;
        private readonly StreamWriter mSerialLogFile;
        private readonly string cAppStartDateString;

        private string mCurrentSerialReceiveMessage = null;

        // this is a cumulative value over the entire app run
        private long mWriteToPlaneFailCount = 0;
        private long mPlaneFeedbackCount = 0;

        private readonly StringBuilder mLogStringBuilder = new StringBuilder();

        /// <summary>
        /// Buffers for write timer to avoid allocation each iteration
        /// </summary>
        const int cSourceValuesLength = 4;

        const int cTrimValuesCount = 4;
        private readonly float[] mSourceValues = new float[cSourceValuesLength];

        const int cMessageSize =
            6 * sizeof(byte) +
            cSourceValuesLength * sizeof(float) +
            cTrimValuesCount * sizeof(byte) +
            sizeof(short) + sizeof(byte)
            + 4 * sizeof(byte)
            + sizeof(byte)
            + sizeof(byte)
            + sizeof(byte);

        private readonly byte[] mMessageBuffer = new byte[cMessageSize];

        private DateTime mLastBatteryReading = DateTime.MinValue;

        public List<GamepadAxes> GamepadAxesOptions { get; } =
            Enum.GetValues(typeof(GamepadAxes)).Cast<GamepadAxes>().ToList();

        private DateTime mLastPlaneFeedback = DateTime.MinValue;

        private const GamepadButtonFlags cThrottleSetpoint1Button = GamepadButtonFlags.X;
        private const GamepadButtonFlags cThrottleSetpoint2Button = GamepadButtonFlags.B;

        private DateTime mFirstVideoFrameTime = DateTime.MinValue;

        private long mTimeCheckpointCount = 0;

        private DateTime mLastWriteToPlaneFailTime = DateTime.MinValue;

        private DateTime mLastLogFileWriteTime = DateTime.MinValue;

        public MainWindowViewModel()
        {
            OrientationViewModel = new OrientationControlViewModel();
            MapViewModel = new MapControlViewModel();
            MapViewModel.MapPoints = new ObservableCollection<Vector>();
            ControlValues = new PlaneControlValues();
            TrimValues = new PlaneTrimValues();


            OrientationViewModel.PlaneControlValues = ControlValues;

            SetAxisSettings(Controller.ActiveType);

            Controller.PropertyChanged += Controller_PropertyChanged;

            ControlValues.Announcer = mAnnouncer;

            Directory.CreateDirectory(cLogDirectory);

            cAppStartDateString = cAppStartTime.ToUniversalTime().ToString("dd_MM_yyyy_HH_mm_ss_ffff").Replace(':', '_')
                .Replace(' ', '_');
            mLogFile = new StreamWriter(cLogDirectory + "/log_file_" + cAppStartDateString + ".txt");

            LogDataSeries = new Dictionary<string, LogSeriesInfo>
            {
                { "Ailerons", new LogSeriesInfo(() => ControlValues.Ailerons) },
                { "Elevator", new LogSeriesInfo(() => ControlValues.Elevator) },
                { "Rudder", new LogSeriesInfo(() => ControlValues.Rudder) },
                { "Throttle", new LogSeriesInfo(() => ControlValues.Throttle) },
                { "ThrottleLocked", new LogSeriesInfo(() => ControlValues.ThrottleLocked ? 1 : 0) },
                { "FlapOffset", new LogSeriesInfo(() => ControlValues.FlapOffset) },
                { "Checkpoint", new LogSeriesInfo(() => mTimeCheckpointCount, true) },
                { "Controller", new LogSeriesInfo(() => Controller.IsConnected ? 1 : 0) },
                { "Gamepad", new LogSeriesInfo(() => Controller.IsConnectedGamepad ? 1 : 0) },
                { "Joystick", new LogSeriesInfo(() => Controller.IsConnectedJoystick ? 1 : 0) },
                { "SerialPort", new LogSeriesInfo(() => PortOpen ? 1 : 0) },
                { "Height", new LogSeriesInfo(() => OrientationViewModel.Height) },
                { "HeightRaw", new LogSeriesInfo(() => OrientationViewModel.HeightRaw) },
                { "TemperatureRaw", new LogSeriesInfo(() => OrientationViewModel.TemperatureRaw) },
                { "PressureRaw", new LogSeriesInfo(() => OrientationViewModel.PressureRaw) },
                { "PressureLevel0", new LogSeriesInfo(() => OrientationViewModel.PressureLevel0) },
                { "GPS_lat", new LogSeriesInfo(() => OrientationViewModel.GPSData.Latitude) },
                { "GPS_long", new LogSeriesInfo(() => OrientationViewModel.GPSData.Longitude) },
                { "GPS_speed", new LogSeriesInfo(() => OrientationViewModel.GPSData.Speed) },
                { "GPS_course", new LogSeriesInfo(() => OrientationViewModel.GPSData.Course) },
                { "GPS_alt", new LogSeriesInfo(() => OrientationViewModel.GPSData.Altitude) },
                { "Battery", new LogSeriesInfo(() => PlaneBattery) },
                { "EnablePlaneFeedback", new LogSeriesInfo(() => EnablePlaneFeedback ? 1 : 0) },
                {
                    "WriteToPlaneFailCount", new LogSeriesInfo(() => Interlocked.Read(ref mWriteToPlaneFailCount), true)
                },
                { "PlaneFeedbackCount", new LogSeriesInfo(() => Interlocked.Read(ref mPlaneFeedbackCount), true) },
                { "Pitch", new LogSeriesInfo(() => OrientationViewModel.Pitch) },
                { "Roll", new LogSeriesInfo(() => OrientationViewModel.Roll) },
                { "PitchRaw", new LogSeriesInfo(() => OrientationViewModel.PitchRaw) },
                { "RollRaw", new LogSeriesInfo(() => OrientationViewModel.RollRaw) }
            };

            mLogFile.Write("Time");

            foreach (var nItem in LogDataSeries)
            {
                mLogFile.Write(",");
                mLogFile.Write(nItem.Key);
            }

            mLogFile.WriteLine();

            foreach (var nItem in LogDataSeries)
            {
                mPlotDataBuffer.Add(nItem.Key, new Queue<DataPoint>());
            }

            SelectedDataSeriesPlot = LogDataSeries.First().Key;

            mSerialLogFile = new StreamWriter(cLogDirectory + "/serial_log_file_" + cAppStartDateString + ".txt");
            mSerialLogFile.WriteLine("Serial port logging started");
            mSerialLogFile.Flush();

            // we will use tag to identify the plotted series
            PlotModel.Series.Add(new LineSeries() { Tag = null });

            mReadTimer = new Timer(ReadTimerCallbackFcn, null, 0, Timeout.Infinite);
            mWriteTimer = new Timer(WriteTimerCallbackFcn, null, 0, Timeout.Infinite);
            mCameraFastTimer = new Timer(CameraFastTimerCallbackFcn, null, 0, Timeout.Infinite);
            mCameraSlowTimer = new Timer(CameraSlowTimerCallbackFcn, null, 0, Timeout.Infinite);
            mLogTimer = new Timer(LogTimerCallbackFcn, null, 0, Timeout.Infinite);
        }

        private void Controller_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Controller.ActiveType))
            {
                mLockThrottle = true;
                SetAxisSettings(Controller.ActiveType);
                //SetButtonSettings(Controller.ActiveType);
            }
        }

        void SetAxisSettings(PlaneController.ControllerType aControllerType)
        {
            ControlValues.AileronsAxis = mAxisSettingsStorage[aControllerType][0];
            ControlValues.ElevatorAxis = mAxisSettingsStorage[aControllerType][1];
            ControlValues.RudderAxis = mAxisSettingsStorage[aControllerType][2];
            ControlValues.ThrottleAxis = mAxisSettingsStorage[aControllerType][3];
        }

        //void SetButtonSettings(PlaneController.ControllerType aControllerType)
        //{
        //	ControlValues.Buttons = mButtonSettingStorage[aControllerType];

        //	foreach (var nButton in ControlValues.Buttons)
        //		nButton.ButtonOptions = Controller.ButtonOptions[aControllerType];
        //}

        private void ProcessReceivedSerialMessage(string aMessage, DateTime aTimeStamp)
        {
            try
            {
                var lParts = aMessage.Split(';');

                var lNaNSymbol = CultureInfo.InvariantCulture.NumberFormat.NaNSymbol.ToLower();

                const double lHeightA = 0.9;
                const double lHeightB = 1.0 - lHeightA;

                if (lParts.Length <= 0) return;

                var lFirstPart = lParts[0];

                // pitch roll
                if (lFirstPart == "PR")
                {
                    if (lParts.Length == 3)
                    {
                        var lPitch = float.Parse(lParts[1], CultureInfo.InvariantCulture);
                        var lRoll = float.Parse(lParts[2], CultureInfo.InvariantCulture);

                        OrientationViewModel.PitchRaw = lPitch;
                        OrientationViewModel.RollRaw = lRoll;

                        const double lA = 0.01;
                        const double lB = 1 - lA;

                        OrientationViewModel.Pitch = lA * OrientationViewModel.Pitch + lB * lPitch;

                        if (OrientationViewModel.Roll < -90 && lRoll > 90)
                            lRoll -= 360;
                        if (OrientationViewModel.Roll > 90 && lRoll < -90)
                            lRoll += 360;

                        var lNewRoll = lA * OrientationViewModel.Roll + lB * lRoll;

                        if (lNewRoll > 180)
                            lNewRoll -= 360;
                        if (lNewRoll < -180)
                            lNewRoll += 360;

                        OrientationViewModel.Roll = lNewRoll;
                    }
                }
                // height
                else if (lFirstPart == "H")
                {
                    if (lParts.Length == 2)
                    {
                        var lHeight = float.Parse(lParts[1], CultureInfo.InvariantCulture);

                        OrientationViewModel.Height =
                            lHeightA * OrientationViewModel.Height + (lHeightB) * (double)lHeight;
                        OrientationViewModel.HeightRaw = lHeight;
                    }
                }
                // temperature
                else if (lFirstPart == "T")
                {
                    if (lParts.Length == 2)
                    {
                        var lTemperature = float.Parse(lParts[1], CultureInfo.InvariantCulture);
                        OrientationViewModel.TemperatureRaw = lTemperature;
                    }
                }
                // pressure
                else if (lFirstPart == "P")
                {
                    if (lParts.Length == 2)
                    {
                        var lPressure = float.Parse(lParts[1], CultureInfo.InvariantCulture);
                        OrientationViewModel.PressureRaw = lPressure;
                        if (OrientationViewModel.PressureLevel0 != 0)
                        {
                            var lHeight = 44330.0 * (1.0 -
                                                     Math.Pow(
                                                         OrientationViewModel.PressureRaw /
                                                         OrientationViewModel.PressureLevel0, 0.1903));
                            OrientationViewModel.HeightRaw = lHeight;
                            OrientationViewModel.Height = lHeightA * OrientationViewModel.Height + lHeightB * lHeight;
                        }
                    }
                }
                // zero level pressure
                else if (lFirstPart == "P0")
                {
                    if (lParts.Length == 2)
                    {
                        var lPressure0 = float.Parse(lParts[1], CultureInfo.InvariantCulture);
                        OrientationViewModel.PressureLevel0 = lPressure0;
                    }
                }
                // yaw (heading)
                else if (lFirstPart == "Y")
                {
                    if (lParts.Length == 2)
                    {
                        var lYaw = float.Parse(lParts[1], CultureInfo.InvariantCulture);

                        if (OrientationViewModel.Heading < 180 && lYaw > 180)
                            lYaw -= 360;

                        if (OrientationViewModel.Heading > 180 && lYaw < 180)
                            lYaw += 360;

                        const double lA = 0.9;
                        const double lB = 1 - lA;

                        var lNewYaw = lA * OrientationViewModel.Heading + lB * lYaw;

                        while (lNewYaw < 0) lNewYaw += 360;
                        while (lNewYaw > 360) lNewYaw -= 360;

                        OrientationViewModel.Heading = lNewYaw;
                    }
                }
                // GPS coordintes
                else if (lFirstPart == "GPS")
                {
                    if (lParts.Length == 6)
                    {
                        var lLatitude = double.Parse(lParts[1], CultureInfo.InvariantCulture);
                        var lLongitude = double.Parse(lParts[2], CultureInfo.InvariantCulture);
                        var lSpeed = double.Parse(lParts[3], CultureInfo.InvariantCulture);
                        var lCourse = double.Parse(lParts[4], CultureInfo.InvariantCulture);
                        var lAltitude = double.Parse(lParts[5], CultureInfo.InvariantCulture);

                        OrientationViewModel.GPSData.Latitude = lLatitude;
                        OrientationViewModel.GPSData.Longitude = lLongitude;
                        OrientationViewModel.GPSData.Speed = lSpeed;
                        OrientationViewModel.GPSData.Course = lCourse;
                        OrientationViewModel.GPSData.Altitude = lAltitude;

                        mLastGPSUpdate = DateTime.Now;

                        // update map points
                        var lNewPoint = new Vector(lLatitude, lLongitude);

                        if (MapViewModel.MapPoints.Count > 0)
                        {
                            var lLastMapPoint = MapViewModel.MapPoints[MapViewModel.MapPoints.Count - 1];

                            var lDistance = GPSHelp.ComputeDistance(lLastMapPoint.X, lLastMapPoint.Y, lNewPoint.X,
                                lNewPoint.Y);

                            if (lDistance >= MapViewModel.MinPositionUpdateDistance)
                            {
                                Application.Current.Dispatcher.Invoke(() => { MapViewModel.MapPoints.Add(lNewPoint); });
                            }
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() => { MapViewModel.MapPoints.Add(lNewPoint); });
                        }
                    }
                }
                else if (lFirstPart == "TM")
                {
                    if (lParts.Length == 2)
                    {
                        var lThrottleMinutes = Double.Parse(lParts[1], CultureInfo.InvariantCulture);
                        ThrottleMinutes = lThrottleMinutes;
                    }
                }
                // plane arduino loop frequency
                else if (lFirstPart == "PALF")
                {
                    if (lParts.Length == 2)
                    {
                        var lLoopFreq = double.Parse(lParts[1], CultureInfo.InvariantCulture);
                        PlaneArduinoLoopFrequency = lLoopFreq;
                    }
                }
                // plane battery capacity
                else if (lFirstPart == "B")
                {
                    if (lParts.Length == 2)
                    {
                        var lVoltageRaw = float.Parse(lParts[1], CultureInfo.InvariantCulture);
                        PlaneBatteryVoltageRaw = lVoltageRaw;

                        if (lVoltageRaw >= 0)
                        {
                            mLastBatteryReading = DateTime.Now;
                        }
                    }
                }
                else if (lFirstPart == "LF")
                {
                    if (lParts.Length == 2)
                    {
                        var lFreq = double.Parse(lParts[1], CultureInfo.InvariantCulture);
                        ControlArduinoLoopFrequency = lFreq;
                    }
                }
            }
            catch (Exception aEx)
            {
                Console.WriteLine("Exception when parsing message <{0}>: {1}", aMessage, aEx.Message);
            }
        }

        private void ResetPlaneFeedbackData()
        {
            PlaneArduinoLoopFrequency = 0;
        }

        private void AnnounceTrimSurface()
        {
            //Task.Run(() =>
            //{
            if (AileronLeftTrimSelected)
                mAnnouncer.AnnounceLeftAileronTrim();

            if (AileronRightTrimSelected)
                mAnnouncer.AnnounceRightAileronTrim();

            if (AileronsBothTrimSelected)
                mAnnouncer.AnnounceBothAileronsTrim();

            if (ElevatorTrimSelected)
                mAnnouncer.AnnounceElevatorTrim();

            if (RudderTrimSelected)
                mAnnouncer.AnnounceRudderTrim();
            //});
        }

        private void AnnounceFlapMode()
        {
            //Task.Run(() =>
            //{
            if (ControlValues.FlapModeNeutral)
                mAnnouncer.AnnounceFlapsNeutral();
            if (ControlValues.FlapModeDown1)
                mAnnouncer.AnnounceFlapsDown1();
            if (ControlValues.FlapModeDown2)
                mAnnouncer.AnnounceFlapsDown2();
            //});
        }

        private bool mSquareLeftStick = false;

        public bool SquareLeftStick
        {
            get { return mSquareLeftStick; }
            set
            {
                if (value == mSquareLeftStick) return;
                mSquareLeftStick = value;
                OnPropertyChanged(nameof(SquareLeftStick));
            }
        }

        private bool mSquareRightStick = false;

        public bool SquareRightStick
        {
            get { return mSquareRightStick; }
            set
            {
                if (value == mSquareRightStick) return;
                mSquareRightStick = value;
                OnPropertyChanged(nameof(SquareRightStick));
            }
        }


        private bool mTimeCheckpointTextBlockVisible = false;

        public bool TimeCheckpointTextBlockVisible
        {
            get { return mTimeCheckpointTextBlockVisible; }
            set
            {
                if (value == mTimeCheckpointTextBlockVisible) return;
                mTimeCheckpointTextBlockVisible = value;
                OnPropertyChanged(nameof(TimeCheckpointTextBlockVisible));
            }
        }

        private OrientationControlViewModel mOrientationViewModel;

        public OrientationControlViewModel OrientationViewModel
        {
            get { return mOrientationViewModel; }
            set
            {
                if (value == mOrientationViewModel) return;
                mOrientationViewModel = value;
                OnPropertyChanged(nameof(OrientationViewModel));
            }
        }


        private MapControlViewModel mMapViewModel;

        public MapControlViewModel MapViewModel
        {
            get { return mMapViewModel; }
            set
            {
                if (value == mMapViewModel) return;
                mMapViewModel = value;
                OnPropertyChanged(nameof(MapViewModel));
            }
        }

        private PlaneControlValues mControlValues;

        public PlaneControlValues ControlValues
        {
            get { return mControlValues; }
            set
            {
                if (value == mControlValues) return;
                mControlValues = value;
                OnPropertyChanged(nameof(ControlValues));
            }
        }


        private PlaneTrimValues mTrimValues;

        public PlaneTrimValues TrimValues
        {
            get { return mTrimValues; }
            set
            {
                if (value == mTrimValues) return;
                mTrimValues = value;
                OnPropertyChanged(nameof(TrimValues));
            }
        }

        private string mSelectedTabHeader;

        public string SelectedTabHeader
        {
            get { return mSelectedTabHeader; }
            set
            {
                if (value == mSelectedTabHeader) return;
                mSelectedTabHeader = value;
                OnPropertyChanged(nameof(SelectedTabHeader));
            }
        }

        private double mControlArduinoLoopFrequency;

        public double ControlArduinoLoopFrequency
        {
            get { return mControlArduinoLoopFrequency; }
            set
            {
                if (value == mControlArduinoLoopFrequency) return;
                mControlArduinoLoopFrequency = value;
                OnPropertyChanged(nameof(ControlArduinoLoopFrequency));
            }
        }

        private double mPlaneArduinoLoopFrequency;

        public double PlaneArduinoLoopFrequency
        {
            get { return mPlaneArduinoLoopFrequency; }
            set
            {
                if (value == mPlaneArduinoLoopFrequency) return;
                mPlaneArduinoLoopFrequency = value;
                OnPropertyChanged(nameof(PlaneArduinoLoopFrequency));
            }
        }

        private double mRadioFeedbackFrequency;

        public double RadioFeedbackFrequency
        {
            get { return mRadioFeedbackFrequency; }
            set
            {
                if (value == mRadioFeedbackFrequency) return;
                mRadioFeedbackFrequency = value;
                OnPropertyChanged(nameof(RadioFeedbackFrequency));
            }
        }

        public Dictionary<string, LogSeriesInfo> LogDataSeries { get; }

        private string mSelectedDataSeriesPlot;

        public string SelectedDataSeriesPlot
        {
            get { return mSelectedDataSeriesPlot; }
            set
            {
                if (value == mSelectedDataSeriesPlot) return;
                mSelectedDataSeriesPlot = value;
                OnPropertyChanged(nameof(SelectedDataSeriesPlot));
            }
        }

        public PlotModel PlotModel { get; } = new PlotModel();


        private bool mUpdatePlotLastChecked = true;
        private bool mUpdatePlot = true;

        public bool UpdatePlot
        {
            get { return mUpdatePlot; }
            set
            {
                if (value == mUpdatePlot) return;
                mUpdatePlot = value;
                OnPropertyChanged(nameof(UpdatePlot));
            }
        }

        private int mPlotXAxisRange = 10;

        public int PlotXAxisRange
        {
            get { return mPlotXAxisRange; }
            set
            {
                if (value == mPlotXAxisRange) return;
                mPlotXAxisRange = value;
                OnPropertyChanged(nameof(PlotXAxisRange));
            }
        }

        private double mPlotYAxisMin = -1;

        public double PlotYAxisMin
        {
            get { return mPlotYAxisMin; }
            set
            {
                if (value == mPlotYAxisMin) return;
                mPlotYAxisMin = value;
                OnPropertyChanged(nameof(PlotYAxisMin));
            }
        }

        private double mPlotYAxisMax = 1;

        public double PlotYAxisMax
        {
            get { return mPlotYAxisMax; }
            set
            {
                if (value == mPlotYAxisMax) return;
                mPlotYAxisMax = value;
                OnPropertyChanged(nameof(PlotYAxisMax));
            }
        }

        private bool mPlotYAxisMinAuto = true;

        public bool PlotYAxisMinAuto
        {
            get { return mPlotYAxisMinAuto; }
            set
            {
                if (value == mPlotYAxisMinAuto) return;
                mPlotYAxisMinAuto = value;
                OnPropertyChanged(nameof(PlotYAxisMinAuto));
            }
        }

        private bool mPlotYAxisMaxAuto = true;

        public bool PlotYAxisMaxAuto
        {
            get { return mPlotYAxisMaxAuto; }
            set
            {
                if (value == mPlotYAxisMaxAuto) return;
                mPlotYAxisMaxAuto = value;
                OnPropertyChanged(nameof(PlotYAxisMaxAuto));
            }
        }

        /// <summary>
        /// in seconds
        /// </summary>
        public int MaxPlotDataRange { get; } = 180;

        private bool mXAxisLive = true;

        public bool XAxisLive
        {
            get { return mXAxisLive; }
            set
            {
                if (value == mXAxisLive) return;
                mXAxisLive = value;
                OnPropertyChanged(nameof(XAxisLive));
            }
        }


        private double mPlaneReferenceVoltage = 5.0;

        public double PlaneReferenceVoltage
        {
            get { return mPlaneReferenceVoltage; }
            set
            {
                if (value == mPlaneReferenceVoltage) return;
                mPlaneReferenceVoltage = value;
                OnPropertyChanged(nameof(PlaneReferenceVoltage));
                OnPropertyChanged(nameof(PlaneBatteryVoltage));
            }
        }

        private double mPlaneBatteryVoltageRaw = Double.NaN;

        public double PlaneBatteryVoltageRaw
        {
            get { return mPlaneBatteryVoltageRaw; }
            set
            {
                if (value == mPlaneBatteryVoltageRaw) return;
                mPlaneBatteryVoltageRaw = value;
                OnPropertyChanged(nameof(PlaneBatteryVoltageRaw));
                OnPropertyChanged(nameof(PlaneBatteryVoltage));
            }
        }

        public double PlaneBatteryVoltage
        {
            get
            {
                if (Double.IsNaN(PlaneBatteryVoltageRaw) || PlaneBatteryVoltageRaw < 0)
                    return -1;

                return PlaneBatteryVoltageRaw / 1023.0 * PlaneReferenceVoltage * 5.0;
            }
        }

        private double mPlaneBattery = -1;

        /// <summary>
        /// value between 0 and 1, indicating plane battery capacity
        /// </summary>
        public double PlaneBattery
        {
            get { return mPlaneBattery; }
            set
            {
                if (value == mPlaneBattery) return;
                mPlaneBattery = value;
                OnPropertyChanged(nameof(PlaneBattery));
            }
        }


        private double mBatteryMinVoltage = 3 * 3.4;

        public double BatteryMinVoltage
        {
            get { return mBatteryMinVoltage; }
            set
            {
                if (value == mBatteryMinVoltage) return;
                mBatteryMinVoltage = value;
                OnPropertyChanged(nameof(BatteryMinVoltage));
            }
        }

        private double mThrottleMinutes;

        public double ThrottleMinutes
        {
            get { return mThrottleMinutes; }
            set
            {
                if (value == mThrottleMinutes) return;
                mThrottleMinutes = value;
                OnPropertyChanged(nameof(ThrottleMinutes));
            }
        }

        private bool mResetThrottleMinutes = false;

        public bool ResetThrottleMinutes
        {
            get { return mResetThrottleMinutes; }
            set
            {
                if (value == mResetThrottleMinutes) return;
                mResetThrottleMinutes = value;
                OnPropertyChanged(nameof(ResetThrottleMinutes));
            }
        }


        private bool mSaveTrimValues = false;

        public bool SaveTrimValues
        {
            get { return mSaveTrimValues; }
            set
            {
                if (value == mSaveTrimValues) return;
                mSaveTrimValues = value;
                OnPropertyChanged(nameof(SaveTrimValues));
            }
        }

        private double mBatteryMaxVoltage = 3 * 4.2;

        public double BatteryMaxVoltage
        {
            get { return mBatteryMaxVoltage; }
            set
            {
                if (value == mBatteryMaxVoltage) return;
                mBatteryMaxVoltage = value;
                OnPropertyChanged(nameof(BatteryMaxVoltage));
            }
        }

        private string mPortName;

        public string PortName
        {
            get { return mPortName; }
            set
            {
                if (value == mPortName) return;
                mPortName = value;
                ControlValues.ThrottleLocked = true;
                OnPropertyChanged(nameof(PortName));
            }
        }


        private ObservableCollection<string> mPortList = new ObservableCollection<string>();

        public ObservableCollection<string> PortList
        {
            get { return mPortList; }
            set
            {
                if (value == mPortList) return;
                mPortList = value;
                OnPropertyChanged(nameof(PortList));
            }
        }

        private ObservableCollection<string> mCameraList = new ObservableCollection<string>();

        public ObservableCollection<string> CameraList
        {
            get { return mCameraList; }
        }


        private string mSelectedCamera;

        public string SelectedCamera
        {
            get { return mSelectedCamera; }
            set
            {
                if (value == mSelectedCamera) return;
                mSelectedCamera = value;
                OnPropertyChanged(nameof(SelectedCamera));
                UpdateCamera(mSelectedCamera);
            }
        }

        private ICamera mCamera = null;

        private void UpdateCamera(string selectedCamera)
        {
            if (mCamera != null)
            {
                mCamera.NewFrame -= CameraNewFrame;
                mCamera.Dispose();
            }

            mCamera = mCameraSelector.GetCamera(selectedCamera);
            mCamera.NewFrame += CameraNewFrame;
        }

        private void CameraNewFrame(Bitmap bitmap, DateTime time)
        {
            var lBitmap = (Bitmap)bitmap.Clone();
            var lImage = lBitmap.ToBitmapImage();

            if (lImage != null)
            {
                lImage.Freeze(); // avoid cross thread operations and prevents leaks

                Dispatcher.CurrentDispatcher.Invoke(() => { OrientationViewModel.CameraImage = lImage; });
            }

            if (lBitmap != null)
                lBitmap.Dispose();
        }

        //private FilterInfo mSelectedCamera = null;
        //public FilterInfo SelectedCamera
        //{
        //	get { return mSelectedCamera; }
        //	set
        //	{
        //		if (value == mSelectedCamera) return;
        //		mSelectedCamera = value;
        //		OnPropertyChanged(nameof(SelectedCamera));

        //		if (mCameraAdapter != null)
        //		{
        //			mCameraAdapter.Stop();
        //			mCameraAdapter.NewFrameEvent -= CameraAdapterNewFrameEvent;
        //		}
        //		lock (mNewCameraFrames)
        //		{
        //			foreach (var nItem in mNewCameraFrames)
        //				nItem.Item2.Dispose();

        //			mNewCameraFrames.Clear();
        //		}
        //		lock (mVideoWriterLock)
        //		{
        //			mVideoWriter?.Close();
        //		}
        //		Console.WriteLine("Creating new camera adapter");
        //		mCameraAdapter = new CameraAdapter(mSelectedCamera.MonikerString);
        //		mCameraAdapter.Start();

        //		mCameraAdapter.NewFrameEvent += CameraAdapterNewFrameEvent;
        //	}
        //}

        private bool mRecordCameraToFile = false;

        public bool RecordCameraToFile
        {
            get { return mRecordCameraToFile; }
            set
            {
                if (value == mRecordCameraToFile) return;
                mRecordCameraToFile = value;
                OnPropertyChanged(nameof(RecordCameraToFile));
            }
        }

        private double mCameraFramerate = 0;

        public double CameraFramerate
        {
            get { return mCameraFramerate; }
            set
            {
                if (value == mCameraFramerate) return;
                mCameraFramerate = value;
                OnPropertyChanged(nameof(CameraFramerate));
            }
        }

        private void CameraAdapterNewFrameEvent(Bitmap aBitmap)
        {
            try
            {
                var lNow = DateTime.Now;
                lock (mNewCameraFrames)
                {
                    mNewCameraFrames.Enqueue(new Tuple<DateTime, Bitmap>(lNow, (Bitmap)aBitmap.Clone()));
                }
            }
            catch (Exception lEx)
            {
                Console.WriteLine("Exception in camera new frame event: {0}", lEx.Message);
            }
        }

        public ObservableCollection<Stretch> VideoFillList { get; } =
            new ObservableCollection<Stretch>(Enum.GetValues(typeof(Stretch)).Cast<Stretch>());

        private string mText;

        public string Text
        {
            get { return mText; }
            set
            {
                if (value == mText) return;
                mText = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        private bool mPortOpen;

        public bool PortOpen
        {
            get { return mPortOpen; }
            set
            {
                if (value == mPortOpen) return;
                mPortOpen = value;
                OnPropertyChanged(nameof(PortOpen));
                OnPropertyChanged(nameof(PortStatusText));
            }
        }

        public string PortStatusText
        {
            get
            {
                if (mPortOpen) return "open";
                else return "closed";
            }
        }


        private double mWriteTimerFrequency = 0;

        public double WriteTimerFrequency
        {
            get { return mWriteTimerFrequency; }
            set
            {
                if (value == mWriteTimerFrequency) return;
                mWriteTimerFrequency = value;
                OnPropertyChanged(nameof(WriteTimerFrequency));
            }
        }


        private double mReadTimerFrequency = 0;

        public double ReadTimerFrequency
        {
            get { return mReadTimerFrequency; }
            set
            {
                if (value == mReadTimerFrequency) return;
                mReadTimerFrequency = value;
                OnPropertyChanged(nameof(ReadTimerFrequency));
            }
        }

        private double mWriteSuccessFrequency;

        public double WriteSuccessFrequency
        {
            get { return mWriteSuccessFrequency; }
            set
            {
                if (value == mWriteSuccessFrequency) return;
                mWriteSuccessFrequency = value;
                OnPropertyChanged(nameof(WriteSuccessFrequency));
            }
        }

        private bool mScrollLock = true;

        public bool ScrollLock
        {
            get { return mScrollLock; }
            set
            {
                if (value == mScrollLock) return;
                mScrollLock = value;
                OnPropertyChanged(nameof(ScrollLock));
            }
        }


        private bool mPlaneWrite = false;

        public bool PlaneWrite
        {
            get { return mPlaneWrite; }
            set
            {
                if (value == mPlaneWrite) return;
                mPlaneWrite = value;
                OnPropertyChanged(nameof(PlaneWrite));
            }
        }

        private bool mEnablePlaneFeedback = false;

        public bool EnablePlaneFeedback
        {
            get { return mEnablePlaneFeedback; }
            set
            {
                if (value == mEnablePlaneFeedback) return;
                mEnablePlaneFeedback = value;
                OnPropertyChanged(nameof(EnablePlaneFeedback));
            }
        }

        private bool mPlaneRadioFeedback = false;

        public bool PlaneRadioFeedback
        {
            get { return mPlaneRadioFeedback; }
            set
            {
                if (value == mPlaneRadioFeedback) return;
                mPlaneRadioFeedback = value;
                OnPropertyChanged(nameof(PlaneRadioFeedback));
            }
        }

        private bool mUpdateScrollView = true;

        public bool UpdateScrollView
        {
            get { return mUpdateScrollView; }
            set
            {
                if (value == mUpdateScrollView) return;
                mUpdateScrollView = value;
                OnPropertyChanged(nameof(UpdateScrollView));
            }
        }

        private bool mElevatorTrimSelected = true;

        public bool ElevatorTrimSelected
        {
            get { return mElevatorTrimSelected; }
            set
            {
                if (value == mElevatorTrimSelected) return;
                mElevatorTrimSelected = value;
                OnPropertyChanged(nameof(ElevatorTrimSelected));
            }
        }

        private bool mRudderTrimSelected;

        public bool RudderTrimSelected
        {
            get { return mRudderTrimSelected; }
            set
            {
                if (value == mRudderTrimSelected) return;
                mRudderTrimSelected = value;
                OnPropertyChanged(nameof(RudderTrimSelected));
            }
        }

        private bool mAileronLeftTrimSelected;

        public bool AileronLeftTrimSelected
        {
            get { return mAileronLeftTrimSelected; }
            set
            {
                if (value == mAileronLeftTrimSelected) return;
                mAileronLeftTrimSelected = value;
                OnPropertyChanged(nameof(AileronLeftTrimSelected));
            }
        }

        private bool mAileronRightTrimSelected;

        public bool AileronRightTrimSelected
        {
            get { return mAileronRightTrimSelected; }
            set
            {
                if (value == mAileronRightTrimSelected) return;
                mAileronRightTrimSelected = value;
                OnPropertyChanged(nameof(AileronRightTrimSelected));
            }
        }


        private bool mAileronsBothTrimSelected;

        public bool AileronsBothTrimSelected
        {
            get { return mAileronsBothTrimSelected; }
            set
            {
                if (value == mAileronsBothTrimSelected) return;
                mAileronsBothTrimSelected = value;
                OnPropertyChanged(nameof(AileronsBothTrimSelected));
            }
        }

        private bool mControllerStatus;

        public bool ControllerStatus
        {
            get { return mControllerStatus; }
            set
            {
                if (value == mControllerStatus) return;
                mControllerStatus = value;
                OnPropertyChanged(nameof(ControllerStatus));
            }
        }

        private int mTransmitterRadioPALevel = 0;

        public int TransmitterRadioPALevel
        {
            get { return mTransmitterRadioPALevel; }
            set
            {
                if (value == mTransmitterRadioPALevel) return;
                mTransmitterRadioPALevel = value;
                OnPropertyChanged(nameof(TransmitterRadioPALevel));
            }
        }


        private int mRadioChannel = 79;

        public int RadioChannel
        {
            get { return mRadioChannel; }
            set
            {
                if (value == mRadioChannel) return;
                mRadioChannel = value;
                OnPropertyChanged(nameof(RadioChannel));
            }
        }

        public GamepadButtonFlags ThrottleSetpoint1Button
        {
            get { return cThrottleSetpoint1Button; }
        }

        public GamepadButtonFlags ThrottleSetpoint2Button
        {
            get { return cThrottleSetpoint2Button; }
        }

        #region Timer callbacks

        private void ReadTimerCallbackFcn(object aArg)
        {
            var lNow = DateTime.Now;
            try
            {
                mReadTimerTimes.Enqueue(lNow);

                //int lFramerate = mCameraAdapter != null ? mCameraAdapter.Framerate : 0;
                var lFramerate = 0.0;
                CameraFramerate = 0.8 * CameraFramerate + 0.2 * lFramerate;

                if (BatteryMonitor.BatteryLevel() <= 0.15 && lNow - mLastBatteryAnnounceTime > TimeSpan.FromSeconds(20))
                {
                    mAnnouncer.AnnounceBatteryLevel(BatteryMonitor.BatteryLevel());
                    mLastBatteryAnnounceTime = lNow;
                }

                if (lNow - mLastBatteryReading > TimeSpan.FromSeconds(2))
                {
                    PlaneBattery = -1;
                    PlaneBatteryVoltageRaw = Double.NaN;
                }
                else
                {
                    var lVoltage = PlaneBatteryVoltage;

                    double lNewCapacity = -1;
                    if (lVoltage >= 0)
                    {
                        lNewCapacity = (lVoltage - BatteryMinVoltage) / (BatteryMaxVoltage - BatteryMinVoltage);
                        if (lNewCapacity < 0) lNewCapacity = 0;
                        if (lNewCapacity > 1) lNewCapacity = 1;
                    }

                    if (lNewCapacity >= 0 && PlaneBattery >= 0)
                    {
                        const double lA = 0.8;
                        const double lB = 1 - lA;
                        PlaneBattery = lA * PlaneBattery + lB * lNewCapacity;
                    }
                    else
                    {
                        PlaneBattery = lNewCapacity;
                    }
                }

                if (lNow - mLastPlaneFeedback > TimeSpan.FromSeconds(1))
                {
                    PlaneRadioFeedback = false;
                    ResetPlaneFeedbackData();
                }

                if (lNow - mLastGPSUpdate > TimeSpan.FromSeconds(5))
                {
                    OrientationViewModel.GPSData.Invalidate();
                }

                // update port list
                if (lNow - mLastPortListUpdateTime > mPortListUpdatePeriod)
                {
                    List<string> lPortList = SerialPort.GetPortNames().ToList();

                    var lToAdd = lPortList.Except(mPortList).ToList();
                    var lToRemove = mPortList.Except(lPortList).ToList();

                    if (lToAdd.Count > 0 || lToRemove.Count > 0)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            lToAdd.ForEach(a => PortList.Add(a));
                            lToRemove.ForEach(a => PortList.Remove(a));
                        });
                    }

                    mLastPortListUpdateTime = lNow;
                }

                if (PortName == null && PortList.Count > 0)
                    PortName = PortList[0];

                // update camera list
                try
                {
                    if (lNow - mLastCameraListUpdateTime > mCameraListUpdatePeriod)
                    {
                        var lCameraDevices = mCameraSelector.GetAvailableCameras();
                        //var lCameraDevices = CameraAdapter.GetAvailableCameraDevices();

                        var lToAdd = lCameraDevices.Except(CameraList, StringComparer.InvariantCultureIgnoreCase)
                            .ToList();
                        var lToRemove = CameraList.Except(lCameraDevices, StringComparer.InvariantCultureIgnoreCase)
                            .ToList();

                        if (lToAdd.Count > 0 || lToRemove.Count > 0)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                lToAdd.ForEach(a => CameraList.Add(a));
                                lToRemove.ForEach(a => CameraList.Remove(a));
                            });
                        }

                        if (SelectedCamera == null && CameraList.Count > 0)
                            SelectedCamera = CameraList[0];

                        if (!CameraList.Contains(SelectedCamera))
                            SelectedCamera = null;

                        mLastCameraListUpdateTime = lNow;
                    }
                }
                catch (Exception aEx)
                {
                    Console.WriteLine("Error when updating camera list: {0}", aEx.Message);
                }

                bool lCreateNewPort = false;
                if (mPort == null && mPortName != null) lCreateNewPort = true;
                if (mPort != null && mPortName != null && mPort.PortName != mPortName)
                {
                    if (mPort.IsOpen) mPort.Close();
                    lCreateNewPort = true;
                }

                if (lCreateNewPort)
                {
                    Console.WriteLine("Opening new port: {0}", mPortName);
                    mSerialLogFile.WriteLine("Opening new port: {0}", mPortName);
                    mPort = new SerialPort(mPortName, mBaudRate);
                    mPort.WriteTimeout = 80;
                }

                bool lOpening = false;
                if (mPort != null && !mPort.IsOpen)
                {
                    lOpening = true;
                    try
                    {
                        //Console.WriteLine("Trying to open port ...");
                        mPort.Open();
                    }
                    catch (Exception)
                    {
                    }
                }

                var lNewMessages = new List<string>();

                if ((lNow - mLastWriteToPlaneFailTime).TotalSeconds > 2 && PortOpen)
                {
                    PlaneWrite = true;
                }
                else PlaneWrite = false;

                if (mPort != null && mPort.IsOpen)
                {
                    if (lOpening)
                    {
                        Console.WriteLine("Port successfully open");
                        mSerialLogFile.WriteLine("Port successfully open");
                        mLockThrottle = true;
                    }
                    else
                        PortOpen = true; // enable this flag only if the port was already open before (in previous loop)

                    var lText = mPort.ReadExisting();

                    if (lText.Length > 0)
                    {
                        mSerialLogFile.WriteLine("Time");
                        mSerialLogFile.WriteLine(
                            (lNow - cAppStartTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture));
                        mSerialLogFile.WriteLine("Text");
                        mSerialLogFile.WriteLine(lText);
                        mSerialLogFile.Flush();
                    }

                    var lTextToPrint = "";

                    foreach (char lC in lText)
                    {
                        if (lC == '!')
                        {
                            mLastControlArduinoFeedbackTime = lNow;
                            while (mWriteSuccesFeedbackTimes.Count >= mWriteSuccessFeedbackTimesSize)
                                mWriteSuccesFeedbackTimes.Dequeue();
                            mWriteSuccesFeedbackTimes.Enqueue(lNow);
                        }
                        else if (lC == '?')
                            mPortLines.Add("--radio failure--\r\n");
                        else if (lC == '+')
                        {
                            Interlocked.Increment(ref mWriteToPlaneFailCount);
                            mLastWriteToPlaneFailTime = lNow;
                        }
                        else if (lC == ':')
                        {
                            Interlocked.Increment(ref mPlaneFeedbackCount);
                            PlaneRadioFeedback = true;
                            mLastPlaneFeedback = lNow;
                            while (mRadioFeedbackTimes.Count >= cRadioFeedbackTimesSize)
                                mRadioFeedbackTimes.Dequeue();
                            mRadioFeedbackTimes.Enqueue(lNow);
                        }
                        else if (lC == '<' || lC == '>' || mCurrentSerialReceiveMessage != null)
                        {
                        }
                        else lTextToPrint += lC;

                        if (lC == '<')
                        {
                            // if there is an existing message already, it gets discarded here
                            mCurrentSerialReceiveMessage = "";
                        }
                        else if (lC == '>')
                        {
                            if (mCurrentSerialReceiveMessage != null)
                                lNewMessages.Add(mCurrentSerialReceiveMessage);
                            mCurrentSerialReceiveMessage = null;
                        }

                        if (mCurrentSerialReceiveMessage != null && lC != '<' && lC != '>')
                            mCurrentSerialReceiveMessage += lC;
                    }

                    if (mWriteSuccesFeedbackTimes.Count <= 0 ||
                        lNow - mLastControlArduinoFeedbackTime > TimeSpan.FromSeconds(0.8))
                    {
                        PortOpen = false;
                    }

                    if (mUpdateScrollView)
                    {
                        if (lTextToPrint.Length > 0)
                        {
                            if ((lNow - mLastLogWindowTimeStampTime).TotalMinutes > 2)
                            {
                                mPortLines.Add("Time stamp: " + lNow.ToString() + "\n");
                            }

                            mPortLines.Add(lTextToPrint);

                            while (mPortLines.Count > 100)
                                mPortLines.RemoveAt(0);

                            Text = String.Join(String.Empty, mPortLines);
                        }
                    }
                }
                else PortOpen = false;

                if (!PortOpen)
                {
                    mLockThrottle = true;
                }

                while (mRadioFeedbackTimes.Count > 0 &&
                       lNow - mRadioFeedbackTimes.Peek() >= TimeSpan.FromSeconds(2))
                    mRadioFeedbackTimes.Dequeue();


                if (mRadioFeedbackTimes.Count >= 2)
                {
                    RadioFeedbackFrequency =
                        mRadioFeedbackTimes.Count / (lNow - mRadioFeedbackTimes.Peek()).TotalSeconds;
                }
                else RadioFeedbackFrequency = 0;

                foreach (var nMessage in lNewMessages)
                    ProcessReceivedSerialMessage(nMessage, lNow);

                while (mWriteSuccesFeedbackTimes.Count > 200
                       ||
                       (mWriteSuccesFeedbackTimes.Count > 0 &&
                        lNow - mWriteSuccesFeedbackTimes.Peek() > TimeSpan.FromSeconds(4)))
                {
                    mWriteSuccesFeedbackTimes.Dequeue();
                }

                if (mWriteSuccesFeedbackTimes.Count > 0)
                    WriteSuccessFrequency = mWriteSuccesFeedbackTimes.Count /
                                            (lNow - mWriteSuccesFeedbackTimes.Peek()).TotalSeconds;
                else
                    WriteSuccessFrequency = 0;

                while (mReadTimerTimes.Count > 0 && lNow - mReadTimerTimes.Peek() > TimeSpan.FromSeconds(4))
                {
                    mReadTimerTimes.Dequeue();
                }

                if (mReadTimerTimes.Count > 1)
                {
                    ReadTimerFrequency = mReadTimerTimes.Count / (lNow - mReadTimerTimes.Peek()).TotalSeconds;
                }
                else ReadTimerFrequency = 0;
            }
            catch (Exception lEx)
            {
                Console.WriteLine("Exception in read timer: {0}", lEx.Message);
            }

            if (!mAppClosing)
            {
                var lElapsed = (int)(DateTime.Now - lNow).TotalMilliseconds;
                var lNewDelay = Math.Max(cReadTimerPeriod - lElapsed, 0);
                mReadTimer.Change(lNewDelay, Timeout.Infinite);
            }
        }

        private void WriteTimerCallbackFcn(object aArg)
        {
            var lNow = DateTime.Now;
            try
            {
                HashSet<Key> lKeysDown = null;
                lock (mKeysDown)
                {
                    lKeysDown = mKeysDown.ToHashSet();
                }

                bool lWasConnected = ControllerStatus;

                if (Controller.IsConnected)
                {
                    ControllerStatus = true;

                    if (!lWasConnected) ControlValues.ThrottleLocked = true;
                }
                else
                {
                    ControllerStatus = false;
                    ControlValues.ThrottleLocked = true;
                }

                if (!ControllerStatus && lWasConnected)
                {
                    ControlValues.Ailerons = 0;
                    ControlValues.Elevator = 0;
                    ControlValues.Rudder = 0;
                }

                if (Controller.IsConnected)
                {
                    // handle controller axes
                    var lSquareFlags = SquareStick.None;
                    if (SquareLeftStick) lSquareFlags |= SquareStick.Left;
                    if (SquareRightStick) lSquareFlags |= SquareStick.Right;

                    ControlValues.Ailerons = Controller.GetAxisValue(ControlValues.AileronsAxis, lSquareFlags);

                    ControlValues.Elevator = Controller.GetAxisValue(ControlValues.ElevatorAxis, lSquareFlags);

                    ControlValues.Rudder = Controller.GetAxisValue(ControlValues.RudderAxis, lSquareFlags);

                    var lThrottleAxisValue = Controller.GetAxisValue(ControlValues.ThrottleAxis, lSquareFlags);

                    if (ControlValues.ThrottleAxis.Axis is JoystickAxes &&
                        (JoystickAxes)ControlValues.ThrottleAxis.Axis == JoystickAxes.ThrottleLever)
                    {
                        ControlValues.Throttle = lThrottleAxisValue;
                    }
                    else
                    {
                        const double lThrottleGain = 0.015;

                        var lThrottleNew = ControlValues.Throttle + lThrottleGain * lThrottleAxisValue;

                        if (lThrottleNew < 0) lThrottleNew = 0;
                        if (lThrottleNew > 1) lThrottleNew = 1;

                        ControlValues.Throttle = lThrottleNew;
                    }

                    var lButtons = Controller.GetActiveButtons();
                    var lButtonsPressed = Controller.GetActiveButtonsPressed();
                    var lButtonSettings = Controller.ButtonSettings[Controller.ActiveType];
                    var lButtonSettingsFixed = Controller.ButtonSettingsFixed[Controller.ActiveType];

                    // handle buttons
                    // IMPORTANT! The indices used for the button arrays have to match with the established naming of the actions in PlaneController

                    // time checkpoint
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.TimeCheckpoint].Button) > 0)
                    {
                        TimeCheckpointTextBlockVisible = true;
                        mTimeCheckpointCount++;
                        mAnnouncer.AnnounceTimeCheckpoint();

                        Task.Run(() =>
                        {
                            Thread.Sleep(400);
                            Dispatcher.CurrentDispatcher.Invoke(() => { TimeCheckpointTextBlockVisible = false; });
                        });
                    }

                    // switch plane feedback
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.PlaneFeedback].Button) > 0)
                    {
                        EnablePlaneFeedback = !EnablePlaneFeedback;
                    }

                    // switch flaperons position
                    if ((lButtonsPressed & (ushort)lButtonSettings[PlaneController.ButtonSettingType.Flaps].Button) > 0)
                    {
                        if (ControlValues.FlapModeNeutral) ControlValues.FlapModeDown1 = true;
                        else if (ControlValues.FlapModeDown1) ControlValues.FlapModeDown2 = true;
                        else if (ControlValues.FlapModeDown2) ControlValues.FlapModeNeutral = true;
                        AnnounceFlapMode();
                    }

                    // full throttle
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.ThrottleFull].Button) > 0)
                    {
                        ControlValues.Throttle = 1.0;
                    }

                    // zero throttle
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.ThrottleZero].Button) > 0)
                    {
                        ControlValues.Throttle = 0.0;
                    }

                    // throttle setpoint 1
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.ThrottleSetpoint1].Button) > 0)
                    {
                        ControlValues.Throttle = ControlValues.ThrottleSetpoint1;
                    }

                    // throttle setpoint 2
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.ThrottleSetpoint2].Button) > 0)
                    {
                        ControlValues.Throttle = ControlValues.ThrottleSetpoint2;
                    }

                    // trim down/left
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.TrimDownOrLeft].Button) > 0)
                    {
                        if (ElevatorTrimSelected) TrimValues.ElevatorTrimDown.Execute(null);
                        if (RudderTrimSelected) TrimValues.RudderTrimLeft.Execute(null);
                        if (AileronLeftTrimSelected) TrimValues.AileronLeftTrimDown.Execute(null);
                        if (AileronRightTrimSelected) TrimValues.AileronRightTrimDown.Execute(null);
                        if (AileronsBothTrimSelected)
                        {
                            TrimValues.AileronLeftTrimUp.Execute(null);
                            TrimValues.AileronRightTrimDown.Execute(null);
                        }
                    }

                    // trim up/right
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.TrimUpOrRight].Button) > 0)
                    {
                        if (ElevatorTrimSelected) TrimValues.ElevatorTrimUp.Execute(null);
                        if (RudderTrimSelected) TrimValues.RudderTrimRight.Execute(null);
                        if (AileronLeftTrimSelected) TrimValues.AileronLeftTrimUp.Execute(null);
                        if (AileronRightTrimSelected) TrimValues.AileronRightTrimUp.Execute(null);
                        if (AileronsBothTrimSelected)
                        {
                            TrimValues.AileronLeftTrimDown.Execute(null);
                            TrimValues.AileronRightTrimUp.Execute(null);
                        }
                    }

                    // trim switch up
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.TrimSwitchUp].Button) > 0)
                    {
                        if (ElevatorTrimSelected)
                        {
                            ElevatorTrimSelected = false;
                            AileronsBothTrimSelected = true;
                        }
                        else if (RudderTrimSelected)
                        {
                            RudderTrimSelected = false;
                            ElevatorTrimSelected = true;
                        }
                        else if (AileronLeftTrimSelected)
                        {
                            AileronLeftTrimSelected = false;
                            RudderTrimSelected = true;
                        }
                        else if (AileronRightTrimSelected)
                        {
                            AileronRightTrimSelected = false;
                            AileronLeftTrimSelected = true;
                        }
                        else if (AileronsBothTrimSelected)
                        {
                            AileronsBothTrimSelected = false;
                            AileronRightTrimSelected = true;
                        }

                        AnnounceTrimSurface();
                    }

                    // trim switch down
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.TrimSwitchDown].Button) > 0)
                    {
                        if (ElevatorTrimSelected)
                        {
                            ElevatorTrimSelected = false;
                            RudderTrimSelected = true;
                        }
                        else if (RudderTrimSelected)
                        {
                            RudderTrimSelected = false;
                            AileronLeftTrimSelected = true;
                        }
                        else if (AileronLeftTrimSelected)
                        {
                            AileronLeftTrimSelected = false;
                            AileronRightTrimSelected = true;
                        }
                        else if (AileronRightTrimSelected)
                        {
                            AileronRightTrimSelected = false;
                            AileronsBothTrimSelected = true;
                        }
                        else if (AileronsBothTrimSelected)
                        {
                            AileronsBothTrimSelected = false;
                            ElevatorTrimSelected = true;
                        }

                        AnnounceTrimSurface();
                    }

                    // trim elevator up
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.TrimElevatorUp].Button) > 0)
                    {
                        TrimValues.ElevatorTrimUp.Execute(null);
                    }

                    // trim elevator down
                    if ((lButtonsPressed &
                         (ushort)lButtonSettings[PlaneController.ButtonSettingType.TrimElevatorDown].Button) > 0)
                    {
                        TrimValues.ElevatorTrimDown.Execute(null);
                    }

                    // throttle lock
                    var lThrottleLockCombination =
                        (ushort)lButtonSettingsFixed[PlaneController.ButtonSettingType.ThrottleLock].Button;
                    if ((lButtonsPressed & (~lThrottleLockCombination)) == 0
                        && (lButtonsPressed & lThrottleLockCombination) > 0
                        && lButtons == lThrottleLockCombination)
                    {
                        if (mPort != null && mPort.IsOpen &&
                            (lThrottleAxisValue == 0.0 || !ControlValues.ThrottleLocked))
                        {
                            ControlValues.ThrottleLocked = !ControlValues.ThrottleLocked;
                        }
                        else
                            ControlValues
                                .AnnounceThrottleLock(); // announce redundantly when throttle is locked and stays locked
                    }
                }

                // handle throttle lock request
                if (mLockThrottle)
                {
                    ControlValues.ThrottleLocked = true;
                }

                mLockThrottle = false;

                // write all values to the array and send to transmitter
                if (mPort != null && mPort.IsOpen)
                {
                    mSourceValues[0] = (float)ControlValues.Elevator;
                    mSourceValues[1] = (float)ControlValues.Rudder;
                    mSourceValues[2] = (float)ControlValues.Ailerons;
                    mSourceValues[3] = (float)ControlValues.Throttle;

                    mMessageBuffer[0] = 255;
                    mMessageBuffer[1] = 0;
                    mMessageBuffer[2] = 127;
                    mMessageBuffer[cMessageSize - 3] = 129;
                    mMessageBuffer[cMessageSize - 2] = 0;
                    mMessageBuffer[cMessageSize - 1] = 255;

                    Buffer.BlockCopy(mSourceValues, 0, mMessageBuffer,
                        3 * sizeof(byte), cSourceValuesLength * sizeof(float));

                    int lBufferPosition = 3 * sizeof(byte) + cSourceValuesLength * sizeof(float);

                    // trim values
                    mMessageBuffer[lBufferPosition + 0 * sizeof(byte)] = TrimValues.ElevatorTrim;
                    mMessageBuffer[lBufferPosition + 1 * sizeof(byte)] = TrimValues.RudderTrim;
                    mMessageBuffer[lBufferPosition + 2 * sizeof(byte)] = TrimValues.AileronLeftTrim;
                    mMessageBuffer[lBufferPosition + 3 * sizeof(byte)] = TrimValues.AileronRightTrim;

                    lBufferPosition += cTrimValuesCount * sizeof(byte);

                    // flap offset
                    short[] lSourceValues2 = new short[1];
                    lSourceValues2[0] = (short)ControlValues.FlapOffset;

                    Buffer.BlockCopy(lSourceValues2, 0, mMessageBuffer,
                        lBufferPosition, 1 * sizeof(short));

                    lBufferPosition += sizeof(short);

                    // enable plane feedback flag
                    mMessageBuffer[lBufferPosition] =
                        EnablePlaneFeedback ? (byte)1 : (byte)0;

                    lBufferPosition += sizeof(byte);

                    // plane control surfaces max angles
                    mMessageBuffer[lBufferPosition + 0 * sizeof(byte)] = (byte)ControlValues.AileronLeftAngle;
                    mMessageBuffer[lBufferPosition + 1 * sizeof(byte)] = (byte)ControlValues.AileronRightAngle;
                    mMessageBuffer[lBufferPosition + 2 * sizeof(byte)] = (byte)ControlValues.ElevatorAngle;
                    mMessageBuffer[lBufferPosition + 3 * sizeof(byte)] = (byte)ControlValues.RudderAngle;

                    lBufferPosition += 4 * sizeof(byte);

                    // radio PA level
                    mMessageBuffer[lBufferPosition++] = (byte)TransmitterRadioPALevel;

                    // radio channel
                    mMessageBuffer[lBufferPosition++] = (byte)RadioChannel;

                    byte lSignalByteAlpha = 0;
                    // reset throttle minutes flag
                    if (ResetThrottleMinutes)
                    {
                        lSignalByteAlpha |= 0b00000001;
                        Console.WriteLine("Reset TM signal sent to the transmitter");
                        ResetThrottleMinutes = false;
                    }

                    if (SaveTrimValues)
                    {
                        lSignalByteAlpha |= 0b00000010;
                        Console.WriteLine("Save trim signal sent to the transmitter");
                        SaveTrimValues = false;
                    }

                    mMessageBuffer[lBufferPosition++] = lSignalByteAlpha;

                    try
                    {
                        mPort.Write(mMessageBuffer, 0, cMessageSize);
                    }
                    catch (Exception lEx)
                    {
                        Console.WriteLine("Exception in serial port write: {0}", lEx.Message);
                    }
                }
                else
                {
                    ControlValues.ThrottleLocked = true;
                }

                mWriteTimerTimes.Enqueue(lNow);

                while (mWriteTimerTimes.Count > 200
                       ||
                       (mWriteTimerTimes.Count > 0 && lNow - mWriteTimerTimes.Peek() > TimeSpan.FromSeconds(2)))
                {
                    mWriteTimerTimes.Dequeue();
                }

                if (mWriteTimerTimes.Count > 0)
                    WriteTimerFrequency = mWriteTimerTimes.Count / (lNow - mWriteTimerTimes.Peek()).TotalSeconds;
                else WriteTimerFrequency = 0;

                TrimValues.ResetTrims();
            }
            catch (Exception lEx)
            {
                Console.WriteLine("Exception in write timer: {0}", lEx.Message);
            }

            if (!mAppClosing)
            {
                var lElapsed = (int)(DateTime.Now - lNow).TotalMilliseconds;
                var lNewDelay = Math.Max(cWriteTimerPeriod - lElapsed, 0);
                mWriteTimer.Change(lNewDelay, Timeout.Infinite);
            }
        }

        private void CameraFastTimerCallbackFcn(object aArg)
        {
            try
            {
                Bitmap lBitmap = null;
                BitmapImage lImage = null;
                lock (mNewCameraFrames)
                {
                    if (mNewCameraFrames.Count > 0)
                    {
                        lBitmap = (Bitmap)mNewCameraFrames.End().Item2.Clone();
                        lImage = lBitmap.ToBitmapImage();
                    }
                }

                if (lImage != null)
                {
                    lImage.Freeze(); // avoid cross thread operations and prevents leaks

                    Dispatcher.CurrentDispatcher.Invoke(() => { OrientationViewModel.CameraImage = lImage; });
                }

                if (lBitmap != null)
                    lBitmap.Dispose();
            }
            catch (Exception lEx)
            {
                Console.WriteLine("Camera fast timer exception: {0}", lEx.Message);
            }

            if (!mAppClosing)
                mCameraFastTimer.Change(cCameraFastTimerPeriod, Timeout.Infinite);
        }

        private void CameraSlowTimerCallbackFcn(object aArg)
        {
            try
            {
                List<Tuple<DateTime, Bitmap>> lNewFrames = new List<Tuple<DateTime, Bitmap>>();
                lock (mNewCameraFrames)
                {
                    lNewFrames = mNewCameraFrames
                        .Select(a => new Tuple<DateTime, Bitmap>(a.Item1, (Bitmap)a.Item2.Clone()))
                        .ToList();
                    foreach (var nItem in mNewCameraFrames)
                        nItem.Item2.Dispose();

                    mNewCameraFrames.Clear();
                }

                //lock (mVideoWriterLock)
                //{
                //	foreach (var nFrame in lNewFrames)
                //	{
                //		if (mVideoWriter == null && !mAppClosing)
                //		{
                //			mVideoWriter = new AVIWriter();
                //			var videoFilePath = cLogDirectory + "/CameraLog_" +
                //				cAppStartDateString +
                //				"_" + mSelectedCamera.Name + "_" +
                //				(mCameraFileIndex++).ToString() + ".mp4";
                //			mVideoWriter.Open(videoFilePath,
                //				nFrame.Item2.Width, nFrame.Item2.Height);

                //			mFirstVideoFrameTime = nFrame.Item1;
                //		}

                //		if (mVideoWriter != null && mRecordCameraToFile)
                //		{
                //			try
                //			{
                //				mVideoWriter.AddFrame(nFrame.Item2);
                //			}
                //			catch (Exception lEx)
                //			{
                //				Console.WriteLine("Exception when writing frame: {0}, stack:", lEx.Message);
                //				Console.WriteLine(lEx.StackTrace);
                //			}
                //		}
                //		nFrame.Item2.Dispose();
                //	}
                //}
            }
            catch (Exception lEx)
            {
                Console.WriteLine("Camera slow timer exception: {0}, stack trace: ", lEx.Message);
                Console.WriteLine(lEx.StackTrace);
            }

            if (!mAppClosing)
                mCameraSlowTimer.Change(cCameraSlowTimerPeriod, Timeout.Infinite);
        }

        private void LogTimerCallbackFcn(object aArg)
        {
            try
            {
                var lTimerStartTime = DateTime.Now;

                // update plot
                try
                {
                    var lNow = DateTime.Now;
                    var lMsFromAppStart = (lNow - cAppStartTime).TotalMilliseconds;

                    var lCurrentSeries = PlotModel.Series[0] as LineSeries;
                    lCurrentSeries.MarkerType = MarkerType.None;
                    //if (mPlotShowPointMarkers) lCurrentSeries.MarkerType = MarkerType.Circle;
                    //else lCurrentSeries.MarkerType = MarkerType.None;
                    var lCurrentSeriesName = lCurrentSeries.Tag as string;
                    if (lCurrentSeriesName == null) lCurrentSeriesName = "";

                    if (lCurrentSeriesName.CompareTo(SelectedDataSeriesPlot) != 0 ||
                        (UpdatePlot && !mUpdatePlotLastChecked))
                    {
                        lCurrentSeries.Points.Clear();
                        if (!mPlotDataBuffer.ContainsKey(SelectedDataSeriesPlot))
                            throw new KeyNotFoundException(String.Format("Series name \"{0}\" is not recognised!",
                                SelectedDataSeriesPlot));

                        lCurrentSeries.Points.AddRange(mPlotDataBuffer[SelectedDataSeriesPlot]);
                        lCurrentSeries.Tag = SelectedDataSeriesPlot;
                    }

                    mUpdatePlotLastChecked = UpdatePlot;

                    const int lId = 1;

                    foreach (var nSeries in LogDataSeries)
                    {
                        // we need to get the last value before asking for current value, otherwise it would get overwritten
                        var lLastValue = nSeries.Value.GetLastValue(lId);
                        // get the current value of the property
                        var lNewValue = nSeries.Value.GetValue(lId);
                        if (nSeries.Value.IsCumulativeFlag)
                        {
                            if (Double.IsNaN(lLastValue))
                                lNewValue = 0; // this one is technically wrong but it's only for one datapoint
                            else lNewValue -= lLastValue;
                        }

                        var lThisSeriesBuffer = mPlotDataBuffer[nSeries.Key];

                        var lDataPoint = new DataPoint(lMsFromAppStart / 1000, lNewValue);
                        lThisSeriesBuffer.Enqueue(lDataPoint);

                        while (lThisSeriesBuffer.Count > 0 &&
                               lMsFromAppStart / 1000 - lThisSeriesBuffer.Peek().X > MaxPlotDataRange + 1)
                            lThisSeriesBuffer.Dequeue();

                        if (nSeries.Key.CompareTo(SelectedDataSeriesPlot) == 0 && UpdatePlot)
                        {
                            lCurrentSeries.Points.Add(lDataPoint);

                            while (lCurrentSeries.Points.Count > 0 &&
                                   lMsFromAppStart / 1000 - lCurrentSeries.Points[0].X > MaxPlotDataRange + 1)
                                lCurrentSeries.Points.RemoveAt(0);
                        }
                    }

                    foreach (var nAxis in PlotModel.Axes)
                    {
                        nAxis.MajorGridlineStyle = LineStyle.Solid;
                    }

                    var lXAxis = PlotModel.Axes.Where(a => a.IsHorizontal()).FirstOrDefault();
                    var lXAxisRange = PlotXAxisRange;
                    if (lXAxis != null)
                    {
                        if (XAxisLive)
                        {
                            lXAxis.Minimum = lMsFromAppStart / 1000 - lXAxisRange;
                            lXAxis.Maximum = lMsFromAppStart / 1000;
                            lXAxis.IsZoomEnabled = false;
                            lXAxis.IsPanEnabled = false;
                            lXAxis.Reset();
                        }
                        else
                        {
                            lXAxis.IsZoomEnabled = true;
                            lXAxis.IsPanEnabled = true;
                        }
                    }

                    var lYAxis = PlotModel.Axes.Where(a => a.IsVertical()).FirstOrDefault();
                    if (lYAxis != null)
                    {
                        lYAxis.IsZoomEnabled = false;
                        lYAxis.IsPanEnabled = false;
                        if (PlotYAxisMinAuto) lYAxis.Minimum = Double.NaN;
                        else lYAxis.Minimum = PlotYAxisMin;
                        if (PlotYAxisMaxAuto) lYAxis.Maximum = Double.NaN;
                        else lYAxis.Maximum = PlotYAxisMax;
                        lYAxis.Reset();
                    }

                    PlotModel.InvalidatePlot(true);
                }
                catch (Exception lEx)
                {
                    Console.WriteLine("Exception when updating data plot: {0}", lEx.Message);
                }

                // write to log file
                try
                {
                    var lNow = DateTime.Now;
                    var lDataValues = new double[LogDataSeries.Count];

                    int lCount = 0;
                    foreach (var nItem in LogDataSeries)
                    {
                        lDataValues[lCount++] = nItem.Value.GetValue();
                        mLogStringBuilder.Append(",");
                        mLogStringBuilder.Append(nItem.Value.GetValue().ToString(CultureInfo.InvariantCulture));
                    }

                    var lDataPoint = new Tuple<DateTime, double[]>(lNow, lDataValues);
                    mFileLogDataBuffer.Enqueue(lDataPoint);

                    if ((lNow - mLastLogFileWriteTime).TotalMilliseconds > cFileLogWritePeriod)
                    {
                        mLogStringBuilder.Clear();

                        foreach (var nPoint in mFileLogDataBuffer)
                        {
                            mLogStringBuilder.Append(
                                (nPoint.Item1 - cAppStartTime).TotalMilliseconds
                                .ToString(CultureInfo.InvariantCulture));

                            foreach (var nValue in nPoint.Item2)
                            {
                                mLogStringBuilder.Append(",");
                                mLogStringBuilder.Append(nValue.ToString(CultureInfo.InvariantCulture));
                            }

                            mLogStringBuilder.Append(Environment.NewLine);
                        }

                        mLogFile.Write(mLogStringBuilder.ToString());
                        mLastLogFileWriteTime = lNow;
                        mFileLogDataBuffer.Clear();
                    }
                }
                catch (Exception lEx)
                {
                    Console.WriteLine("Exception when writing to log file: {0}", lEx.Message);
                }
            }
            catch (Exception lEx)
            {
                Console.WriteLine("Log timer exception: {0}, stack trace: ", lEx.Message);
                Console.WriteLine(lEx.StackTrace);
            }

            if (!mAppClosing)
                mLogTimer.Change(cLogTimerPeriod, Timeout.Infinite);
        }

        #endregion

        #region Commands

        private ICommand mKeyDown;

        public ICommand KeyDown
        {
            get
            {
                if (mKeyDown == null)
                {
                    mKeyDown = new RelayCommand(a =>
                    {
                        var lKeyArgs = a as KeyEventArgs;

                        lock (mKeysDown)
                        {
                            mKeysDown.Add(lKeyArgs.Key);
                        }

                        if (SelectedTabHeader == cOrientationTabHeader)
                        {
                            mOrientationViewModel.KeyDown(lKeyArgs);
                        }

                        if (lKeyArgs.Key == Key.Space)
                        {
                            ControlValues.AllZeroCommand.Execute(null);
                        }
                    });
                }

                return mKeyDown;
            }
        }

        private ICommand mKeyUp;

        public ICommand KeyUp
        {
            get
            {
                if (mKeyUp == null)
                {
                    mKeyUp = new RelayCommand(a =>
                    {
                        var lKeyArgs = a as KeyEventArgs;

                        lock (mKeysDown)
                        {
                            mKeysDown.Remove(lKeyArgs.Key);
                        }

                        if (SelectedTabHeader == cOrientationTabHeader)
                        {
                            mOrientationViewModel.KeyUp(lKeyArgs);
                        }
                    });
                }

                return mKeyUp;
            }
        }

        private ICommand mWindowClosing;

        public ICommand WindowClosing
        {
            get
            {
                if (mWindowClosing == null)
                {
                    mWindowClosing = new RelayCommand(a =>
                    {
                        mAppClosing = true;

                        mReadTimer.Dispose();
                        mWriteTimer.Dispose();
                        mCameraFastTimer.Dispose();
                        mCameraSlowTimer.Dispose();

                        mLogFile.Dispose();
                        mSerialLogFile.Dispose();
                        Controller.Dispose();

                        mOrientationViewModel.WindowClosing();

                        //lock (mVideoWriter)
                        //{
                        //	mVideoWriter.Close();
                        //}

                        //mCameraAdapter?.Stop();
                    });
                }

                return mWindowClosing;
            }
        }


        private ICommand mClearSerialLogWindow;

        public ICommand ClearSerialLogWindow
        {
            get
            {
                if (mClearSerialLogWindow == null)
                {
                    mClearSerialLogWindow = new RelayCommand(a =>
                    {
                        Text = "";
                        mPortLines.Clear();
                    });
                }

                return mClearSerialLogWindow;
            }
        }

        private ICommand mSetBaseLocation;

        public ICommand SetBaseLocation
        {
            get
            {
                if (mSetBaseLocation == null)
                {
                    mSetBaseLocation = new RelayCommand(a =>
                    {
                        if (!Double.IsNaN(OrientationViewModel.GPSData.Longitude) &&
                            !Double.IsNaN(OrientationViewModel.GPSData.Latitude))
                        {
                            OrientationViewModel.BaseLongitude = OrientationViewModel.GPSData.Longitude;
                            OrientationViewModel.BaseLatitude = OrientationViewModel.GPSData.Latitude;
                        }
                    });
                }

                return mSetBaseLocation;
            }
        }

        private ICommand mClearBaseLocation;

        public ICommand ClearBaseLocation
        {
            get
            {
                if (mClearBaseLocation == null)
                {
                    mClearBaseLocation = new RelayCommand(a =>
                    {
                        OrientationViewModel.BaseLongitude = Double.NaN;
                        OrientationViewModel.BaseLatitude = Double.NaN;
                    });
                }

                return mClearBaseLocation;
            }
        }

        private ICommand mTestCommand;

        public ICommand TestCommand
        {
            get
            {
                if (mTestCommand == null)
                {
                    mTestCommand = new RelayCommand(a => { });
                }

                return mTestCommand;
            }
        }

        #endregion
    }
}