﻿using LibraryWpf;
using System.Collections.Generic;

namespace BloodflyControlUI
{
    public class ButtonSetting : BaseViewModel
    {
        private string mActionName;

        public string ActionName
        {
            get { return mActionName; }
            set
            {
                if (value == mActionName) return;
                mActionName = value;
                OnPropertyChanged(nameof(ActionName));
            }
        }

        private object mButton;

        public object Button
        {
            get { return mButton; }
            set
            {
                if (value == mButton) return;
                mButton = value;
                OnPropertyChanged(nameof(Button));
            }
        }

        private List<object> mButtonOptions;

        public List<object> ButtonOptions
        {
            get { return mButtonOptions; }
            set
            {
                if (value == mButtonOptions) return;
                mButtonOptions = value;
                OnPropertyChanged(nameof(ButtonOptions));
            }
        }
    }
}