﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace BloodflyControlUI
{
    /// <summary>
    /// Interaction logic for MapControl.xaml
    /// </summary>
    public partial class MapControl : UserControl
    {
        const double cPathThickness = 4.0;

        public ObservableCollection<Vector> MapPoints
        {
            get { return (ObservableCollection<Vector>)GetValue(MapPointsProperty); }
            set { SetValue(MapPointsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MapPoints.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MapPointsProperty =
            DependencyProperty.Register("MapPoints", typeof(ObservableCollection<Vector>), typeof(MapControl),
                new PropertyMetadata(new ObservableCollection<Vector>())
                {
                    PropertyChangedCallback = MapPointsChangedCallback,
                });

        public static void MapPointsChangedCallback(DependencyObject aD, DependencyPropertyChangedEventArgs aE)
        {
            var lControl = aD as MapControl;
            if (lControl == null) return;

            var lOldCollection = aE.OldValue as ObservableCollection<Vector>;
            var lNewCollection = aE.NewValue as ObservableCollection<Vector>;

            if (lOldCollection != null) lOldCollection.CollectionChanged -= lControl.MapPointsCollectionChanged;
            if (lNewCollection != null) lNewCollection.CollectionChanged += lControl.MapPointsCollectionChanged;

            lControl.RefreshPath(lOldCollection, lNewCollection, NotifyCollectionChangedAction.Remove);
        }

        private void RefreshPath(System.Collections.IList aOldItems, System.Collections.IList aNewItems,
            NotifyCollectionChangedAction aAction)
        {
            var lToAdd = new List<Vector>();

            if (aAction == NotifyCollectionChangedAction.Add && (aOldItems == null || aOldItems.Count == 0))
            {
                if (aNewItems != null)
                {
                    foreach (var nPoint in aNewItems)
                        lToAdd.Add((Vector)nPoint);
                }
            }
            else
            {
                mPath.Points.Clear();

                foreach (var nPoint in MapPoints)
                    lToAdd.Add(nPoint);
            }

            foreach (var nPoint in lToAdd)
            {
                var lPoint = new GMap.NET.PointLatLng(nPoint.X, nPoint.Y);
                mPath.Points.Add(lPoint);
            }

            if (mPath.Points.Count > 0)
            {
                if (!mMap.Markers.Contains(mPathEndPoint))
                    mMap.Markers.Add(mPathEndPoint);

                mPathEndPoint.Position = mPath.Points[mPath.Points.Count - 1];
                mPathEndPoint.Shape = mPathEndPointShape;
            }
            else
            {
                if (mMap.Markers.Contains(mPathEndPoint))
                    mMap.Markers.Remove(mPathEndPoint);
            }

            if (mPath.Shape is Path)
            {
                (mPath.Shape as Path).StrokeThickness = cPathThickness;
                (mPath.Shape as Path).Stroke = mPathBrush;
            }

            //mPath.RegenerateShape(mMap);
            mPathPointCountTextBlock.Text = mPath.Points.Count.ToString();
        }

        private void MapPointsCollectionChanged(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RefreshPath(e.OldItems, e.NewItems, e.Action);
        }

        private GMap.NET.WindowsPresentation.GMapRoute mPath;
        private Brush mPathBrush;
        private Shape mPathEndPointShape;
        private GMap.NET.WindowsPresentation.GMapMarker mPathEndPoint;

        public MapControl()
        {
            InitializeComponent();

            mMap.CacheLocation = "MapCache/.";
            mMap.Manager.Mode = GMap.NET.AccessMode.ServerAndCache;
            //mMap.MapProvider = GMap.NET.MapProviders.GMapProviders.CzechMap;
            mMap.ShowCenter = false;
            mMap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionWithoutCenter;

            mMapTypeComboBox.ItemsSource = Enum.GetValues(typeof(MapType)).Cast<MapType>().ToList();

            mMap.OnMapZoomChanged += MMap_OnMapZoomChanged;

            Console.WriteLine("Map library cache location: {0}", mMap.CacheLocation);

            mPath = new GMap.NET.WindowsPresentation.GMapRoute(new List<GMap.NET.PointLatLng>());

            mPath.ZIndex = MapPoints.Count;

            mMap.Markers.Add(mPath);

            if (mPathColorPicker.SelectedColor != null)
                mPathBrush = new SolidColorBrush(mPathColorPicker.SelectedColor.Value);
            else
                mPathBrush = new SolidColorBrush(Colors.Black);

            mPath.Shape = new Path
            {
                Stroke = mPathBrush,
                StrokeThickness = cPathThickness
            };

            mPathEndPointShape = new Ellipse { Height = 2 * cPathThickness, Width = 2 * cPathThickness };
            mPathEndPointShape.Stroke = Brushes.Red;
            mPathEndPointShape.RenderTransform = new TranslateTransform(-cPathThickness, -cPathThickness);

            mPathEndPoint = new GMap.NET.WindowsPresentation.GMapMarker(new GMap.NET.PointLatLng(0, 0));

            mPlaceComboBox.ItemsSource = new List<MapPlace>
            {
                new MapPlace
                {
                    Name = "Výškovice",
                    MapZoom = 16,
                    MapPosition = new Vector(49.78085889279642, 18.207871030293287)
                }
            };
        }

        private void MMap_OnMapZoomChanged()
        {
            mZoomLevelTextBlock.Text = mMap.Zoom.ToString();
        }

        private void mMapTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var lBox = sender as ComboBox;

            if (lBox.SelectedItem != null)
            {
                var lSelected = (MapType)lBox.SelectedItem;

                if (lSelected == MapType.SeznamNormal)
                    mMap.MapProvider = GMap.NET.MapProviders.GMapProviders.CzechMap;
                else if (lSelected == MapType.SeznamSatelite)
                    mMap.MapProvider = GMap.NET.MapProviders.GMapProviders.CzechSatelliteMap;
                else if (lSelected == MapType.GoogleNormal)
                    mMap.MapProvider = GMap.NET.MapProviders.GMapProviders.GoogleMap;
                else if (lSelected == MapType.GoogleSatelite)
                    mMap.MapProvider = GMap.NET.MapProviders.GMapProviders.GoogleSatelliteMap;
                else throw new Exception("Invalid enum value!");
            }
        }

        private void mLoadButton_Click(object sender, RoutedEventArgs e)
        {
            mMap.ShowImportDialog();
        }

        private void mSaveButton_Click(object sender, RoutedEventArgs e)
        {
            mMap.ShowExportDialog();
        }

        private void mCacheAreaButton_Click(object sender, RoutedEventArgs e)
        {
            var lArea = mMap.SelectedArea;

            if (!lArea.IsEmpty)
            {
                const int lMinZoom = 1;
                const int lMaxZoom = 20;
                for (int i = lMinZoom; i <= lMaxZoom; i++)
                {
                    Console.WriteLine("Caching level map {0} ({1} - {2})", i, lMinZoom, lMaxZoom);
                    Dispatcher.Invoke(() =>
                    {
                        var lPrefetcher = new GMap.NET.WindowsPresentation.TilePrefetcher();
                        lPrefetcher.Owner = Window.GetWindow(this);
                        lPrefetcher.ShowCompleteMessage = false;
                        lPrefetcher.Start(lArea, i, mMap.MapProvider, 100);
                    });
                }

                Console.WriteLine("Map caching complete");
            }
        }

        private void mClearPathButton_Click(object sender, RoutedEventArgs e)
        {
            MapPoints.Clear();
        }

        private void mPathColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            if (mPathColorPicker.SelectedColor != null)
                mPathBrush = new SolidColorBrush(mPathColorPicker.SelectedColor.Value);
        }

        private void mPlaceComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GoToSelectedPlace();
        }

        private void GoToSelectedPlace()
        {
            var lSelectedItem = mPlaceComboBox.SelectedItem;
            if (lSelectedItem != null && lSelectedItem is MapPlace)
            {
                var lPlace = (MapPlace)lSelectedItem;
                mMap.Position = new GMap.NET.PointLatLng(lPlace.MapPosition.X, lPlace.MapPosition.Y);
                if (lPlace.MapZoom > 0)
                    mMap.Zoom = lPlace.MapZoom;
            }
        }

        private void mGoToPlaceButton_Click(object sender, RoutedEventArgs e)
        {
            GoToSelectedPlace();
        }

        private void mZoomInButton_Click(object sender, RoutedEventArgs e)
        {
            mMap.Zoom++;
        }

        private void mZoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            mMap.Zoom--;
        }
    }
}