﻿using LibraryWpf;
using SharpDX.XInput;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace BloodflyControlUI
{
    public class PlaneController : BaseViewModel, IDisposable
    {
        public enum ControllerType
        {
            Gamepad,
            Joystick
        }

        private readonly JoystickWrapper mJoystick = new JoystickWrapper();
        private readonly GamepadWrapper mGamepad = new GamepadWrapper(UserIndex.One);

        private List<object> mEmptyAxesOptions = new List<object>();

        private List<object> mGamepadAxesOptions = Enum.GetValues(typeof(GamepadAxes)).Cast<object>().ToList();
        private List<object> mJoystickAxesOptions = Enum.GetValues(typeof(JoystickAxes)).Cast<object>().ToList();

        public Dictionary<ControllerType, List<object>> ButtonOptions { get; } =
            new Dictionary<ControllerType, List<object>>
            {
                { ControllerType.Gamepad, Enum.GetValues(typeof(GamepadButtonFlags)).Cast<object>().ToList() },
                {
                    ControllerType.Joystick,
                    Enum.GetValues(typeof(JoystickWrapper.JoystickButtons)).Cast<object>().ToList()
                }
            };

        public enum ButtonSettingType
        {
            TimeCheckpoint,
            PlaneFeedback,
            Flaps,
            ThrottleFull,
            ThrottleZero,
            ThrottleSetpoint1,
            ThrottleSetpoint2,
            TrimDownOrLeft,
            TrimUpOrRight,
            TrimSwitchUp,
            TrimSwitchDown,
            TrimElevatorUp,
            TrimElevatorDown,
            ThrottleLock
        };

        private readonly Dictionary<ButtonSettingType, string> mButtonSettingNames =
            new Dictionary<ButtonSettingType, string>
            {
                { ButtonSettingType.TimeCheckpoint, "Time checkpoint" },
                { ButtonSettingType.PlaneFeedback, "Plane feedback" },
                { ButtonSettingType.Flaps, "Flaps" },
                { ButtonSettingType.ThrottleFull, "Throttle full" },
                { ButtonSettingType.ThrottleZero, "Throttle zero" },
                { ButtonSettingType.ThrottleSetpoint1, "Throttle setpoint 1" },
                { ButtonSettingType.ThrottleSetpoint2, "Throttle setpoint 2" },
                { ButtonSettingType.TrimDownOrLeft, "Trim down/left" },
                { ButtonSettingType.TrimUpOrRight, "Trim up/right" },
                { ButtonSettingType.TrimSwitchUp, "Trim switch up" },
                { ButtonSettingType.TrimSwitchDown, "Trim switch down" },
                { ButtonSettingType.TrimElevatorUp, "Trim elevator up" },
                { ButtonSettingType.TrimElevatorDown, "Trim elevator down" },
                { ButtonSettingType.ThrottleLock, "Throttle lock" }
            };

        public Dictionary<ControllerType, Dictionary<ButtonSettingType, ButtonSetting>> ButtonSettings { get; } =
            new Dictionary<ControllerType, Dictionary<ButtonSettingType, ButtonSetting>>
            {
                {
                    ControllerType.Gamepad,
                    new Dictionary<ButtonSettingType, ButtonSetting>
                    {
                        {
                            ButtonSettingType.TimeCheckpoint,
                            new ButtonSetting { Button = GamepadButtonFlags.RightShoulder }
                        },
                        { ButtonSettingType.PlaneFeedback, new ButtonSetting { Button = GamepadButtonFlags.Back } },
                        { ButtonSettingType.Flaps, new ButtonSetting { Button = GamepadButtonFlags.LeftShoulder } },
                        { ButtonSettingType.ThrottleFull, new ButtonSetting { Button = GamepadButtonFlags.Y } },
                        { ButtonSettingType.ThrottleZero, new ButtonSetting { Button = GamepadButtonFlags.A } },
                        { ButtonSettingType.ThrottleSetpoint1, new ButtonSetting { Button = GamepadButtonFlags.X } },
                        { ButtonSettingType.ThrottleSetpoint2, new ButtonSetting { Button = GamepadButtonFlags.B } },
                        {
                            ButtonSettingType.TrimDownOrLeft, new ButtonSetting { Button = GamepadButtonFlags.DPadLeft }
                        },
                        {
                            ButtonSettingType.TrimUpOrRight, new ButtonSetting { Button = GamepadButtonFlags.DPadRight }
                        },
                        { ButtonSettingType.TrimSwitchUp, new ButtonSetting { Button = GamepadButtonFlags.DPadUp } },
                        {
                            ButtonSettingType.TrimSwitchDown, new ButtonSetting { Button = GamepadButtonFlags.DPadDown }
                        },
                        { ButtonSettingType.TrimElevatorUp, new ButtonSetting { Button = (ushort)0 } },
                        { ButtonSettingType.TrimElevatorDown, new ButtonSetting { Button = (ushort)0 } },
                    }
                },
                {
                    ControllerType.Joystick,
                    new Dictionary<ButtonSettingType, ButtonSetting>
                    {
                        {
                            ButtonSettingType.TimeCheckpoint,
                            new ButtonSetting { Button = JoystickWrapper.JoystickButtons.Eleven }
                        },
                        {
                            ButtonSettingType.PlaneFeedback,
                            new ButtonSetting { Button = JoystickWrapper.JoystickButtons.Twelve }
                        },
                        {
                            ButtonSettingType.Flaps,
                            new ButtonSetting { Button = JoystickWrapper.JoystickButtons.Seven }
                        },
                        { ButtonSettingType.ThrottleFull, new ButtonSetting { Button = (ushort)0 } },
                        { ButtonSettingType.ThrottleZero, new ButtonSetting { Button = (ushort)0 } },
                        { ButtonSettingType.ThrottleSetpoint1, new ButtonSetting { Button = (ushort)0 } },
                        { ButtonSettingType.ThrottleSetpoint2, new ButtonSetting { Button = (ushort)0 } },
                        {
                            ButtonSettingType.TrimDownOrLeft,
                            new ButtonSetting { Button = JoystickWrapper.JoystickButtons.DLeft }
                        },
                        {
                            ButtonSettingType.TrimUpOrRight,
                            new ButtonSetting { Button = JoystickWrapper.JoystickButtons.DRight }
                        },
                        {
                            ButtonSettingType.TrimSwitchUp,
                            new ButtonSetting { Button = JoystickWrapper.JoystickButtons.DUp }
                        },
                        {
                            ButtonSettingType.TrimSwitchDown,
                            new ButtonSetting { Button = JoystickWrapper.JoystickButtons.DDown }
                        },
                        {
                            ButtonSettingType.TrimElevatorUp,
                            new ButtonSetting { Button = JoystickWrapper.JoystickButtons.Four }
                        },
                        {
                            ButtonSettingType.TrimElevatorDown,
                            new ButtonSetting { Button = JoystickWrapper.JoystickButtons.Three }
                        },
                    }
                }
            };

        public Dictionary<ControllerType, Dictionary<ButtonSettingType, ButtonSetting>> ButtonSettingsFixed { get; } =
            new Dictionary<ControllerType, Dictionary<ButtonSettingType, ButtonSetting>>
            {
                {
                    ControllerType.Gamepad,
                    new Dictionary<ButtonSettingType, ButtonSetting>
                    {
                        {
                            ButtonSettingType.ThrottleLock,
                            new ButtonSetting { Button = GamepadButtonFlags.LeftThumb | GamepadButtonFlags.RightThumb }
                        }
                    }
                },
                {
                    ControllerType.Joystick,
                    new Dictionary<ButtonSettingType, ButtonSetting>
                    {
                        {
                            ButtonSettingType.ThrottleLock,
                            new ButtonSetting
                                { Button = JoystickWrapper.JoystickButtons.Two | JoystickWrapper.JoystickButtons.Eight }
                        }
                    }
                }
            };

        public PlaneController()
        {
            AvailableAxes = mGamepadAxesOptions;

            foreach (var nList in ButtonSettings)
            {
                foreach (var nSetting in nList.Value)
                {
                    nSetting.Value.ActionName = mButtonSettingNames[nSetting.Key];
                    nSetting.Value.ButtonOptions = ButtonOptions[nList.Key];
                }
            }

            foreach (var nList in ButtonSettingsFixed)
            {
                foreach (var nSetting in nList.Value)
                {
                    nSetting.Value.ActionName = mButtonSettingNames[nSetting.Key];
                }
            }
        }

        private List<object> mAvailableAxes;

        public List<object> AvailableAxes
        {
            get { return mAvailableAxes; }
            private set
            {
                if (value == mAvailableAxes) return;
                mAvailableAxes = value;
                OnPropertyChanged(nameof(AvailableAxes));
            }
        }

        public bool IsConnected
        {
            get
            {
                return (mJoystick.IsConnected && mActiveType == ControllerType.Joystick) ||
                       (mGamepad.IsConnected && mActiveType == ControllerType.Gamepad);
            }
        }

        public bool IsConnectedGamepad
        {
            get { return mGamepad.IsConnected; }
        }

        public bool IsConnectedJoystick
        {
            get { return mJoystick.IsConnected; }
        }

        private ControllerType mActiveType = ControllerType.Gamepad;

        public ControllerType ActiveType
        {
            get { return mActiveType; }
            set
            {
                if (value == mActiveType) return;
                mActiveType = value;
                OnPropertyChanged(nameof(ActiveType));

                if (mActiveType == ControllerType.Gamepad)
                {
                    AvailableAxes = mGamepadAxesOptions;
                }
                else if (mActiveType == ControllerType.Joystick)
                {
                    AvailableAxes = mJoystickAxesOptions;
                }
                else AvailableAxes = mEmptyAxesOptions;
            }
        }

        public ObservableCollection<ControllerType> AvailableControllers { get; } =
            new ObservableCollection<ControllerType> { ControllerType.Gamepad, ControllerType.Joystick };

        public double GetAxisValue(AxisControlSettings aAxis, SquareStick aSquareStick = SquareStick.None)
        {
            if (aAxis.Axis is GamepadAxes && mActiveType == ControllerType.Gamepad && mGamepad.IsConnected)
                return mGamepad.GetAxisValue((GamepadAxes)aAxis.Axis, aAxis.Inverted, aAxis.Deadband, aSquareStick);
            if (aAxis.Axis is JoystickAxes && mActiveType == ControllerType.Joystick && mJoystick.IsConnected)
                return mJoystick.GetAxisValue((JoystickAxes)aAxis.Axis, aAxis.Inverted, aAxis.Deadband);

            return 0.0;
        }

        public GamepadButtonFlags GetGamepadButtons()
        {
            return mGamepad.GetState().Gamepad.Buttons;
        }

        public GamepadButtonFlags GetGamepadButtonsPressed(bool aUpdateButtons = true)
        {
            return mGamepad.GetButtonsPressed(aUpdateButtons);
        }

        public JoystickWrapper.JoystickButtons GetJoystickButtons()
        {
            return mJoystick.GetButtons();
        }

        public JoystickWrapper.JoystickButtons GetJoystickButtonsPressed(bool aUpdateButtons = true)
        {
            return mJoystick.GetButtonsPressed(aUpdateButtons);
        }

        public ushort GetActiveButtons()
        {
            if (ActiveType == ControllerType.Gamepad) return (ushort)GetGamepadButtons();
            if (ActiveType == ControllerType.Joystick) return (ushort)GetJoystickButtons();
            return 0;
        }

        public ushort GetActiveButtonsPressed(bool aUpdateButtons = true)
        {
            if (ActiveType == ControllerType.Gamepad) return (ushort)GetGamepadButtonsPressed(aUpdateButtons);
            if (ActiveType == ControllerType.Joystick) return (ushort)GetJoystickButtonsPressed(aUpdateButtons);
            return 0;
        }

        public void Dispose()
        {
            mJoystick.Dispose();
        }
    }
}