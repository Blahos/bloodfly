﻿using System.Windows.Forms;

namespace BloodflyControlUI
{
    public static class BatteryMonitor
    {
        public static double BatteryLevel() => (double)SystemInformation.PowerStatus.BatteryLifePercent;
    }
}