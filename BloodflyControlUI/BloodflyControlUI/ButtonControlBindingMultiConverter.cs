﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using static BloodflyControlUI.PlaneController;

namespace BloodflyControlUI
{
    public class ButtonControlBindingMultiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null) return null;
            if (values.Length != 2) return null;
            if (!(values[0] is Dictionary<ControllerType, Dictionary<ButtonSettingType, ButtonSetting>>)) return null;
            if (!(values[1] is ControllerType)) return null;

            var lDict = values[0] as Dictionary<ControllerType, Dictionary<ButtonSettingType, ButtonSetting>>;
            var lKey = (ControllerType)values[1];

            if (!lDict.ContainsKey(lKey)) return null;
            return lDict[lKey].Values.ToList();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}