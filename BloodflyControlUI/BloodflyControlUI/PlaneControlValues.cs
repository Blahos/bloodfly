﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Speech.Synthesis;
using System.Threading;
using System.Windows.Input;

namespace BloodflyControlUI
{
    public class PlaneControlValues : BaseViewModel, IPlaneControlValues
    {
        private DateTime mLastThrottleUnlockTime = DateTime.Now;
        private DateTime mLastThrottleChangeTime = DateTime.MinValue;
        public Announcer Announcer { get; set; } = null;
        double mLastAnnouncedThrottle = -1;
        List<Prompt> mThrottleAnnouncePromts = new List<Prompt>();
        private readonly Timer mThrottleAnnounceTimer = null;

        public PlaneControlValues()
        {
            mThrottleAnnounceTimer = new Timer(ThrottleAnnounceTimerCallback, null, 0, Timeout.Infinite);
        }

        private void ThrottleAnnounceTimerCallback(object aArg)
        {
            var lThrottle = Throttle;
            var lAnnouncer = Announcer;

            if (lAnnouncer != null &&
                lThrottle != mLastAnnouncedThrottle &&
                (DateTime.Now - mLastThrottleChangeTime) > TimeSpan.FromMilliseconds(100))
            {
                mThrottleAnnouncePromts.Clear();
                lAnnouncer.AnnounceThrottle(lThrottle);
                mLastAnnouncedThrottle = lThrottle;
            }

            mThrottleAnnounceTimer.Change(100, Timeout.Infinite);
        }

        public void AnnounceThrottleLock()
        {
            //Task.Run(() =>
            //{
            var lAnnouncer = Announcer;
            if (lAnnouncer == null) return;
            lAnnouncer.AnnounceThrottleLockState(ThrottleLocked);
            //});
        }


        private List<ButtonSetting> mButtons = new List<ButtonSetting>();

        public List<ButtonSetting> Buttons
        {
            get { return mButtons; }
            set
            {
                if (value == mButtons) return;
                mButtons = value;
                OnPropertyChanged(nameof(Buttons));
            }
        }

        private double mAilerons;

        public double Ailerons
        {
            get { return mAilerons; }
            set
            {
                if (value == mAilerons) return;
                mAilerons = value;
                OnPropertyChanged(nameof(Ailerons));
            }
        }

        private double mElevator;

        public double Elevator
        {
            get { return mElevator; }
            set
            {
                if (value == mElevator) return;
                mElevator = value;
                OnPropertyChanged(nameof(Elevator));
            }
        }

        private double mRudder;

        public double Rudder
        {
            get { return mRudder; }
            set
            {
                if (value == mRudder) return;
                mRudder = value;
                OnPropertyChanged(nameof(Rudder));
            }
        }

        private double mThrottle;

        public double Throttle
        {
            get { return mThrottle; }
            set
            {
                if (ThrottleLocked || (DateTime.Now - mLastThrottleUnlockTime).TotalSeconds < 3.0)
                    value = 0;
                if (value == mThrottle) return;
                mThrottle = value;

                mLastThrottleChangeTime = DateTime.Now;
                OnPropertyChanged(nameof(Throttle));
            }
        }

        private bool mThrottleLocked = true;

        public bool ThrottleLocked
        {
            get { return mThrottleLocked; }
            set
            {
                if (value == mThrottleLocked) return;
                mThrottleLocked = value;
                Throttle = 0;
                if (!mThrottleLocked) mLastThrottleUnlockTime = DateTime.Now;
                OnPropertyChanged(nameof(ThrottleLocked));
                AnnounceThrottleLock();
            }
        }

        private double mThrottleSetpoint1 = 0.2;

        public double ThrottleSetpoint1
        {
            get { return mThrottleSetpoint1; }
            set
            {
                if (value == mThrottleSetpoint1) return;
                mThrottleSetpoint1 = value;
                OnPropertyChanged(nameof(ThrottleSetpoint1));
            }
        }

        private double mThrottleSetpoint2 = 0.7;

        public double ThrottleSetpoint2
        {
            get { return mThrottleSetpoint2; }
            set
            {
                if (value == mThrottleSetpoint2) return;
                mThrottleSetpoint2 = value;
                OnPropertyChanged(nameof(ThrottleSetpoint2));
            }
        }


        private AxisControlSettings mAileronsAxis = new AxisControlSettings();

        public AxisControlSettings AileronsAxis
        {
            get { return mAileronsAxis; }
            set
            {
                if (value == mAileronsAxis) return;
                mAileronsAxis = value;
                OnPropertyChanged(nameof(AileronsAxis));
            }
        }

        private AxisControlSettings mElevatorAxis = new AxisControlSettings();

        public AxisControlSettings ElevatorAxis
        {
            get { return mElevatorAxis; }
            set
            {
                if (value == mElevatorAxis) return;
                mElevatorAxis = value;
                OnPropertyChanged(nameof(ElevatorAxis));
            }
        }

        private AxisControlSettings mRudderAxis = new AxisControlSettings();

        public AxisControlSettings RudderAxis
        {
            get { return mRudderAxis; }
            set
            {
                if (value == mRudderAxis) return;
                mRudderAxis = value;
                OnPropertyChanged(nameof(RudderAxis));
            }
        }

        private AxisControlSettings mThrottleAxis = new AxisControlSettings();

        public AxisControlSettings ThrottleAxis
        {
            get { return mThrottleAxis; }
            set
            {
                if (value == mThrottleAxis) return;
                mThrottleAxis = value;
                OnPropertyChanged(nameof(ThrottleAxis));
            }
        }

        private int mAileronLeftAngle = 30;

        public int AileronLeftAngle
        {
            get { return mAileronLeftAngle; }
            set
            {
                if (value == mAileronLeftAngle) return;
                mAileronLeftAngle = value;
                OnPropertyChanged(nameof(AileronLeftAngle));
            }
        }

        private int mAileronRightAngle = 30;

        public int AileronRightAngle
        {
            get { return mAileronRightAngle; }
            set
            {
                if (value == mAileronRightAngle) return;
                mAileronRightAngle = value;
                OnPropertyChanged(nameof(AileronRightAngle));
            }
        }

        private int mElevatorAngle = 30;

        public int ElevatorAngle
        {
            get { return mElevatorAngle; }
            set
            {
                if (value == mElevatorAngle) return;
                mElevatorAngle = value;
                OnPropertyChanged(nameof(ElevatorAngle));
            }
        }

        private int mRudderAngle = 30;

        public int RudderAngle
        {
            get { return mRudderAngle; }
            set
            {
                if (value == mRudderAngle) return;
                mRudderAngle = value;
                OnPropertyChanged(nameof(RudderAngle));
            }
        }

        private int mFlapOffsetNeutral = 0;

        public int FlapOffsetNeutral
        {
            get { return mFlapOffsetNeutral; }
            set
            {
                if (value == mFlapOffsetNeutral) return;
                mFlapOffsetNeutral = value;
                OnPropertyChanged(nameof(FlapOffsetNeutral));
            }
        }


        private int mFlapOffsetDown1 = -20;

        public int FlapOffsetDown1
        {
            get { return mFlapOffsetDown1; }
            set
            {
                if (value == mFlapOffsetDown1) return;
                mFlapOffsetDown1 = value;
                OnPropertyChanged(nameof(FlapOffsetDown1));
            }
        }


        private int mFlapOffsetDown2 = -30;

        public int FlapOffsetDown2
        {
            get { return mFlapOffsetDown2; }
            set
            {
                if (value == mFlapOffsetDown2) return;
                mFlapOffsetDown2 = value;
                OnPropertyChanged(nameof(FlapOffsetDown2));
            }
        }


        private bool mFlapModeNeutral = true;

        public bool FlapModeNeutral
        {
            get { return mFlapModeNeutral; }
            set
            {
                if (value == mFlapModeNeutral) return;
                mFlapModeNeutral = value;
                if (mFlapModeNeutral)
                {
                    FlapModeDown1 = false;
                    FlapModeDown2 = false;
                }

                OnPropertyChanged(nameof(FlapModeNeutral));
            }
        }

        private bool mFlapModeDown1 = false;

        public bool FlapModeDown1
        {
            get { return mFlapModeDown1; }
            set
            {
                if (value == mFlapModeDown1) return;
                mFlapModeDown1 = value;
                if (mFlapModeDown1)
                {
                    FlapModeNeutral = false;
                    FlapModeDown2 = false;
                }

                OnPropertyChanged(nameof(FlapModeDown1));
            }
        }

        private bool mFlapModeDown2 = false;

        public bool FlapModeDown2
        {
            get { return mFlapModeDown2; }
            set
            {
                if (value == mFlapModeDown2) return;
                mFlapModeDown2 = value;
                if (mFlapModeDown2)
                {
                    FlapModeNeutral = false;
                    FlapModeDown1 = false;
                }

                OnPropertyChanged(nameof(FlapModeDown2));
            }
        }

        public int FlapOffset
        {
            get
            {
                int lReturn = 0;
                if (FlapModeNeutral) lReturn = FlapOffsetNeutral;
                else if (FlapModeDown1) lReturn = FlapOffsetDown1;
                else if (FlapModeDown2) lReturn = FlapOffsetDown2;
                return lReturn;
            }
        }

        private ICommand mAileronsZeroCommand;

        public ICommand AileronsZeroCommand
        {
            get
            {
                if (mAileronsZeroCommand == null)
                {
                    mAileronsZeroCommand = new RelayCommand(a => { Ailerons = 0; });
                }

                return mAileronsZeroCommand;
            }
        }

        private ICommand mElevatorZeroCommand;

        public ICommand ElevatorZeroCommand
        {
            get
            {
                if (mElevatorZeroCommand == null)
                {
                    mElevatorZeroCommand = new RelayCommand(a => { Elevator = 0; });
                }

                return mElevatorZeroCommand;
            }
        }

        private ICommand mRudderZeroCommand;

        public ICommand RudderZeroCommand
        {
            get
            {
                if (mRudderZeroCommand == null)
                {
                    mRudderZeroCommand = new RelayCommand(a => { Rudder = 0; });
                }

                return mRudderZeroCommand;
            }
        }

        private ICommand mThrottleZeroCommand;

        public ICommand ThrottleZeroCommand
        {
            get
            {
                if (mThrottleZeroCommand == null)
                {
                    mThrottleZeroCommand = new RelayCommand(a => { Throttle = 0; });
                }

                return mThrottleZeroCommand;
            }
        }

        private ICommand mAllZeroCommand;

        public ICommand AllZeroCommand
        {
            get
            {
                if (mAllZeroCommand == null)
                {
                    mAllZeroCommand = new RelayCommand(a =>
                    {
                        Ailerons = 0;
                        Elevator = 0;
                        Throttle = 0;
                        Rudder = 0;
                    });
                }

                return mAllZeroCommand;
            }
        }
    }
}