﻿namespace BloodflyControlUI
{
    public enum NavigationPlan
    {
        None,
        NextCheckpoint,
        Base,
        NextCheckpointOrBase
    }
}