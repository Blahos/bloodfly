﻿using FlashCap;
using System;
using System.Drawing;
using System.IO;

namespace BloodflyControlUI
{
    public class FlashCapCamera : ICamera
    {
        private CaptureDevice mDevice = null;

        public FlashCapCamera(CaptureDeviceDescriptor descriptor, VideoCharacteristics characteristics)
        {
            var deviceTask = descriptor.OpenAsync(characteristics, OnPixelBufferArrived);
            if (deviceTask.Wait(1000))
            {
                mDevice = deviceTask.Result;
                if (!(mDevice?.StartAsync().Wait(200) ?? true))
                {
                    Console.WriteLine("Failed to start camera device");
                }
            }
            else
            {
                Console.WriteLine("Failed to open camera device");
            }
        }

        public event Action<Bitmap, DateTime> NewFrame;

        public void Dispose()
        {
            var stopSuccessful = mDevice?.StopAsync().Wait(200);
            if (!(stopSuccessful ?? true))
            {
                Console.WriteLine("Failed to dipose camera device");
            }
        }

        private void OnPixelBufferArrived(PixelBufferScope bufferScope)
        {
            var time = DateTime.Now;
            byte[] image = bufferScope.Buffer.ExtractImage();

            var ms = new MemoryStream(image);
            var bitmap = new Bitmap(ms);

            NewFrame?.Invoke(bitmap, time);
        }
    }
}