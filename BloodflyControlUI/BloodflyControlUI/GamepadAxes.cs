﻿namespace BloodflyControlUI
{
    public enum GamepadAxes
    {
        LeftThumbX,
        LeftThumbY,
        RightThumbX,
        RightThumbY,
        Trigger
    }
}