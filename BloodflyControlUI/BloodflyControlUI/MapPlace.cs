﻿using System.Windows;

namespace BloodflyControlUI
{
    public class MapPlace
    {
        public Vector MapPosition { get; set; }
        public string Name { get; set; }
        public int MapZoom { get; set; } = -1;
    }
}