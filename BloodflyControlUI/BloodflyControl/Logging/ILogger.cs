﻿namespace BloodflyControl.Logging
{
    public interface ILogger
    {
        void LogMessage(string message, LogLevel level);
    }
}