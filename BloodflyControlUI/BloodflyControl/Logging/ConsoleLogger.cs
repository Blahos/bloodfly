﻿namespace BloodflyControl.Logging
{
    public class ConsoleLogger : ILogger
    {
        public void LogMessage(string message, LogLevel level)
        {
            Console.WriteLine($"{DateTime.Now} - {level}: {message}");
        }
    }
}