﻿namespace BloodflyControl.Logging
{
    public class EmptyLogger : ILogger
    {
        private static readonly EmptyLogger instance = new EmptyLogger();
        public static EmptyLogger Instance => instance;

        private EmptyLogger()
        {
        }

        public void LogMessage(string message, LogLevel level)
        {
            return;
        }
    }
}