﻿namespace BloodflyControl.Logging
{
    public enum LogLevel
    {
        Info,
        Error
    }
}