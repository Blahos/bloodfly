﻿using BloodflyControl.Framework.Interfaces;

namespace BloodflyControl.Processors
{
    internal class PlaneControlFeedbackProcessor : IDisposable
    {
        private readonly ISerialTimer processTimer;

        public PlaneControlFeedbackProcessor(ISerialTimerFactory serialTimerFactory)
        {
            processTimer = serialTimerFactory.CreateSerialTimer(Process, TimeSpan.FromMilliseconds(100));
        }

        public void Dispose()
        {
            processTimer.Terminate(false);
        }

        private void Process()
        {
        }
    }
}