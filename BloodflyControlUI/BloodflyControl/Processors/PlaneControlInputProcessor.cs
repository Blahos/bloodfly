﻿using BloodflyControl.ControlCommunication.Interfaces;
using BloodflyControl.ControllerInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;
using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.Processors
{
    internal class PlaneControlInputProcessor : IDisposable
    {
        private readonly IPlaneControllerProvider planeControllerProvider;
        private readonly ITargetPlaneAxesValuesStorage targetPlaneAxesValuesStorage;
        private readonly ISafePlaneAxesValuesProvider safePlaneAxesValuesProvider;
        private readonly IControllerToPlaneAxesTranslator controllerToPlaneAxesTranslator;
        private readonly IPlaneControlWriter planeControlWriter;
        private readonly IControlActionsExecutor controlActionsExecutor;
        private readonly ILogger logger;

        private readonly ISerialTimer processTimer;

        public PlaneControlInputProcessor(
            IPlaneControllerProvider planeControllerProvider,
            ITargetPlaneAxesValuesStorage targetPlaneAxesValuesStorage,
            ISafePlaneAxesValuesProvider safePlaneAxesValuesProvider,
            IControllerToPlaneAxesTranslator controllerToPlaneAxesTranslator,
            IPlaneControlWriter planeControlWriter,
            IControlActionsExecutor controlActionsExecutor,
            ISerialTimerFactory serialTimerFactory,
            ILogger logger)
        {
            this.planeControllerProvider = planeControllerProvider;
            this.targetPlaneAxesValuesStorage = targetPlaneAxesValuesStorage;
            this.safePlaneAxesValuesProvider = safePlaneAxesValuesProvider;
            this.controllerToPlaneAxesTranslator = controllerToPlaneAxesTranslator;
            this.planeControlWriter = planeControlWriter;
            this.controlActionsExecutor = controlActionsExecutor;
            this.logger = logger;

            processTimer = serialTimerFactory.CreateSerialTimer(Process, TimeSpan.FromMilliseconds(20));
        }

        private void Process()
        {
            ProcessControllers();
            planeControlWriter.Write();
        }

        private void ProcessControllers()
        {
            foreach (var controller in planeControllerProvider.GetControllersConnectionStatus())
            {
                if (!controller.Value) continue;

                try
                {
                    ProcessController(controller.Key);
                }
                catch (Exception ex)
                {
                    logger.LogMessage(
                        $"Failed to process controller input for controller role {controller.Key}: {ex.Message}",
                        LogLevel.Error);
                }
            }
        }

        private void ProcessController(ControllerRole controllerRole)
        {
            var controllerInput = planeControllerProvider.GetControllerInput(controllerRole);
            controlActionsExecutor.ExecuteActions(
                controllerInput,
                controllerRole);

            UpdatePlaneAxesValues(controllerInput, controllerRole);
        }

        private void UpdatePlaneAxesValues(ControllerInputStateEx controllerInput, ControllerRole controllerRole)
        {
            var newPlaneAxesValues = controllerToPlaneAxesTranslator.TranslateToPlaneAxes(
                controllerInput.Axes,
                safePlaneAxesValuesProvider.GetValues(),
                controllerInput.TimeElapsedSincePreviousInput,
                controllerRole);

            targetPlaneAxesValuesStorage.SetValues(newPlaneAxesValues);
        }

        public void Dispose()
        {
            processTimer.Terminate(false);
        }
    }
}