﻿using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.SerialComm
{
    internal class SerialPortCommunicator(ISerialPortProvider serialPortProvider)
        : ISerialPortWriter, ISerialPortReader
    {
        public string Read()
        {
            var port = serialPortProvider.SerialPort;
            if (port == null) throw new InvalidOperationException("Port unavailable for read");
            return port.Read();
        }

        public void Write(string text)
        {
            var port = serialPortProvider.SerialPort;
            if (port == null) throw new InvalidOperationException("Port unavailable for write");
            port.Write(text);
        }

        public void Write(byte[] buffer)
        {
            var port = serialPortProvider.SerialPort;
            if (port == null) throw new InvalidOperationException("Port unavailable for write");
            port.Write(buffer);
        }
    }
}