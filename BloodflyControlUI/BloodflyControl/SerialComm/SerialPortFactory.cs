﻿using BloodflyControl.Logging;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.SerialComm
{
    internal class SerialPortFactory(ILogger logger) : ISerialPortFactory
    {
        public ISerialPort CreateSerialPort(SerialPortParameters serialPortParameters)
        {
            return new SerialPort(serialPortParameters, logger);
        }
    }
}