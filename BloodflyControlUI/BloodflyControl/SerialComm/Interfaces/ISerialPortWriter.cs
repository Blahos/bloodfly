﻿namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPortWriter
    {
        void Write(string text);
        void Write(byte[] buffer);
    }
}