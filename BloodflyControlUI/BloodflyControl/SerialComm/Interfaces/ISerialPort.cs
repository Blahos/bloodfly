﻿namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPort : ISerialPortWriter, ISerialPortReader, IDisposable
    {
        bool IsOpen { get; }
        SerialPortParameters Parameters { get; }
        void Open();
    }
}