﻿using BloodflyControl.Framework.Interfaces;

namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPortScanner : IGenericStateProvider<AvailablePortsState>, IDisposable
    {
    }
}