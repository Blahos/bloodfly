﻿namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPortStateProvider
    {
        SerialPortParameters? PortParameters { get; }
        bool PortOpen { get; }

        event Action? PortStateChanged;
    }
}