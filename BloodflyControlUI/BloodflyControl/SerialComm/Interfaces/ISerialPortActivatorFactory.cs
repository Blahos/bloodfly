﻿namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPortActivatorFactory
    {
        ISerialPortActivator CreateSerialPortActivator();
    }
}