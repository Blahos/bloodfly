﻿namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPortActivator : IDisposable
    {
        public event Action? PortOpenedStateChanged;
        void SetSerialPort(ISerialPort? serialPort);
    }
}