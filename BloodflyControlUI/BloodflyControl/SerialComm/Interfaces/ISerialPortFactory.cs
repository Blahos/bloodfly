﻿namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPortFactory
    {
        ISerialPort CreateSerialPort(SerialPortParameters serialPortParameters);
    }
}