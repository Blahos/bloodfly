﻿namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPortProvider
    {
        ISerialPort? SerialPort { get; }
    }
}