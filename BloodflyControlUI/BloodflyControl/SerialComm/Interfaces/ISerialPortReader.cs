﻿namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPortReader
    {
        string Read();
    }
}