﻿namespace BloodflyControl.SerialComm.Interfaces
{
    internal interface ISerialPortConnectorSettings
    {
        SerialPortParameters? SerialPortParameters { get; set; }
        event Action? SerialPortParametersChanged;
    }
}