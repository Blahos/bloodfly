﻿namespace BloodflyControl.SerialComm
{
    internal readonly record struct SerialPortParameters(string Name, int BaudRate);
}