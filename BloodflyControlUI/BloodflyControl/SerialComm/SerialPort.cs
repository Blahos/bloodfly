﻿using BloodflyControl.Logging;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.SerialComm
{
    internal class SerialPort : ISerialPort
    {
        private readonly System.IO.Ports.SerialPort port;
        private readonly object writeLock = new();
        private readonly object readLock = new();
        private readonly ILogger logger;
        public bool IsOpen => port.IsOpen;
        public SerialPortParameters Parameters => new(port.PortName, port.BaudRate);

        public SerialPort(SerialPortParameters serialPortParameters, ILogger logger)
        {
            port = new System.IO.Ports.SerialPort(serialPortParameters.Name, serialPortParameters.BaudRate);
            port.WriteTimeout = 10;
            this.logger = logger;
        }

        public void Open()
        {
            if (IsOpen) return;
            try
            {
                port.Open();
                if (IsOpen) logger.LogMessage($"Port {port.PortName} successfully opened", LogLevel.Info);
            }
            catch (Exception)
            {
                logger.LogMessage($"Failed to open serial port {port.PortName}", LogLevel.Error);
            }
        }

        public void Dispose()
        {
            if (port.IsOpen)
            {
                port.Close();
            }

            port.Dispose();
        }

        public void Write(string text)
        {
            lock (writeLock)
            {
                if (!IsOpen)
                    throw new InvalidOperationException($"Cannot write to port {Parameters.Name}, it is not open");
                port.Write(text);
            }
        }

        public void Write(byte[] buffer)
        {
            lock (writeLock)
            {
                if (!IsOpen)
                    throw new InvalidOperationException($"Cannot write to port {Parameters.Name}, it is not open");
                port.Write(buffer, 0, buffer.Length);
            }
        }

        public string Read()
        {
            lock (readLock)
            {
                if (!IsOpen)
                    throw new InvalidOperationException($"Cannot read from port {Parameters.Name}, it is not open");
                return port.ReadExisting();
            }
        }
    }
}