﻿using BloodflyControl.Framework.Interfaces;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.SerialComm
{
    internal class SerialPortConnector : ISerialPortStateProvider, ISerialPortProvider, IDisposable
    {
        private readonly ISerialPortFactory serialPortFactory;
        private readonly ISerialPortConnectorSettings serialPortConnectorSettings;
        private readonly ISerialTimer serialPortRefreshingTimer;
        private readonly ISerialPortActivator serialPortActivator;
        private ISerialPort? serialPort = null;
        private readonly object serialPortLock = new();

        public event Action? PortStateChanged;

        public SerialPortConnector(
            ISerialPortFactory serialPortFactory,
            ISerialPortConnectorSettings serialPortConnectorSettings,
            ISerialPortActivatorFactory serialPortActivatorFactory,
            ISerialTimerFactory serialTimerFactory)
        {
            this.serialPortFactory = serialPortFactory;
            this.serialPortConnectorSettings = serialPortConnectorSettings;

            serialPortConnectorSettings.SerialPortParametersChanged +=
                SerialPortConnectorSettings_SerialPortParametersChanged;

            serialPortActivator = serialPortActivatorFactory.CreateSerialPortActivator();
            serialPortActivator.PortOpenedStateChanged += SerialPortActivator_PortOpenedStateChanged;

            serialPortRefreshingTimer =
                serialTimerFactory.CreateSerialTimer(RefreshSerialPort, TimeSpan.FromMilliseconds(300));
        }

        public void Dispose()
        {
            serialPortRefreshingTimer.Terminate(true);
            serialPortActivator.PortOpenedStateChanged -= SerialPortActivator_PortOpenedStateChanged;
            serialPortConnectorSettings.SerialPortParametersChanged -=
                SerialPortConnectorSettings_SerialPortParametersChanged;
            serialPortActivator.Dispose();
            lock (serialPortLock)
            {
                DisposeSerialPortIfNeeded();
            }
        }

        public bool PortOpen
        {
            get
            {
                lock (serialPortLock)
                {
                    return IsSerialPortUpToDate() && (serialPort?.IsOpen ?? false);
                }
            }
        }

        public SerialPortParameters? PortParameters
        {
            get
            {
                lock (serialPortLock)
                {
                    return IsSerialPortUpToDate() ? serialPort?.Parameters : null;
                }
            }
        }

        public ISerialPort? SerialPort
        {
            get
            {
                lock (serialPortLock)
                {
                    return IsSerialPortUpToDate() ? serialPort : null;
                }
            }
        }

        private void RefreshSerialPort()
        {
            var desiredPortParameters = serialPortConnectorSettings.SerialPortParameters;
            var raiseEvent = false;
            lock (serialPortLock)
            {
                if (ShouldUpdateSerialPort(desiredPortParameters))
                {
                    raiseEvent = true;
                    DisposeSerialPortIfNeeded();
                    CreateNewSerialPortIfNeeded(desiredPortParameters);
                }
            }

            if (raiseEvent)
                OnPortStateChanged();
        }

        private bool IsSerialPortUpToDate()
        {
            return serialPort?.Parameters == serialPortConnectorSettings.SerialPortParameters;
        }

        private bool ShouldUpdateSerialPort(SerialPortParameters? serialPortParameters)
        {
            return serialPort?.Parameters != serialPortParameters;
        }

        private void DisposeSerialPortIfNeeded()
        {
            if (serialPort == null) { return; }

            serialPort.Dispose();
            serialPort = null;
            serialPortActivator.SetSerialPort(null);
        }

        private void CreateNewSerialPortIfNeeded(SerialPortParameters? serialPortParameters)
        {
            if (serialPortParameters == null) { return; }

            serialPort = serialPortFactory.CreateSerialPort(serialPortParameters.Value);
            serialPortActivator.SetSerialPort(serialPort);
        }

        private void SerialPortActivator_PortOpenedStateChanged()
        {
            OnPortStateChanged();
        }

        private void SerialPortConnectorSettings_SerialPortParametersChanged()
        {
            OnPortStateChanged();
        }

        private void OnPortStateChanged()
        {
            PortStateChanged?.Invoke();
        }
    }
}