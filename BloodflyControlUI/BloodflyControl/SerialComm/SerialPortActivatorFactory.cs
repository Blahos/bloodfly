﻿using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.SerialComm
{
    internal class SerialPortActivatorFactory(
        ISerialTimerFactory serialTimerFactory,
        ILogger logger)
        : ISerialPortActivatorFactory
    {
        public ISerialPortActivator CreateSerialPortActivator()
        {
            return new SerialPortActivator(serialTimerFactory, logger);
        }
    }
}