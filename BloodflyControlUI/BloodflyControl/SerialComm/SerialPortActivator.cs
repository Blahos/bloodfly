﻿using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.SerialComm
{
    internal class SerialPortActivator : ISerialPortActivator
    {
        private readonly record struct SerialPortActivateResult(ISerialPort? Port, bool IsOpen);

        private ISerialPort? serialPort = null;
        private readonly ISerialTimer activatingTimer;
        private readonly ILogger logger;
        private SerialPortActivateResult lastActivateResult = new(null, false);

        public event Action? PortOpenedStateChanged;

        public SerialPortActivator(ISerialTimerFactory serialTimerFactory, ILogger logger)
        {
            this.logger = logger;
            activatingTimer = serialTimerFactory.CreateSerialTimer(ActivateCallback, TimeSpan.FromSeconds(0.5));
        }

        private void ActivateCallback()
        {
            var serialPort = this.serialPort;

            if (serialPort != null)
            {
                TryOpenPortIfClosed(serialPort);
            }

            var newActivateResult = new SerialPortActivateResult(serialPort, serialPort?.IsOpen ?? false);
            var raiseEvent = newActivateResult != lastActivateResult;
            lastActivateResult = newActivateResult;

            if (raiseEvent)
            {
                PortOpenedStateChanged?.Invoke();
            }
        }

        private void TryOpenPortIfClosed(ISerialPort port)
        {
            try
            {
                if (!port.IsOpen)
                {
                    logger.LogMessage($"Trying to open port {port.Parameters.Name}", LogLevel.Info);
                    port.Open();
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage($"Failed to open port {port.Parameters.Name}: {ex}", LogLevel.Error);
            }
        }


        public void Dispose()
        {
            activatingTimer.Terminate(true);
        }

        public void SetSerialPort(ISerialPort? serialPort)
        {
            this.serialPort = serialPort;
        }
    }
}