﻿using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.SerialComm
{
    internal class SerialPortConnectorSettings : ISerialPortConnectorSettings
    {
        private readonly object parametersLock = new();
        private SerialPortParameters? parameters;

        public SerialPortParameters? SerialPortParameters
        {
            get
            {
                lock (parametersLock)
                {
                    return parameters;
                }
            }
            set
            {
                var raiseEvent = false;
                lock (parametersLock)
                {
                    if (value != parameters)
                    {
                        parameters = value;
                        raiseEvent = true;
                    }
                }

                if (raiseEvent)
                {
                    OnParametersChanged();
                }
            }
        }

        public event Action? SerialPortParametersChanged;

        private void OnParametersChanged()
        {
            SerialPortParametersChanged?.Invoke();
        }
    }
}