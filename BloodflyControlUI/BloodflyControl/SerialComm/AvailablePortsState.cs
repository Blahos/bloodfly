﻿using BloodflyControl.Framework.Interfaces;

namespace BloodflyControl.SerialComm
{
    internal class AvailablePortsState(HashSet<string> ports) : IState<AvailablePortsState>
    {
        private readonly HashSet<string> ports = ports;
        public IReadOnlyCollection<string> Ports => ports;

        public AvailablePortsState Clone()
        {
            return new AvailablePortsState(ports.ToHashSet());
        }

        public bool Equals(AvailablePortsState? other)
        {
            if (other == null) return false;
            return ports.SetEquals(other.ports);
        }
    }
}