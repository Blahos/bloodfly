﻿using BloodflyControl.Framework;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.SerialComm
{
    internal class SerialPortScanner(ISerialTimerFactory serialTimerFactory, ILogger logger)
        : GenericStateScanner<AvailablePortsState>(serialTimerFactory, TimeSpan.FromSeconds(0.4)), ISerialPortScanner
    {
        private readonly ILogger logger = logger;

        protected override AvailablePortsState GetDefaultState()
        {
            return new([]);
        }

        protected override AvailablePortsState GetNewState()
        {
            var ports = System.IO.Ports.SerialPort.GetPortNames().ToHashSet();
            return new AvailablePortsState(ports);
        }
    }
}