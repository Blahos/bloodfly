﻿using BloodflyControl.ControllerInput.DInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.ControllerInput.XInput;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;

namespace BloodflyControl.ControllerInput
{
    internal class ControllerFactory(
        ISerialTimerFactory serialTimerFactory,
        IDInputJoystickWrapperFactory dInputJoystickWrapperFactory,
        IDInputJoystickButtonParser dInputJoystickButtonParser,
        IDInputJoystickAxesParser dInputJoystickAxesParser,
        IXInputButtonConverter xInputButtonConverter,
        ILogger logger) : IControllerFactory
    {
        public IController CreateController(IControllerId controllerId)
        {
            if (controllerId is DInputControllerId dInputControllerId)
            {
                return new DInputController(dInputControllerId, dInputJoystickButtonParser, dInputJoystickAxesParser,
                    dInputJoystickWrapperFactory, serialTimerFactory, logger);
            }

            if (controllerId is XInputControllerId xInputControllerId)
            {
                return new XInputController(xInputControllerId, xInputButtonConverter, serialTimerFactory, logger);
            }

            throw new ArgumentException($"Unknown controlelrId type: {controllerId.GetType()}");
        }
    }
}