﻿namespace BloodflyControl.ControllerInput
{
    internal static class ControllerRoleExtensions
    {
        public static bool IsFlyingController(this ControllerRole role)
        {
            return role == ControllerRole.PrimaryFlying || role == ControllerRole.AlternativeFlying;
        }
    }
}