﻿namespace BloodflyControl.ControllerInput
{
    public enum ControllerAxis : int
    {
        DInput_X,
        DInput_Y,
        DInput_Z,
        DInput_RotationX,
        DInput_RotationY,
        DInput_RotationZ,
        DInput_Slider0,
        DInput_Slider1,

        XInput_LeftThumbX,
        XInput_LeftThumbY,
        XInput_RightThumbX,
        XInput_RightThumbY,
        XInput_LeftTrigger,
        XInput_RightTrigger
    }
}