﻿namespace BloodflyControl.ControllerInput
{
    [Flags]
    internal enum ControllerButton : ulong
    {
        None = 0UL,

        DInput_One = 1UL << 0,
        DInput_Two = 1UL << 1,
        DInput_Three = 1UL << 2,
        DInput_Four = 1UL << 3,
        DInput_Five = 1UL << 4,
        DInput_Six = 1UL << 5,
        DInput_Seven = 1UL << 6,
        DInput_Eight = 1UL << 7,
        DInput_Nine = 1UL << 8,
        DInput_Ten = 1UL << 9,
        DInput_Eleven = 1UL << 10,
        DInput_Twelve = 1UL << 11,
        DInput_DUp = 1UL << 12,
        DInput_DDown = 1UL << 13,
        DInput_DLeft = 1UL << 14,
        DInput_DRight = 1UL << 15,

        XInput_DPadUp = 1UL << 16,
        XInput_DPadDown = 1UL << 17,
        XInput_DPadLeft = 1UL << 18,
        XInput_DPadRight = 1UL << 19,
        XInput_Start = 1UL << 20,
        XInput_Back = 1UL << 21,
        XInput_LeftThumb = 1UL << 22,
        XInput_RightThumb = 1UL << 23,
        XInput_LeftShoulder = 1UL << 24,
        XInput_RightShoulder = 1UL << 25,

        // note: there is a gap in values here
        XInput_A = 1UL << 28,
        XInput_B = 1UL << 29,
        XInput_X = 1UL << 30,
        XInput_Y = 1UL << 31,
    }
}