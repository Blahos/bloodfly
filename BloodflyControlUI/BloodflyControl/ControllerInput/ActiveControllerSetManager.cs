﻿using BloodflyControl.ControllerInput.Interfaces;

namespace BloodflyControl.ControllerInput
{
    internal class ActiveControllerSetManager : IActiveControllerSetManager
    {
        private readonly IPlaneControllerProvider planeControllerProvider;
        private readonly object activeControllerSetLock = new object();

        public ActiveControllerSetManager(IPlaneControllerProvider planeControllerProvider)
        {
            this.planeControllerProvider = planeControllerProvider;
            planeControllerProvider.ControllersChanged += PlaneControllerProvider_ControllersChanged;
            UpdateActiveControllerSet();
        }

        private void PlaneControllerProvider_ControllersChanged()
        {
            UpdateActiveControllerSet();
        }

        private ControllerSet desiredActiveControllerSet = ControllerSet.Primary;

        public ControllerSet DesiredActiveControllerSet
        {
            get { return desiredActiveControllerSet; }
            set
            {
                desiredActiveControllerSet = value;
                UpdateActiveControllerSet();
            }
        }

        public ControllerSet? ActiveControllerSet { get; set; }

        public event Action? ActiveControllerSetChanged;

        public bool IsControllerActive(ControllerRole controllerRole)
        {
            var activeSet = ActiveControllerSet;
            if (activeSet == null) return false;
            if (activeSet == ControllerSet.Primary)
            {
                return controllerRole == ControllerRole.PrimaryFlying || controllerRole == ControllerRole.Assisting;
            }

            if (activeSet == ControllerSet.Alternative)
            {
                return controllerRole == ControllerRole.AlternativeFlying || controllerRole == ControllerRole.Assisting;
            }

            throw new Exception($"Unrecognised controller set: {activeSet}");
        }

        private void UpdateActiveControllerSet()
        {
            var raiseEvent = false;
            lock (activeControllerSetLock)
            {
                var currentControllerSet = ActiveControllerSet;
                var newControllerSet = EvaluateActiveControllerSet();
                ActiveControllerSet = newControllerSet;
                raiseEvent = currentControllerSet != newControllerSet;
            }

            if (raiseEvent)
                ActiveControllerSetChanged?.Invoke();
        }

        private ControllerSet? EvaluateActiveControllerSet()
        {
            var desiredSet = DesiredActiveControllerSet;
            var connectedControllers = planeControllerProvider.GetControllersConnectionStatus();
            if (desiredSet == ControllerSet.Alternative && connectedControllers[ControllerRole.AlternativeFlying])
            {
                return ControllerSet.Alternative;
            }

            if (connectedControllers[ControllerRole.PrimaryFlying])
            {
                return ControllerSet.Primary;
            }

            return null;
        }
    }
}