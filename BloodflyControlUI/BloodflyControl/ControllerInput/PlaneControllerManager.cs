﻿using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.PlaneControl;
using BloodflyControl.Utils;

namespace BloodflyControl.ControllerInput
{
    internal class PlaneControllerManager(
        IControllerFactory controllerFactory,
        IControllerInputStateAggregatorFactory controllerInputStateAggregatorFactory)
        : IPlaneControllerManager
    {
        private readonly struct ControllerTools
        {
            public IControllerId ControllerId { get; init; }
            public IController Controller { get; init; }
            public IControllerInputStateAggregator InputStateAggregator { get; init; }
        }

        private readonly ControllerRolesValues<ControllerTools?> controllers = new();
        private readonly object controllersLock = new();

        public event Action? ControllersChanged;

        public ControllerRolesValues<bool> GetControllersConnectionStatus()
        {
            var result = new ControllerRolesValues<bool>();
            lock (controllersLock)
            {
                foreach (var role in EnumValuesProvider.ControllerRoles)
                {
                    result[role] = controllers[role].HasValue && controllers[role]!.Value.Controller.IsConnected;
                }
            }

            return result;
        }

        public ControllerRolesValues<IControllerId?> GetAssignedControllers()
        {
            var result = new ControllerRolesValues<IControllerId?>();
            lock (controllersLock)
            {
                foreach (var role in EnumValuesProvider.ControllerRoles)
                {
                    if (controllers[role].HasValue)
                        result[role] = controllers[role]!.Value.ControllerId;
                }
            }

            return result;
        }

        public void SetControllerForRole(ControllerRole role, IControllerId? controllerId)
        {
            var controllerRemoved = false;
            var controllerAdded = false;
            lock (controllersLock)
            {
                if (IsAlreadyAssigned(role, controllerId)) return;
                controllerRemoved = RemoveControllerIfPresent(role);
                if (controllerId != null)
                {
                    AddController(role, controllerId);
                    controllerAdded = true;
                }
            }

            if (controllerRemoved || controllerAdded)
                OnControllersChanged();
        }

        public ControllerInputStateEx GetControllerInput(ControllerRole role)
        {
            lock (controllersLock)
            {
                var controllerTools = controllers[role];
                if (controllerTools == null) return ControllerInputStateEx.Default;

                return controllerTools.Value.InputStateAggregator.GetInput();
            }
        }

        private bool IsAlreadyAssigned(ControllerRole role, IControllerId? controllerId)
        {
            lock (controllersLock)
            {
                return (controllerId == null && controllers[role] == null)
                       ||
                       controllers[role] != null && controllerId == controllers[role]!.Value.ControllerId;
            }
        }

        private bool RemoveControllerIfPresent(ControllerRole role)
        {
            var controllerTools = controllers[role];
            if (controllerTools != null)
            {
                controllerTools.Value.Controller.IsConnectedChanged -= OnControllersChanged;
                controllerTools.Value.Controller.Dispose();
                controllers[role] = null;
                return true;
            }

            return false;
        }

        private void AddController(ControllerRole role, IControllerId controllerId)
        {
            var controller = controllerFactory.CreateController(controllerId);
            var newControllerTools = new ControllerTools
            {
                ControllerId = controllerId,
                Controller = controller,
                InputStateAggregator =
                    controllerInputStateAggregatorFactory.CreateControllerInputStateAggregator(controller)
            };
            newControllerTools.Controller.IsConnectedChanged += OnControllersChanged;
            controllers[role] = newControllerTools;
        }

        private void OnControllersChanged()
        {
            ControllersChanged?.Invoke();
        }
    }
}