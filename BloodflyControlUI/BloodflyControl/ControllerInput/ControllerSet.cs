﻿namespace BloodflyControl.ControllerInput
{
    internal enum ControllerSet
    {
        Primary,
        Alternative
    }
}