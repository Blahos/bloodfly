﻿using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;
using BloodflyControl.Utils;
using SharpDX.XInput;

namespace BloodflyControl.ControllerInput.XInput
{
    internal class XInputController : IController
    {
        private readonly Controller controller;
        private readonly IXInputButtonConverter xInputButtonConverter;
        private readonly ISerialTimer isConnectedScannerTimer;
        private readonly ILogger logger;

        public XInputController(XInputControllerId controllerId, IXInputButtonConverter xInputButtonConverter,
            ISerialTimerFactory serialTimerFactory, ILogger logger)
        {
            this.xInputButtonConverter = xInputButtonConverter;
            controller = new Controller((UserIndex)controllerId.UserIndex);
            isConnectedScannerTimer =
                serialTimerFactory.CreateSerialTimer(ConnectionStateScannerCallback, TimeSpan.FromSeconds(0.2));
            this.logger = logger;
        }

        private bool isConnected = false;
        public bool IsConnected => isConnected;

        public event Action? IsConnectedChanged;

        private void ConnectionStateScannerCallback()
        {
            var isConnectedNew = controller.IsConnected;
            if (isConnectedNew != isConnected)
            {
                isConnected = isConnectedNew;
                OnIsConnectedChanged();
            }
        }

        public void Dispose()
        {
            isConnectedScannerTimer.Terminate(true);
        }

        public ControllerInputState GetInputState()
        {
            var state = TryGetGamepadState();
            if (state == null) return ControllerInputState.Default;

            return new ControllerInputState(
                xInputButtonConverter.GetButtons(state.Value.Buttons),
                GetAxesValues(state.Value),
                TimeUtils.GetTimeElapsedSinceAppStart());
        }

        public static ControllerAxesValues<double> GetAxesValues(Gamepad state)
        {
            var result = new ControllerAxesValues<double>
            {
                [ControllerAxis.XInput_LeftThumbX] = GetNormalizedShortAxisValue(state.LeftThumbX),
                [ControllerAxis.XInput_LeftThumbY] = GetNormalizedShortAxisValue(state.LeftThumbY),
                [ControllerAxis.XInput_RightThumbX] = GetNormalizedShortAxisValue(state.RightThumbX),
                [ControllerAxis.XInput_RightThumbY] = GetNormalizedShortAxisValue(state.RightThumbY),
                [ControllerAxis.XInput_LeftTrigger] = GetNormalizedByteAxisValue(state.LeftTrigger),
                [ControllerAxis.XInput_RightTrigger] = GetNormalizedByteAxisValue(state.RightTrigger)
            };

            return result;
        }

        private void OnIsConnectedChanged()
        {
            IsConnectedChanged?.Invoke();
        }

        public ControllerButton GetPressedButtons()
        {
            var state = TryGetGamepadState();
            if (state == null) return ControllerButton.None;
            return xInputButtonConverter.GetButtons(state.Value.Buttons);
        }

        private Gamepad? TryGetGamepadState()
        {
            if (!IsConnected) return null;
            try
            {
                return controller.GetState().Gamepad;
            }
            catch (Exception)
            {
                logger.LogMessage("Failed to get gamepad state", LogLevel.Error);
                return null;
            }
        }

        private static double GetNormalizedShortAxisValue(short rawAxisValue)
        {
            var val = (double)rawAxisValue / short.MaxValue;
            return MathUtils.ClipToLimits(val, -1.0, 1.0);
        }

        private static double GetNormalizedByteAxisValue(byte rawAxisValue)
        {
            var val = (double)rawAxisValue / byte.MaxValue;
            return MathUtils.ClipToLimits(val, -1.0, 1.0);
        }
    }
}