﻿using BloodflyControl.ControllerInput.Interfaces;
using SharpDX.XInput;

namespace BloodflyControl.ControllerInput.XInput
{
    internal sealed class XInputControllerId(int userIndex) : IControllerId
    {
        public int UserIndex { get; } = userIndex;

        public string Description => $"XInput {(UserIndex)UserIndex}";

        public bool Equals(IControllerId? other)
        {
            if (other is XInputControllerId xinput)
            {
                return xinput.UserIndex == UserIndex;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return UserIndex.GetHashCode();
        }

        public override bool Equals(object? obj)
        {
            if (obj is XInputControllerId xinput)
            {
                return Equals(xinput);
            }

            return base.Equals(obj);
        }
    }
}