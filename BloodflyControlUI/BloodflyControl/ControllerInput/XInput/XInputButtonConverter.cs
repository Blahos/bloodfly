﻿using BloodflyControl.ControllerInput.Interfaces;
using SharpDX.XInput;

namespace BloodflyControl.ControllerInput.XInput
{
    internal class XInputButtonConverter : IXInputButtonConverter
    {
        public ControllerButton GetButtons(GamepadButtonFlags xInputGamepadButtonFlags)
        {
            return (ControllerButton)((ulong)xInputGamepadButtonFlags << 16);
        }
    }
}