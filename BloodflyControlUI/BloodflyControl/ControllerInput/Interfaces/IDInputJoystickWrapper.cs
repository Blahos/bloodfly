﻿using SharpDX.DirectInput;

namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IDInputJoystickWrapper : IDisposable
    {
        JoystickState? TryGetState();
        bool IsConnected();
        void TryRefresh();
        ControllerAxesValues<bool> GetAvailableAxes();
    }
}