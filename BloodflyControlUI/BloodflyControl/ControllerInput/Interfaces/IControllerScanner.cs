﻿using BloodflyControl.Framework.Interfaces;

namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IControllerScanner : IGenericStateProvider<AvailableControllersState>, IDisposable
    {
        bool SkipDInputForXBoxGamepads { get; set; }
    }
}