﻿namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IDInputJoystickWrapperFactory
    {
        IDInputJoystickWrapper CreateWrapper(Guid guid);
    }
}