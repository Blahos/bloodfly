﻿using SharpDX.DirectInput;

namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IDInputJoystickAxesParser
    {
        ControllerAxesValues<double> GetButtons(JoystickState joystickState, ControllerAxesValues<bool> availableAxes);
    }
}