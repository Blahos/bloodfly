﻿using SharpDX.DirectInput;

namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IDInputJoystickButtonParser
    {
        ControllerButton GetButtons(JoystickState joystickState);
    }
}