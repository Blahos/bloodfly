﻿using SharpDX.XInput;

namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IXInputButtonConverter
    {
        ControllerButton GetButtons(GamepadButtonFlags xInputGamepadButtonFlags);
    }
}