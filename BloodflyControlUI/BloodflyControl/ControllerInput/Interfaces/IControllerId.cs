﻿namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IControllerId : IEquatable<IControllerId>
    {
        string Description { get; }
    }
}