﻿namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IControllerInputStateAggregator
    {
        ControllerInputStateEx GetInput();
    }
}