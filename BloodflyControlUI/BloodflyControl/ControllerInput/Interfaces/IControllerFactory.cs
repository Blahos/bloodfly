﻿namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IControllerFactory
    {
        IController CreateController(IControllerId controllerId);
    }
}