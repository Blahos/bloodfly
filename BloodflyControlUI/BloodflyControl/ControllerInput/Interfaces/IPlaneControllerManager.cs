﻿namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IPlaneControllerManager : IPlaneControllerProvider
    {
        void SetControllerForRole(ControllerRole role, IControllerId? controllerId);
    }
}