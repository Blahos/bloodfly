﻿namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IActiveControllerSetProvider
    {
        ControllerSet? ActiveControllerSet { get; }
        event Action? ActiveControllerSetChanged;
        bool IsControllerActive(ControllerRole controllerRole);
    }
}