﻿namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IController : IDisposable
    {
        ControllerInputState GetInputState();
        bool IsConnected { get; }
        event Action? IsConnectedChanged;
    }
}