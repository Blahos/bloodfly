﻿using SharpDX.DirectInput;

namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IDInputProvider
    {
        DirectInput GetInstance();
    }
}