﻿namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IControllerInputStateAggregatorFactory
    {
        IControllerInputStateAggregator CreateControllerInputStateAggregator(IController controller);
    }
}