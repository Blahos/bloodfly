﻿namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IActiveControllerSetManager : IActiveControllerSetProvider
    {
        ControllerSet DesiredActiveControllerSet { get; set; }
    }
}