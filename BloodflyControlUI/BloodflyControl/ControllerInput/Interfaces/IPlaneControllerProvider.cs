﻿using BloodflyControl.PlaneControl;

namespace BloodflyControl.ControllerInput.Interfaces
{
    internal interface IPlaneControllerProvider
    {
        event Action? ControllersChanged;
        ControllerRolesValues<IControllerId?> GetAssignedControllers();
        ControllerRolesValues<bool> GetControllersConnectionStatus();
        ControllerInputStateEx GetControllerInput(ControllerRole role);
    }
}