﻿using BloodflyControl.ControllerInput.Interfaces;

namespace BloodflyControl.ControllerInput
{
    internal class ControllerInputStateAggregatorFactory : IControllerInputStateAggregatorFactory
    {
        public IControllerInputStateAggregator CreateControllerInputStateAggregator(IController controller)
        {
            return new ControllerInputStateAggregator(controller);
        }
    }
}