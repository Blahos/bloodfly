﻿using BloodflyControl.Utils;

namespace BloodflyControl.ControllerInput
{
    internal readonly struct ControllerInputState
    {
        public ControllerButton Buttons { get; } = ControllerButton.None;
        public ControllerAxesValues<double> Axes { get; } = new();
        public TimeSpan TimeStamp { get; } = TimeUtils.GetTimeElapsedSinceAppStart();

        public static ControllerInputState Default { get; } = new();

        public ControllerInputState()
        {
        }

        public ControllerInputState(ControllerButton buttons, ControllerAxesValues<double> axes, TimeSpan timeStamp)
        {
            Buttons = buttons;
            Axes = axes;
            TimeStamp = timeStamp;
        }
    }
}