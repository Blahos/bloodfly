﻿using BloodflyControl.ControllerInput.Interfaces;

namespace BloodflyControl.ControllerInput
{
    internal class ControllerInputStateAggregator(IController controller) : IControllerInputStateAggregator
    {
        private readonly IController controller = controller;

        private ControllerInputState lastInput = ControllerInputState.Default;

        public ControllerInputStateEx GetInput()
        {
            var controllerInputState = controller.GetInputState();
            var result = new ControllerInputStateEx(
                controllerInputState.Buttons,
                (lastInput.Buttons ^ controllerInputState.Buttons) & controllerInputState.Buttons,
                controllerInputState.Axes,
                controllerInputState.TimeStamp - lastInput.TimeStamp);

            lastInput = controllerInputState;
            return result;
        }
    }
}