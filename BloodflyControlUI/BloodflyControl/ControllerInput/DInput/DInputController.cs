﻿using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;
using BloodflyControl.Utils;

namespace BloodflyControl.ControllerInput.DInput
{
    internal class DInputController : IController
    {
        private readonly IDInputJoystickAxesParser dInputJoystickAxesParser;
        private readonly IDInputJoystickButtonParser dInputJoystickButtonParser;
        private readonly ISerialTimer isConnectedScannerTimer;
        private readonly IDInputJoystickWrapper joystick;
        private readonly ILogger logger;

        private bool isConnected = false;

        public DInputController(
            DInputControllerId controllerId,
            IDInputJoystickButtonParser dInputJoystickButtonParser,
            IDInputJoystickAxesParser dInputJoystickAxesParser,
            IDInputJoystickWrapperFactory dInputJoystickWrapperFactory,
            ISerialTimerFactory serialTimerFactory,
            ILogger logger)
        {
            this.dInputJoystickButtonParser = dInputJoystickButtonParser;
            this.dInputJoystickAxesParser = dInputJoystickAxesParser;
            joystick = dInputJoystickWrapperFactory.CreateWrapper(controllerId.Guid);
            this.logger = logger;
            isConnectedScannerTimer =
                serialTimerFactory.CreateSerialTimer(ConnectionStateScannerCallback, TimeSpan.FromSeconds(0.2));
        }

        public bool IsConnected => isConnected;

        public event Action? IsConnectedChanged;

        public ControllerInputState GetInputState()
        {
            if (!isConnected) return ControllerInputState.Default;
            var state = joystick.TryGetState();
            if (state == null) return ControllerInputState.Default;
            var availableAxes = joystick.GetAvailableAxes();

            return new ControllerInputState(
                dInputJoystickButtonParser.GetButtons(state),
                dInputJoystickAxesParser.GetButtons(state, availableAxes), TimeUtils.GetTimeElapsedSinceAppStart());
        }

        public void Dispose()
        {
            isConnectedScannerTimer.Terminate(true);
            joystick.Dispose();
        }

        private void ConnectionStateScannerCallback()
        {
            if (!joystick.IsConnected())
            {
                joystick.TryRefresh();
            }

            var isConnectedNew = joystick.IsConnected();
            if (isConnectedNew != isConnected)
            {
                isConnected = isConnectedNew;
                IsConnectedChanged?.Invoke();
            }
        }
    }
}