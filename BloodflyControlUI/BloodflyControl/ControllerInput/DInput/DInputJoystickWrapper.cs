﻿using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Logging;
using BloodflyControl.Utils;
using SharpDX.DirectInput;

namespace BloodflyControl.ControllerInput.DInput
{
    internal class DInputJoystickWrapper(
        Guid guid,
        IDInputProvider directInputProvider,
        ILogger logger)
        : IDInputJoystickWrapper
    {
        private readonly ControllerAxesValues<bool> availableAxes = new();
        private readonly object joystickLock = new();
        private Joystick? joystick = null;

        public JoystickState? TryGetState()
        {
            lock (joystickLock)
            {
                if (joystick == null || joystick.IsDisposed) return null;
                try
                {
                    return joystick.GetCurrentState();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public bool IsConnected()
        {
            var directInput = directInputProvider.GetInstance();
            return directInput.IsDeviceAttached(guid) && IsCurrentJoystickInstanceValid();
        }

        public void TryRefresh()
        {
            lock (joystickLock)
            {
                joystick?.Dispose();
                joystick = TryCreateNewJoystickInstance();
                UpdateAvailableAxes();
            }
        }

        public ControllerAxesValues<bool> GetAvailableAxes()
        {
            return availableAxes;
        }

        public void Dispose()
        {
            lock (joystickLock)
            {
                joystick?.Dispose();
            }
        }

        private HashSet<string> GetAvailableAxesNames()
        {
            var result = new HashSet<string>();
            lock (joystickLock)
            {
                if (!IsConnected()) return result;

                foreach (var axis in DInputAxesInfo.SupportedAxes.Keys)
                {
                    if (IsAxisAvailable(axis))
                        result.Add(axis);
                }
            }

            return result;
        }

        private bool IsAxisAvailable(string axisName)
        {
            try
            {
                var prop = joystick?.GetObjectPropertiesByName(axisName);
                if (prop == null) return false;
                var deadzone =
                    prop.DeadZone; // getting a property of the object is what can throw an exception if the object (axis) is not available
                return deadzone > 0 || deadzone == 0 || deadzone < 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool IsJoystickStateObtainable()
        {
            return TryGetState() != null;
        }


        private bool IsCurrentJoystickInstanceValid()
        {
            return IsJoystickStateObtainable();
        }

        private Joystick? TryCreateNewJoystickInstance()
        {
            try
            {
                var directInput = directInputProvider.GetInstance();
                if (!directInput.IsDeviceAttached(guid))
                {
                    return null;
                }

                joystick = new Joystick(directInput, guid);
                joystick.Acquire();
                return joystick;
            }
            catch (Exception)
            {
                logger.LogMessage("Failed to create new joystick instance", LogLevel.Error);
                return null;
            }
        }

        private void UpdateAvailableAxes()
        {
            foreach (var axis in EnumValuesProvider.ControllerAxes)
            {
                availableAxes[axis] = false;
            }

            var joystickAvailableAxes = GetAvailableAxesNames();

            foreach (var axis in DInputAxesInfo.SupportedAxes)
            {
                if (joystickAvailableAxes.Contains(axis.Key))
                {
                    availableAxes[axis.Value] = true;
                }
            }
        }
    }
}