﻿using BloodflyControl.ControllerInput.Interfaces;
using SharpDX.DirectInput;

namespace BloodflyControl.ControllerInput.DInput
{
    internal sealed class DInputControllerId(string name, Guid guid) : IControllerId
    {
        private readonly Guid guid = guid;
        public Guid Guid => guid;

        public string Description => $"{name} ({guid})";

        public bool Equals(IControllerId? other)
        {
            if (other is DInputControllerId directInput)
            {
                return guid == directInput.guid;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return guid.GetHashCode();
        }

        public override bool Equals(object? obj)
        {
            if (obj is DInputControllerId directInput)
            {
                return Equals(directInput);
            }

            return base.Equals(obj);
        }
    }
}