﻿using BloodflyControl.ControllerInput.Interfaces;
using SharpDX.DirectInput;

namespace BloodflyControl.ControllerInput.DInput
{
    internal class DInputJoystickButtonParser : IDInputJoystickButtonParser
    {
        public ControllerButton GetButtons(JoystickState joystickState)
        {
            var result = ControllerButton.None;
            for (int i = 0; i < 12; i++)
            {
                if (joystickState.Buttons[i])
                    result |= (ControllerButton)(1 << i);
            }

            result |= GetDPad(joystickState);
            return result;
        }

        private ControllerButton GetDPad(JoystickState joystickState)
        {
            var result = ControllerButton.None;
            if (joystickState.PointOfViewControllers.Length <= 0) return result;
            var lDPadVal = joystickState.PointOfViewControllers[0];

            switch (lDPadVal)
            {
                case 0:
                    result |= ControllerButton.DInput_DUp;
                    break;
                case 4500:
                    result |= ControllerButton.DInput_DUp | ControllerButton.DInput_DRight;
                    break;
                case 9000:
                    result |= ControllerButton.DInput_DRight;
                    break;
                case 13500:
                    result |= ControllerButton.DInput_DRight | ControllerButton.DInput_DDown;
                    break;
                case 18000:
                    result |= ControllerButton.DInput_DDown;
                    break;
                case 22500:
                    result |= ControllerButton.DInput_DDown | ControllerButton.DInput_DLeft;
                    break;
                case 27000:
                    result |= ControllerButton.DInput_DLeft;
                    break;
                case 31500:
                    result |= ControllerButton.DInput_DLeft | ControllerButton.DInput_DUp;
                    break;
                default:
                    break;
            }

            return result;
        }
    }
}