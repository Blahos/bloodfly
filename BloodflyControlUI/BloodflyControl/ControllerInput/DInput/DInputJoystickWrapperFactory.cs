﻿using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Logging;

namespace BloodflyControl.ControllerInput.DInput
{
    internal class DInputJoystickWrapperFactory(IDInputProvider directInputProvider, ILogger logger)
        : IDInputJoystickWrapperFactory
    {
        public IDInputJoystickWrapper CreateWrapper(Guid guid)
        {
            return new DInputJoystickWrapper(guid, directInputProvider, logger);
        }
    }
}