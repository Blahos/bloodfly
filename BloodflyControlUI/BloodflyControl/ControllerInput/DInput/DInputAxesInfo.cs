﻿namespace BloodflyControl.ControllerInput.DInput
{
    internal static class DInputAxesInfo
    {
        public static IReadOnlyDictionary<string, ControllerAxis> SupportedAxes { get; } =
            new Dictionary<string, ControllerAxis>
            {
                { "X", ControllerAxis.DInput_X },
                { "Y", ControllerAxis.DInput_Y },
                { "Z", ControllerAxis.DInput_Z },
                { "RotationX", ControllerAxis.DInput_RotationX },
                { "RotationY", ControllerAxis.DInput_RotationY },
                { "RotationZ", ControllerAxis.DInput_RotationZ },
                { "Sliders0", ControllerAxis.DInput_Slider0 },
                { "Sliders1", ControllerAxis.DInput_Slider1 }
            };
    }
}