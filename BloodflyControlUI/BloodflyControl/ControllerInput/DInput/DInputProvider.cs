﻿using BloodflyControl.ControllerInput.Interfaces;
using SharpDX.DirectInput;

namespace BloodflyControl.ControllerInput.DInput
{
    internal class DInputProvider : IDInputProvider
    {
        private readonly DirectInput instance = new();

        public DirectInput GetInstance()
        {
            return instance;
        }
    }
}