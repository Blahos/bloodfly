﻿using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Utils;
using SharpDX.DirectInput;

namespace BloodflyControl.ControllerInput.DInput
{
    internal class DInputJoystickAxesParser : IDInputJoystickAxesParser
    {
        public ControllerAxesValues<double> GetButtons(JoystickState joystickState,
            ControllerAxesValues<bool> availableAxes)
        {
            var result = new ControllerAxesValues<double>();

            if (availableAxes[ControllerAxis.DInput_X])
                result[ControllerAxis.DInput_X] = GetNormalizedShortAxisValue(joystickState.X);
            if (availableAxes[ControllerAxis.DInput_Y])
                result[ControllerAxis.DInput_Y] = GetNormalizedShortAxisValue(joystickState.Y);
            if (availableAxes[ControllerAxis.DInput_Z])
                result[ControllerAxis.DInput_Z] = GetNormalizedShortAxisValue(joystickState.Z);
            if (availableAxes[ControllerAxis.DInput_RotationX])
                result[ControllerAxis.DInput_RotationX] = GetNormalizedShortAxisValue(joystickState.RotationX);
            if (availableAxes[ControllerAxis.DInput_RotationY])
                result[ControllerAxis.DInput_RotationY] = GetNormalizedShortAxisValue(joystickState.RotationY);
            if (availableAxes[ControllerAxis.DInput_RotationZ])
                result[ControllerAxis.DInput_RotationZ] = GetNormalizedShortAxisValue(joystickState.RotationZ);
            if (availableAxes[ControllerAxis.DInput_Slider0])
                result[ControllerAxis.DInput_Slider0] = GetNormalizedShortAxisValue(joystickState.Sliders[0]);
            if (availableAxes[ControllerAxis.DInput_Slider1])
                result[ControllerAxis.DInput_Slider1] = GetNormalizedShortAxisValue(joystickState.Sliders[1]);

            return result;
        }

        private static double GetNormalizedShortAxisValue(int rawAxisValue)
        {
            var val = (double)(rawAxisValue - short.MaxValue) / short.MaxValue;
            return MathUtils.ClipToLimits(val, -1.0, 1.0);
        }
    }
}