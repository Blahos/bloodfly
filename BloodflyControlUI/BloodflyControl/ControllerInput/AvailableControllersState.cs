﻿using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Framework.Interfaces;

namespace BloodflyControl.ControllerInput
{
    internal class AvailableControllersState : IState<AvailableControllersState>
    {
        private readonly HashSet<IControllerId> availableControllers = [];
        public IReadOnlyCollection<IControllerId> AvailableControllers => availableControllers;

        public AvailableControllersState()
        {
        }

        public AvailableControllersState(HashSet<IControllerId> availableControllers)
        {
            this.availableControllers = availableControllers;
        }

        public AvailableControllersState Clone()
        {
            return new AvailableControllersState([.. availableControllers]);
        }

        public bool Equals(AvailableControllersState? other)
        {
            if (other == null) return false;
            return availableControllers.SetEquals(other.availableControllers);
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as AvailableControllersState);
        }

        public override int GetHashCode()
        {
            return availableControllers.GetHashCode();
        }
    }
}