﻿using BloodflyControl.Utils;
using System.Collections;

namespace BloodflyControl.ControllerInput
{
    internal class ControllerAxesValues<T> : IEnumerable<KeyValuePair<ControllerAxis, T>>
    {
        private static readonly int arraySize = EnumValuesProvider.ControllerAxes.Count;
        private readonly T[] array = new T[arraySize];

        public T this[ControllerAxis axis]
        {
            get => array[(int)axis];
            set => array[(int)axis] = value;
        }

        public int Count { get; } = arraySize;

        public IEnumerator<KeyValuePair<ControllerAxis, T>> GetEnumerator()
        {
            foreach (var axis in EnumValuesProvider.ControllerAxes)
            {
                yield return new KeyValuePair<ControllerAxis, T>(axis, this[axis]);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}