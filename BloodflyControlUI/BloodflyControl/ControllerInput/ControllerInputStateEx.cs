﻿namespace BloodflyControl.ControllerInput
{
    internal readonly struct ControllerInputStateEx
    {
        public ControllerButton ButtonsHeldDown { get; } = ControllerButton.None;
        public ControllerButton ButtonsNewlyPressed { get; } = ControllerButton.None;
        public ControllerAxesValues<double> Axes { get; } = new();
        public TimeSpan TimeElapsedSincePreviousInput { get; } = TimeSpan.Zero;

        public static ControllerInputStateEx Default { get; } = new ControllerInputStateEx();

        public ControllerInputStateEx()
        {
        }

        public ControllerInputStateEx(ControllerButton buttonsHeldDown, ControllerButton buttonsNewlyPressed,
            ControllerAxesValues<double> axes, TimeSpan timeElapsedSincePreviousInput)
        {
            ButtonsHeldDown = buttonsHeldDown;
            ButtonsNewlyPressed = buttonsNewlyPressed;
            Axes = axes;
            TimeElapsedSincePreviousInput = timeElapsedSincePreviousInput;
        }
    }
}