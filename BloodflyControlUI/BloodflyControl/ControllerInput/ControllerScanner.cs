﻿using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Framework;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.ControllerInput.XInput;
using BloodflyControl.ControllerInput.DInput;
using SharpDX.DirectInput;
using SharpDX.XInput;

namespace BloodflyControl.ControllerInput
{
    internal class ControllerScanner(ISerialTimerFactory serialTimerFactory)
        : GenericStateScanner<AvailableControllersState>(serialTimerFactory, TimeSpan.FromSeconds(1)),
            IControllerScanner
    {
        private readonly DirectInput directInput = new();

        private readonly Controller[] xInputControllers =
        [
            new(UserIndex.One), new(UserIndex.Two),
            new(UserIndex.Three), new(UserIndex.Four)
        ];

        public bool SkipDInputForXBoxGamepads { get; set; } = false;

        protected override AvailableControllersState GetNewState()
        {
            var allDevices = GetXInputDevices();
            allDevices.UnionWith(GetDInputDevices());

            return new AvailableControllersState(allDevices);
        }

        private HashSet<IControllerId> GetXInputDevices()
        {
            return xInputControllers
                .Where(a => a.IsConnected)
                .Select(a => new XInputControllerId((int)a.UserIndex))
                .Cast<IControllerId>().ToHashSet();
        }

        private HashSet<IControllerId> GetDInputDevices()
        {
            var gamepads = directInput.GetDevices(
                SharpDX.DirectInput.DeviceType.Gamepad,
                DeviceEnumerationFlags.AttachedOnly);

            if (SkipDInputForXBoxGamepads)
            {
                gamepads = gamepads.Where(a => !a.ProductName.ToLower().Contains("xbox")).ToList();
            }

            var joysticks = directInput.GetDevices(
                SharpDX.DirectInput.DeviceType.Joystick,
                DeviceEnumerationFlags.AttachedOnly);

            return gamepads
                .Union(joysticks)
                .Select(a => new DInputControllerId(a.InstanceName, a.InstanceGuid))
                .Cast<IControllerId>()
                .ToHashSet();
        }

        protected override AvailableControllersState GetDefaultState()
        {
            return new(new());
        }
    }
}