﻿namespace BloodflyControl.ControllerInput
{
    public enum ControllerRole
    {
        PrimaryFlying = 0,
        AlternativeFlying = 1,
        Assisting = 2
    }
}