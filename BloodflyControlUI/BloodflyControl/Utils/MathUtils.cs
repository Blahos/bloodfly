﻿namespace BloodflyControl.Utils
{
    internal static class MathUtils
    {
        public static double ClipToLimits(double value, double min, double max)
        {
            if (value < min)
            {
                value = min;
            }

            if (value > max)
            {
                value = max;
            }

            return value;
        }

        public static double Interpolate(double x1, double y1, double x2, double y2, double x)
        {
            if (x1 == x2) throw new ArgumentException("X values must be different");
            return (x - x1) / (x2 - x1) * (y2 - y1);
        }
    }
}