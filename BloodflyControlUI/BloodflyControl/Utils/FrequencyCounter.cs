﻿namespace BloodflyControl.Utils
{
    internal class FrequencyCounter(TimeSpan windowSize)
    {
        private readonly Queue<DateTime> occurances = new Queue<DateTime>();

        public double AddOccurance()
        {
            occurances.Enqueue(DateTime.UtcNow);
            return GetFrequency();
        }

        public double GetFrequency()
        {
            UpdateQueue();
            return occurances.Count / windowSize.TotalSeconds;
        }

        private void UpdateQueue()
        {
            var now = DateTime.UtcNow;
            while (occurances.Count > 0 && (now - occurances.Peek()) > windowSize)
            {
                occurances.Dequeue();
            }
        }
    }
}