﻿using System.Diagnostics;

namespace BloodflyControl.Utils
{
    internal static class TimeUtils
    {
        private static readonly Stopwatch stopwatch = Stopwatch.StartNew();

        public static DateTime GetCurrentUtcTime()
        {
            return DateTime.UtcNow;
        }

        public static TimeSpan GetTimeElapsedSinceAppStart()
        {
            return stopwatch.Elapsed;
        }
    }
}