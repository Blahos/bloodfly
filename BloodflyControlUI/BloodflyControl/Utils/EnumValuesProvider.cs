﻿using BloodflyControl.ControllerInput;
using BloodflyControl.PlaneControl;

namespace BloodflyControl.Utils
{
    internal static class EnumValuesProvider
    {
        static EnumValuesProvider()
        {
            ThrowIfEnumArrayIsNotValid(ControllerAxes);
            ThrowIfEnumArrayIsNotValid(ControllerRoles);
            ThrowIfEnumArrayIsNotValid(PlaneAxes);
            ThrowIfEnumArrayIsNotValid(PlaneControlSurfaces);
            ThrowIfEnumArrayIsNotValid(ControlActions);
            ThrowIfEnumArrayIsNotValid(TrimOptions);
        }

        public static IReadOnlyList<ControllerAxis> ControllerAxes { get; } =
            Array.AsReadOnly(Enum.GetValues<ControllerAxis>().ToArray());

        public static IReadOnlyList<ControllerRole> ControllerRoles { get; } =
            Array.AsReadOnly(Enum.GetValues<ControllerRole>().ToArray());

        public static IReadOnlyList<PlaneAxis> PlaneAxes { get; } =
            Array.AsReadOnly(Enum.GetValues<PlaneAxis>().ToArray());

        public static IReadOnlyList<PlaneControlSurface> PlaneControlSurfaces { get; } =
            Array.AsReadOnly(Enum.GetValues<PlaneControlSurface>().ToArray());

        public static IReadOnlyList<ControlAction> ControlActions { get; } =
            Array.AsReadOnly(Enum.GetValues<ControlAction>().ToArray());

        public static IReadOnlyList<TrimOption> TrimOptions { get; } =
            Array.AsReadOnly(Enum.GetValues<TrimOption>().ToArray());

        private static void ThrowIfEnumArrayIsNotValid<T>(IReadOnlyList<T> enumArray) where T : Enum
        {
            // the enum must start from 0 and have contiguous values (ie 0, 1, 2, 3, 4, 5, ...)
            for (int i = 0; i < enumArray.Count; i++)
            {
                if (i != (int)ControllerAxes[i])
                    throw new Exception($"{nameof(T)} enum is not contiguous or starting from 0");
            }
        }
    }
}