﻿using BloodflyControl.ControllerInput;
using BloodflyControl.PlaneControl;

namespace BloodflyControl.Utils
{
    internal static class EnumUtils
    {
        public static ControllerRolesValues<bool> FromFlags(params ControllerRole[] flags)
        {
            var result = new ControllerRolesValues<bool>();
            foreach (var flag in flags)
            {
                result[flag] = true;
            }

            return result;
        }

        public static ControllerRolesValues<bool> FromHashSet(HashSet<ControllerRole> hashSet)
        {
            return FromFlags(hashSet.ToArray());
        }
    }
}