﻿using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;

namespace BloodflyControl.Framework
{
    internal class SerialTimerFactory(ILogger logger) : ISerialTimerFactory
    {
        public ISerialTimer CreateSerialTimer(Action callback, TimeSpan period)
        {
            return new SerialTimer(callback, period, logger);
        }
    }
}