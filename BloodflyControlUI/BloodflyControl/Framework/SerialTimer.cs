﻿using BloodflyControl.Logging;
using System.Diagnostics;
using BloodflyControl.Framework.Interfaces;

namespace BloodflyControl.Framework
{
    internal class SerialTimer : ISerialTimer
    {
        private enum SerialTimerProcessState
        {
            Idle,
            CallbackInProgress,
            Terminated
        }

        private class SerialTimerState
        {
            public SerialTimerState()
            {
            }

            public SerialTimerProcessState ProcessState { get; set; } = SerialTimerProcessState.Idle;
            public bool TerminateSignalled { get; set; } = false;
        }

        private readonly Timer timer;
        private readonly Action callback;
        private readonly ILogger logger;
        private readonly ManualResetEvent terminatedHandle = new(false);
        private readonly SerialTimerState state = new();

        public SerialTimer(Action callback, TimeSpan period, ILogger logger)
        {
            this.callback = callback;
            Period = period;
            this.logger = logger;
            timer = new Timer(Callback, null, 0, Timeout.Infinite);
        }

        public void Terminate(bool waitForFinish)
        {
            lock (state)
            {
                state.TerminateSignalled = true;
                if (state.ProcessState != SerialTimerProcessState.CallbackInProgress)
                {
                    state.ProcessState = SerialTimerProcessState.Terminated;
                    terminatedHandle.Set();
                }
            }

            if (waitForFinish)
            {
                terminatedHandle.WaitOne();
            }
        }

        public TimeSpan Period { get; set; }

        private void Callback(object? arg)
        {
            var sw = Stopwatch.StartNew();
            var updateSuccessful = UpdateState(SerialTimerProcessState.CallbackInProgress);
            if (!updateSuccessful)
            {
                DisposeTimer();
                return;
            }

            try
            {
                callback.Invoke();
            }
            catch (Exception ex)
            {
                logger.LogMessage($"Timer callback error: {ex}", LogLevel.Error);
            }
            finally
            {
                updateSuccessful = UpdateState(SerialTimerProcessState.Idle);
                if (updateSuccessful)
                {
                    UpdateTimer(sw);
                }
                else
                {
                    DisposeTimer();
                }
            }
        }

        private void UpdateTimer(Stopwatch timeOfPreviousLoopStart)
        {
            var nextPeriodMS = Period.TotalMilliseconds;
            while (true)
            {
                if (timeOfPreviousLoopStart.ElapsedMilliseconds >= nextPeriodMS)
                    break;
            }

            timer.Change(0, Timeout.Infinite);
        }

        private void DisposeTimer()
        {
            timer.Dispose();
        }

        private bool UpdateState(SerialTimerProcessState newState)
        {
            lock (state)
            {
                if (state.TerminateSignalled)
                {
                    state.ProcessState = SerialTimerProcessState.Terminated;
                    terminatedHandle.Set();
                    return false;
                }

                state.ProcessState = newState;
                return true;
            }
        }
    }
}