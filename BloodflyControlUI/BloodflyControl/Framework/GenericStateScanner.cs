﻿using BloodflyControl.Framework.Interfaces;

namespace BloodflyControl.Framework
{
    internal abstract class GenericStateScanner<T> : IDisposable, IGenericStateProvider<T> where T : IState<T>
    {
        private readonly object stateLock = new();
        private readonly ISerialTimer timer;
        private T state;
        public event Action? StateChanged;

        public GenericStateScanner(ISerialTimerFactory serialTimerFactory, TimeSpan period)
        {
            state = GetDefaultState();
            timer = serialTimerFactory.CreateSerialTimer(Callback, period);
        }

        private void Callback()
        {
            UpdateStateIfNeeded(GetNewState());
        }

        private void UpdateStateIfNeeded(T newState)
        {
            bool stateChanged = false;
            lock (stateLock)
            {
                if (!newState.Equals(state))
                {
                    state = newState;
                    stateChanged = true;
                }
            }

            if (stateChanged) OnStateChanged();
        }

        protected abstract T GetNewState();
        protected abstract T GetDefaultState();

        public T State
        {
            get
            {
                lock (stateLock)
                {
                    return state.Clone();
                }
            }
        }

        private void OnStateChanged()
        {
            StateChanged?.Invoke();
        }

        public void Dispose()
        {
            timer.Terminate(false);
        }
    }
}