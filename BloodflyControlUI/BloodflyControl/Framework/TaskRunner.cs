﻿using BloodflyControl.Framework.Interfaces;

namespace BloodflyControl.Framework
{
    internal class TaskRunner : ITaskRunner
    {
        public void RunTask(Action action)
        {
            Task.Run(action);
        }

        public void RunTaskWithInitialDelay(Action action, TimeSpan delay)
        {
            RunTask(() =>
            {
                Thread.Sleep(delay);
                action();
            });
        }
    }
}