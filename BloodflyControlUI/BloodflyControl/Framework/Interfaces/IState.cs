﻿namespace BloodflyControl.Framework.Interfaces
{
    internal interface IState<T> : IEquatable<T>, ICloneable<T>
    {
    }
}