﻿namespace BloodflyControl.Framework.Interfaces
{
    internal interface ISerialTimer
    {
        void Terminate(bool waitForFinish);
        TimeSpan Period { get; set; }
    }
}