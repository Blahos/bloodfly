﻿namespace BloodflyControl.Framework.Interfaces
{
    internal interface ISerialTimerFactory
    {
        ISerialTimer CreateSerialTimer(Action callback, TimeSpan period);
    }
}