﻿namespace BloodflyControl.Framework.Interfaces
{
    internal interface IGenericStateProvider<T> where T : IState<T>
    {
        T State { get; }
        event Action? StateChanged;
    }
}