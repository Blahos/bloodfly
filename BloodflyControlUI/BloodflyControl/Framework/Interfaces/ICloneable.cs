﻿namespace BloodflyControl.Framework.Interfaces
{
    internal interface ICloneable<T>
    {
        T Clone();
    }
}