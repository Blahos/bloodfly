﻿namespace BloodflyControl.Framework.Interfaces
{
    internal interface ITaskRunner
    {
        void RunTask(Action action);
        void RunTaskWithInitialDelay(Action action, TimeSpan delay);
    }
}