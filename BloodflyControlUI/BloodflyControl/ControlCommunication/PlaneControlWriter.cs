﻿using BloodflyControl.ControlCommunication.Interfaces;
using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.ControlCommunication
{
    internal class PlaneControlWriter(
        ISerialPortWriter serialPortWriter,
        IMessageAssembler messageAssembler,
        ISafePlaneAxesValuesProvider safePlaneAxesValuesProvider,
        IMessageTrimValuesProvider messageTrimValuesProvider,
        IControlSurfaceMaxAngleProvider controlSurfaceMaxAngleProvider,
        IMessageParametersProvider messageParametersProvider)
        : IPlaneControlWriter
    {
        private readonly byte[] writeBuffer = new byte[messageAssembler.MessageSize];

        public void Write()
        {
            var planeAxesValues = safePlaneAxesValuesProvider.GetValues();
            var trimValues = messageTrimValuesProvider.GetTrimValues();
            var maxControlSurfaceAngles = controlSurfaceMaxAngleProvider.GetMaxAngles();
            var messageParameters = messageParametersProvider.GetMessageParameters();

            messageAssembler.AssembleMessageToBuffer(
                planeAxesValues,
                trimValues,
                maxControlSurfaceAngles,
                messageParameters,
                writeBuffer);

            serialPortWriter.Write(writeBuffer);
        }
    }
}