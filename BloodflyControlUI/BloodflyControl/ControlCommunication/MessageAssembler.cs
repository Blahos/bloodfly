﻿using BloodflyControl.ControlCommunication.Interfaces;
using BloodflyControl.PlaneControl;

namespace BloodflyControl.ControlCommunication
{
    internal class MessageAssembler : IMessageAssembler
    {
        private const int messageHeadSize = 3;
        private const int messageTailSize = 3;

        private const int messagePlaneAxesCount = 4;
        private const int messagePlaneAxisSize = sizeof(float);
        private const int messageTrimCount = 4;
        private const int messageFlapOffsetSize = sizeof(short);
        private const int messageMaxAnglesCount = 4;

        private const int messageSize =
            messageHeadSize +
            messageTailSize + // message head and tail - special characters that mark start and end of the message
            messagePlaneAxesCount * messagePlaneAxisSize + // plane axes values
            messageTrimCount + // trim values
            messageFlapOffsetSize + // flap offset
            1 + // enable plane feedback
            messageMaxAnglesCount + // max angles of plane control surfaces
            1 + // radio PA level
            1 + // radio channel
            1; // signal byte

        private readonly ThreadLocal<float[]> planeAxesValuesBuffer = new(() => new float[messagePlaneAxesCount]);
        private readonly ThreadLocal<short[]> planeFlapOffsetBuffer = new(() => new short[1]);

        public int MessageSize => messageSize;

        public void AssembleMessageToBuffer(
            PlaneAxesValues<double> planeAxesValues,
            PlaneControlSurfacesValues<byte> planeControlSurfacesTrimValues,
            PlaneControlSurfacesValues<byte> planeControlSurfacesMaxAngles,
            MessageParameters messageParameters,
            byte[] messageBuffer)
        {
            if (messageBuffer.Length != messageSize)
                throw new ArgumentException("Message buffer has wrong size");

            // message head
            messageBuffer[0] = 255;
            messageBuffer[1] = 0;
            messageBuffer[2] = 127;

            var currentBufferPosition = 3;

            // plane axes values
            var axesValuesTmp = planeAxesValuesBuffer.Value!;
            axesValuesTmp[0] = (float)planeAxesValues[PlaneAxis.Elevator];
            axesValuesTmp[1] = (float)planeAxesValues[PlaneAxis.Rudder];
            axesValuesTmp[2] = (float)planeAxesValues[PlaneAxis.Ailerons];
            axesValuesTmp[3] = (float)planeAxesValues[PlaneAxis.Throttle];

            Buffer.BlockCopy(axesValuesTmp, 0, messageBuffer, currentBufferPosition,
                messagePlaneAxesCount * sizeof(float));
            currentBufferPosition += messagePlaneAxesCount * sizeof(float);

            // trim values
            messageBuffer[currentBufferPosition++] = planeControlSurfacesTrimValues[PlaneControlSurface.Elevator];
            messageBuffer[currentBufferPosition++] = planeControlSurfacesTrimValues[PlaneControlSurface.Rudder];
            messageBuffer[currentBufferPosition++] = planeControlSurfacesTrimValues[PlaneControlSurface.AileronLeft];
            messageBuffer[currentBufferPosition++] = planeControlSurfacesTrimValues[PlaneControlSurface.AileronRight];

            // flap offset
            var flapOffsetTmp = planeFlapOffsetBuffer.Value!;
            flapOffsetTmp[0] = messageParameters.FlapOffset;

            Buffer.BlockCopy(flapOffsetTmp, 0, messageBuffer, currentBufferPosition, messageFlapOffsetSize);
            currentBufferPosition += messageFlapOffsetSize;

            // enable plane feedback
            messageBuffer[currentBufferPosition++] = messageParameters.EnablePlaneFeedback ? (byte)1 : (byte)0;

            // plane control surfaces max angles
            messageBuffer[currentBufferPosition++] = planeControlSurfacesMaxAngles[PlaneControlSurface.AileronLeft];
            messageBuffer[currentBufferPosition++] = planeControlSurfacesMaxAngles[PlaneControlSurface.AileronRight];
            messageBuffer[currentBufferPosition++] = planeControlSurfacesMaxAngles[PlaneControlSurface.Elevator];
            messageBuffer[currentBufferPosition++] = planeControlSurfacesMaxAngles[PlaneControlSurface.Rudder];

            // radio PA level
            messageBuffer[currentBufferPosition++] = messageParameters.RadioPALevel;

            // radio channel
            messageBuffer[currentBufferPosition++] = messageParameters.RadioChannel;

            // signal byte
            messageBuffer[currentBufferPosition++] = AssembleSignalByte(messageParameters);

            if (currentBufferPosition != messageSize - 3)
                throw new Exception("Message assemble check failed");

            // message tail
            messageBuffer[messageSize - 3] = 129;
            messageBuffer[messageSize - 2] = 0;
            messageBuffer[messageSize - 1] = 255;
        }

        private static byte AssembleSignalByte(MessageParameters messageParameters)
        {
            var signalByte = (byte)0;

            if (messageParameters.ResetThrottleMinutes)
            {
                signalByte |= 0b00000001;
            }

            if (messageParameters.SaveTrimValues)
            {
                signalByte |= 0b00000010;
            }

            return signalByte;
        }
    }
}