﻿using BloodflyControl.ControlCommunication.Interfaces;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.ControlCommunication
{
    internal class FeedbackMessageReader(
        ISerialPortReader serialPortReader)
        : IFeedbackMessageReader
    {
        public List<FeedbackMessage> Read()
        {
            var newData = serialPortReader.Read();
            throw new NotImplementedException();
        }
    }
}