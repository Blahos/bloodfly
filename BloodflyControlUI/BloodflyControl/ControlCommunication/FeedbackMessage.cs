﻿namespace BloodflyControl.ControlCommunication
{
    internal readonly struct FeedbackMessage
    {
        public DateTime TimeStamp { get; init; }
        public FeedbackMessageType Type { get; init; }
        public double[] Data { get; init; }
    }
}