﻿namespace BloodflyControl.ControlCommunication
{
    internal enum FeedbackMessageType
    {
        InputMessageReceived,
        ControlArduinoLoopFrequency
    }
}