﻿namespace BloodflyControl.ControlCommunication
{
    internal readonly struct MessageParameters
    {
        public short FlapOffset { get; init; }
        public bool EnablePlaneFeedback { get; init; }
        public byte RadioPALevel { get; init; }
        public byte RadioChannel { get; init; }
        public bool ResetThrottleMinutes { get; init; }
        public bool SaveTrimValues { get; init; }
    }
}