﻿namespace BloodflyControl.ControlCommunication.Interfaces
{
    internal interface IPlaneControlWriter
    {
        void Write();
    }
}