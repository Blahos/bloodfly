﻿namespace BloodflyControl.ControlCommunication.Interfaces
{
    internal interface IMessageParametersProvider
    {
        MessageParameters GetMessageParameters();
    }
}