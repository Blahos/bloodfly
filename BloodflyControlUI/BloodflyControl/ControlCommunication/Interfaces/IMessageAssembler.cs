﻿using BloodflyControl.PlaneControl;

namespace BloodflyControl.ControlCommunication.Interfaces
{
    internal interface IMessageAssembler
    {
        int MessageSize { get; }

        void AssembleMessageToBuffer(
            PlaneAxesValues<double> planeAxesValues,
            PlaneControlSurfacesValues<byte> planeControlSurfacesTrimValues,
            PlaneControlSurfacesValues<byte> planeControlSurfacesMaxAngles,
            MessageParameters messageParameters,
            byte[] messageBuffer);
    }
}