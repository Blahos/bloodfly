﻿using BloodflyControl.PlaneControl;

namespace BloodflyControl.ControlCommunication.Interfaces
{
    internal interface IMessageTrimValuesProvider
    {
        PlaneControlSurfacesValues<byte> GetTrimValues();
    }
}