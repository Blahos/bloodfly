﻿namespace BloodflyControl.ControlCommunication.Interfaces
{
    internal interface IFeedbackMessageReader
    {
        List<FeedbackMessage> Read();
    }
}