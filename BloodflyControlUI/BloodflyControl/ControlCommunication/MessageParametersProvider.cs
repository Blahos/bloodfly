﻿using BloodflyControl.ControlCommunication.Interfaces;
using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.ControlCommunication
{
    internal class MessageParametersProvider(IPlaneControlRadioSettingsProvider planeControlRadioSettingsProvider)
        : IMessageParametersProvider
    {
        public MessageParameters GetMessageParameters()
        {
            return new MessageParameters
            {
                RadioChannel = planeControlRadioSettingsProvider.GetRadioChannel(),
                RadioPALevel = planeControlRadioSettingsProvider.GetRadioPALevel(),
                EnablePlaneFeedback = planeControlRadioSettingsProvider.IsPlaneFeedbackEnabled()
            };
        }
    }
}