﻿using BloodflyControl.ControlCommunication.Interfaces;
using BloodflyControl.PlaneControl;
using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.ControlCommunication
{
    internal class MessageTrimValuesProvider(ITrimRequestBuffer trimRequestBuffer)
        : IMessageTrimValuesProvider
    {
        private const byte cTrimMinusValue = 127;
        private const byte cTrimPlusValue = 255;

        public PlaneControlSurfacesValues<byte> GetTrimValues()
        {
            var trimRequests = trimRequestBuffer.GetAllAndClear();
            var result = new PlaneControlSurfacesValues<byte>();

            foreach (var trimRequest in trimRequests)
            {
                if (trimRequest.Value == null)
                {
                    continue;
                }

                ProcessTrimRequest(trimRequest.Key, trimRequest.Value.Value, result);
            }

            return result;
        }

        private static void ProcessTrimRequest(TrimOption trimOption, TrimDirection trimDirection,
            PlaneControlSurfacesValues<byte> messageTrimValues)
        {
            var trimDirectionValue = TrimDirectionToValue(trimDirection);
            ;
            switch (trimOption)
            {
                case TrimOption.Rudder:
                    messageTrimValues[PlaneControlSurface.Rudder] = trimDirectionValue;
                    break;
                case TrimOption.Elevator:
                    messageTrimValues[PlaneControlSurface.Elevator] = trimDirectionValue;
                    break;
                case TrimOption.AileronLeft:
                    messageTrimValues[PlaneControlSurface.AileronLeft] = trimDirectionValue;
                    break;
                case TrimOption.AileronRight:
                    messageTrimValues[PlaneControlSurface.AileronRight] = trimDirectionValue;
                    break;
                case TrimOption.AileronBoth:
                    messageTrimValues[PlaneControlSurface.AileronRight] = trimDirectionValue;
                    messageTrimValues[PlaneControlSurface.AileronLeft] =
                        TrimDirectionToValue(GetOppositeDirection(trimDirection));
                    break;
                default:
                    break;
            }
        }

        private static byte TrimDirectionToValue(TrimDirection direction)
        {
            return direction == TrimDirection.Plus ? cTrimPlusValue : cTrimMinusValue;
        }

        private static TrimDirection GetOppositeDirection(TrimDirection direction)
        {
            return direction == TrimDirection.Plus ? TrimDirection.Minus : TrimDirection.Plus;
        }
    }
}