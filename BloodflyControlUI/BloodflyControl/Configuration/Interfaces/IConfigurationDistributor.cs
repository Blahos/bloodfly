﻿namespace BloodflyControl.Configuration.Interfaces
{
    internal interface IConfigurationDistributor
    {
        public void ApplyConfiguration(Configuration configuration);
        public Configuration GetConfigurationForCurrentState();
        public event Action? CurrentStateConfigurationChanged;
    }
}