﻿using BloodflyControl.Configuration.Interfaces;
using BloodflyControl.PlaneControl;
using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.Configuration
{
    internal class ConfigurationDistributor : IConfigurationDistributor
    {
        private readonly ISerialPortConnectorSettings serialPortConnectorSettings;
        private readonly IPlaneAxisControlSettingsManager planeAxisControlSettingsManager;
        private readonly IControlActionSettingsManager controlActionSettingsManager;
        private bool suppressEvents = false;

        public ConfigurationDistributor(
            ISerialPortConnectorSettings serialPortConnectorSettings, 
            IPlaneAxisControlSettingsManager planeAxisControlSettingsManager, 
            IControlActionSettingsManager controlActionSettingsManager)
        {
            this.serialPortConnectorSettings = serialPortConnectorSettings;
            this.planeAxisControlSettingsManager = planeAxisControlSettingsManager;
            this.controlActionSettingsManager = controlActionSettingsManager;

            serialPortConnectorSettings.SerialPortParametersChanged += ConfigurationDependencyChanged;
            planeAxisControlSettingsManager.SettingsChanged += ConfigurationDependencyChanged;
            controlActionSettingsManager.SettingsChanged += ConfigurationDependencyChanged;
        }

        public event Action? CurrentStateConfigurationChanged;

        public void ApplyConfiguration(Configuration configuration)
        {
            suppressEvents = true;
            serialPortConnectorSettings.SerialPortParameters = configuration.SerialPortParameters;
            planeAxisControlSettingsManager.SetAxesSettings(configuration.PlaneAxesControlSettings);
            controlActionSettingsManager.SetActionsSettings(configuration.ControlActionSettings,
                InvalidSettingsSetPolicy.Reject);
            suppressEvents = false;
            OnCurrentStateConfigurationChanged();
        }

        public Configuration GetConfigurationForCurrentState()
        {
            return new Configuration
            {
                SerialPortParameters = serialPortConnectorSettings.SerialPortParameters,
                PlaneAxesControlSettings = planeAxisControlSettingsManager.GetAxesSettings(),
                ControlActionSettings = controlActionSettingsManager.GetActionsSettings()
            };
        }

        private void ConfigurationDependencyChanged()
        {
            if (suppressEvents) return;
            OnCurrentStateConfigurationChanged();
        }

        private void OnCurrentStateConfigurationChanged()
        {
            CurrentStateConfigurationChanged?.Invoke();
        }
    }
}