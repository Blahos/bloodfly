﻿using BloodflyControl.PlaneControl;
using BloodflyControl.SerialComm;

namespace BloodflyControl.Configuration
{
    internal class Configuration
    {
        public SerialPortParameters? SerialPortParameters { get; set; }
        public Dictionary<PlaneAxis, PlaneAxisControlSettings> PlaneAxesControlSettings { get; set; } = new();
        public Dictionary<ControlAction, ControlActionSettings> ControlActionSettings { get; set; } = new();
    }
}