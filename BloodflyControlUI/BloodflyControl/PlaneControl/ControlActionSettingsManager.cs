﻿using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class ControlActionSettingsManager(IControlActionSettingsValidator controlActionSettingsValidator)
        : IControlActionSettingsManager
    {
        private readonly ControlActionsValues<ControlActionSettings?> actionsSettings = new();

        public event Action? SettingsChanged;

        public ControlActionSettings? GetActionSettings(ControlAction action)
        {
            lock (actionsSettings)
            {
                return actionsSettings[action];
            }
        }

        public Dictionary<ControlAction, ControlActionSettings> GetActionsSettings()
        {
            var result = new Dictionary<ControlAction, ControlActionSettings>();
            lock (actionsSettings)
            {
                foreach (var (action, settings) in actionsSettings)
                {
                    if (settings != null)
                    {
                        result[action] = settings.Value;
                    }
                }
            }
            return result;
        }

        public void SetActionSettings(ControlAction action, ControlActionSettings? settings,
            InvalidSettingsSetPolicy setPolicy)
        {
            if (SetActionSettingsInternal(action, settings, setPolicy))
            {
                SettingsChanged?.Invoke();
            }
        }

        public void SetActionsSettings(Dictionary<ControlAction, ControlActionSettings> settings, InvalidSettingsSetPolicy setPolicy)
        {
            var anyChange = false;
            foreach (var action in EnumValuesProvider.ControlActions)
            {
                var change = settings.TryGetValue(action, out var setting)
                    ? SetActionSettingsInternal(action, setting, setPolicy)
                    : SetActionSettingsInternal(action, null, setPolicy);

                anyChange = anyChange || change;
            }
            if (anyChange)
            {
                SettingsChanged?.Invoke();
            }
        }

        public bool SetActionSettingsInternal(ControlAction action, ControlActionSettings? settings,
            InvalidSettingsSetPolicy setPolicy)
        {
            bool settingsChanged;
            lock (actionsSettings)
            {
                var validatedSettings =
                    controlActionSettingsValidator.ValidateSettings(action, settings, actionsSettings[action]);

                if (setPolicy == InvalidSettingsSetPolicy.ModifyToComplyAndSet ||
                    Nullable.Equals(validatedSettings, settings))
                {
                    settingsChanged = !Nullable.Equals(actionsSettings[action], validatedSettings);
                    actionsSettings[action] = validatedSettings;
                }
                else throw new ArgumentException($"Invalid settings for action {action}");
            }

            return settingsChanged;
        }
    }
}