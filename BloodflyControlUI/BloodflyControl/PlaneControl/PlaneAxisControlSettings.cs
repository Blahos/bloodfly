﻿using BloodflyControl.ControllerInput;
using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal readonly struct PlaneAxisControlSettings : IEquatable<PlaneAxisControlSettings>
    {
        public ControllerAxis ControllerAxis { get; init; }
        public HashSet<ControllerRole> SupportedControllerRoles { get; init; }
        public bool SupportedControllerMustBeActive { get; init; }
        public double ControllerAxisDeadBand { get; init; }
        public bool Inverted { get; init; }
        public IMapping IntervalMapping { get; init; }
        public double IntervalStart { get; init; }
        public double IntervalEnd { get; init; }
        public IPlaneAxisValueAssigner FinalValueAssigner { get; init; }

        public bool Equals(PlaneAxisControlSettings other)
        {
            return ControllerAxis == other.ControllerAxis &&
                   SupportedControllerRoles.Equals(other.SupportedControllerRoles) &&
                   SupportedControllerMustBeActive == other.SupportedControllerMustBeActive &&
                   ControllerAxisDeadBand.Equals(other.ControllerAxisDeadBand) && Inverted == other.Inverted &&
                   IntervalMapping.Equals(other.IntervalMapping) && IntervalStart.Equals(other.IntervalStart) &&
                   IntervalEnd.Equals(other.IntervalEnd) && FinalValueAssigner.Equals(other.FinalValueAssigner);
        }

        public override bool Equals(object? obj)
        {
            return obj is PlaneAxisControlSettings other && Equals(other);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add((int)ControllerAxis);
            hashCode.Add(SupportedControllerRoles);
            hashCode.Add(SupportedControllerMustBeActive);
            hashCode.Add(ControllerAxisDeadBand);
            hashCode.Add(Inverted);
            hashCode.Add(IntervalMapping);
            hashCode.Add(IntervalStart);
            hashCode.Add(IntervalEnd);
            hashCode.Add(FinalValueAssigner);
            return hashCode.ToHashCode();
        }
    }
}