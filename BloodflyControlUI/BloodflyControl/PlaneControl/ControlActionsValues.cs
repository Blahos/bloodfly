﻿using System.Collections;
using BloodflyControl.ControllerInput;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class ControlActionsValues<T> : IEnumerable<KeyValuePair<ControlAction, T>>
    {
        private static readonly int arraySize = Utils.EnumValuesProvider.ControlActions.Count;
        private readonly T[] array = new T[arraySize];

        public T this[ControlAction axis]
        {
            get => array[(int)axis];
            set => array[(int)axis] = value;
        }

        public int Count { get; } = arraySize;
        public IEnumerator<KeyValuePair<ControlAction, T>> GetEnumerator()
        {
            foreach (var action in EnumValuesProvider.ControlActions)
            {
                yield return new KeyValuePair<ControlAction, T>(action, this[action]);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}