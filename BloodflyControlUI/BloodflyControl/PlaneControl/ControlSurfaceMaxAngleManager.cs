﻿using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class ControlSurfaceMaxAngleManager : IControlSurfaceMaxAngleManager
    {
        private readonly PlaneControlSurfacesValues<byte> values = new();
        public event Action? MaxAnglesChanged;

        public ControlSurfaceMaxAngleManager()
        {
            foreach (var controlSurface in EnumValuesProvider.PlaneControlSurfaces)
            {
                values[controlSurface] = 30;
            }
        }

        public PlaneControlSurfacesValues<byte> GetMaxAngles()
        {
            lock (values)
            {
                return values.Clone();
            }
        }

        public void SetMaxAngle(PlaneControlSurface planeControlSurface, byte maxAngle)
        {
            bool raiseEvent = false;
            lock (values)
            {
                if (values[planeControlSurface] != maxAngle)
                {
                    values[planeControlSurface] = maxAngle;
                    raiseEvent = true;
                }
            }

            if (raiseEvent)
            {
                MaxAnglesChanged?.Invoke();
            }
        }
    }
}