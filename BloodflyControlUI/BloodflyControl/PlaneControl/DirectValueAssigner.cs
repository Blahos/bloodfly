﻿using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal class DirectValueAssigner : IPlaneAxisValueAssigner
    {
        public double GetValue(double inputValue, double referenceAxisValue, TimeSpan timeElapsedSinceReference)
        {
            return inputValue;
        }
    }
}