﻿using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class PiecewiseLinearMapping(List<Tuple<double, double>> map) : IMapping
    {
        public IReadOnlyList<Tuple<double, double>> Map => map;

        public double GetMappedValue(double input)
        {
            if (map.Count == 0) throw new Exception("Map is empty");
            if (input <= map[0].Item1) return map[0].Item2;
            var lastIndex = map.Count - 1;
            if (input >= map[lastIndex].Item1) return map[lastIndex].Item2;

            var leftIndex = 0;
            while (leftIndex < lastIndex && map[leftIndex + 1].Item1 < input)
            {
                leftIndex++;
            }

            return MathUtils.Interpolate(
                map[leftIndex].Item1, map[leftIndex].Item2,
                map[leftIndex + 1].Item1, map[leftIndex + 1].Item2,
                input);
        }
    }
}