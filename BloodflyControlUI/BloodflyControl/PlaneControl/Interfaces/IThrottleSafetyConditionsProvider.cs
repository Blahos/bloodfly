﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IThrottleSafetyConditionsProvider
    {
        ThrottleUnlockAvailability GetUnlockAvailability();

        event Action<ThrottleSafetyDependency>? ThrottleSafetyDependencyChanged;
    }
}