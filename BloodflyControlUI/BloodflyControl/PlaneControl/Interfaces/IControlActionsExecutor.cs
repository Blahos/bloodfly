﻿using BloodflyControl.ControllerInput;

namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IControlActionsExecutor
    {
        void ExecuteActions(ControllerInputStateEx controllerInput, ControllerRole sourceController);
    }
}