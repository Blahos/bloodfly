﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IControlActionSettingsValidator
    {
        ControlActionSettings? ValidateSettings(
            ControlAction action,
            ControlActionSettings? desiredSettings,
            ControlActionSettings? currentSettings);
    }
}