﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IPlaneAxisControlSettingsManager : IPlaneAxisControlSettingsProvider
    {
        void SetAxisSettings(PlaneAxis axis, PlaneAxisControlSettings? settings);
        void SetAxesSettings(Dictionary<PlaneAxis, PlaneAxisControlSettings> settings);
    }
}