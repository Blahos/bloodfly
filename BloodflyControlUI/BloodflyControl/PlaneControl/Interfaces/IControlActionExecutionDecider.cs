﻿using BloodflyControl.ControllerInput;

namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IControlActionExecutionDecider
    {
        bool ShouldActionExecute(ControlAction action, ControllerInputStateEx controllerInput,
            ControllerRole sourceController);
    }
}