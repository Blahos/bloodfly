﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IControlActionSettingsProvider
    {
        event Action? SettingsChanged;
        ControlActionSettings? GetActionSettings(ControlAction action);
        Dictionary<ControlAction, ControlActionSettings> GetActionsSettings();
    }
}