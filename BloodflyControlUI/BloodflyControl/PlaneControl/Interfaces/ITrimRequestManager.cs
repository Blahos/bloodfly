﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface ITrimRequestManager : ITrimRequestBuffer
    {
        void AddRequest(TrimOption trimOption, TrimDirection direction);
    }
}