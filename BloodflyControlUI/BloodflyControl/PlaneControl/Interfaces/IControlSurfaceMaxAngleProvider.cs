﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IControlSurfaceMaxAngleProvider
    {
        PlaneControlSurfacesValues<byte> GetMaxAngles();
        event Action? MaxAnglesChanged;
    }
}