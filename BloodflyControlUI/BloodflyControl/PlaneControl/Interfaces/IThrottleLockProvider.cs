﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IThrottleLockProvider
    {
        event Action? ThrottleLockedStateChanged;
        bool IsThrottleLocked { get; }
    }
}