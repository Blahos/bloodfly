﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IThrottleSafetySettings
    {
        bool IsThrottleValueSafe(double throttle);
        double GetSafeThrottleValue();
    }
}