﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IControlActionsProvider
    {
        Action GetControlAction(ControlAction action);
    }
}