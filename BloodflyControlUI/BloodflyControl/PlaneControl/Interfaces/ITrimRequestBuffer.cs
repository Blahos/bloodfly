﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface ITrimRequestBuffer
    {
        TrimOptionsValues<TrimDirection?> GetAllAndClear();
    }
}