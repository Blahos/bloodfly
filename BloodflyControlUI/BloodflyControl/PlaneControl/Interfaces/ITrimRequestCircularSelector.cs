﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface ITrimRequestCircularSelector
    {
        TrimOption ActiveTrimOption { get; }
        event Action? ActiveTrimOptionChanged;
        void EnqueRequest(TrimDirection direction);

        void CycleActiveTrimNext();
        void CycleActiveTrimPrevious();
    }
}