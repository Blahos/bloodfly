﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IPlaneAxesLimitsProvider
    {
        double GetAxisMinimum(PlaneAxis axis);
        double GetAxisMaximum(PlaneAxis axis);
    }
}