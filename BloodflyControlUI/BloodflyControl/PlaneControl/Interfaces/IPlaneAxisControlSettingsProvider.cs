﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IPlaneAxisControlSettingsProvider
    {
        PlaneAxisControlSettings? GetAxisSettings(PlaneAxis axis);
        Dictionary<PlaneAxis, PlaneAxisControlSettings> GetAxesSettings();
        event Action? SettingsChanged;
    }
}