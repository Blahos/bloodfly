﻿using BloodflyControl.ControllerInput;

namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IControllerToPlaneAxesTranslator
    {
        PlaneAxesValues<double?> TranslateToPlaneAxes(
            ControllerAxesValues<double> controllerAxesValues,
            PlaneAxesValues<double> referenceAxesValues,
            TimeSpan timeElapsedSinceReferenceValues,
            ControllerRole controllerRole);
    }
}