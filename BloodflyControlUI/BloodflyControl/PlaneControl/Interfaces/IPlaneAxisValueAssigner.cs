﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IPlaneAxisValueAssigner
    {
        double GetValue(double inputValue, double referenceAxisValue, TimeSpan timeElapsedSinceReference);
    }
}