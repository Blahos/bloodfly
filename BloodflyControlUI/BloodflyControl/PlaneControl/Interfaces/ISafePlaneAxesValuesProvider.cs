﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface ISafePlaneAxesValuesProvider
    {
        PlaneAxesValues<double> GetValues();
        event Action? ValuesChanged;
    }
}