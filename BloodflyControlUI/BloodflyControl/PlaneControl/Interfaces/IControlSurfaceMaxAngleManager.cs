﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IControlSurfaceMaxAngleManager : IControlSurfaceMaxAngleProvider
    {
        void SetMaxAngle(PlaneControlSurface planeControlSurface, byte maxAngle);
    }
}