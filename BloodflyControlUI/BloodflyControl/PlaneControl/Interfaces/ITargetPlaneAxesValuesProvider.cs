﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface ITargetPlaneAxesValuesProvider
    {
        PlaneAxesValues<double> GetValues();
        double GetValue(PlaneAxis axis);
        event Action? ValuesChanged;
    }
}