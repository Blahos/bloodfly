﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IPlaneControlRadioSettingsProvider
    {
        byte GetRadioChannel();
        byte GetRadioPALevel();
        bool IsPlaneFeedbackEnabled();

        event Action? RadioChannelChanged;
        event Action? RadioPALevelChanged;
        event Action? IsPlaneFeedbackEnaledChanged;
    }
}