﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IThrottleLockManager : IThrottleLockProvider
    {
        void LockThrottle();
        void RequestThrottleUnlock();
    }
}