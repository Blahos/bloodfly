﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IMapping
    {
        double GetMappedValue(double input);
    }
}