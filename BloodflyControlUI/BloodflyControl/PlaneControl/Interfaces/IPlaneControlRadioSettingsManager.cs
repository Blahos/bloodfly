﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IPlaneControlRadioSettingsManager : IPlaneControlRadioSettingsProvider
    {
        void SetRadioChannel(byte channel);
        void SetRadioPALevel(byte paLevel);
        void SetIsPlaneFeedbackEnabled(bool isPlaneFeedbackEnabled);
    }
}