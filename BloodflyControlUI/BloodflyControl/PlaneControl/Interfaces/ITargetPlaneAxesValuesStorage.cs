﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface ITargetPlaneAxesValuesStorage : ITargetPlaneAxesValuesProvider
    {
        void SetValue(PlaneAxis axis, double value);
        void SetValues(PlaneAxesValues<double?> values);
    }
}