﻿namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IControlActionSettingsManager : IControlActionSettingsProvider
    {
        void SetActionSettings(ControlAction action, ControlActionSettings? settings,
            InvalidSettingsSetPolicy setPolicy);

        void SetActionsSettings(Dictionary<ControlAction, ControlActionSettings> settings,
            InvalidSettingsSetPolicy setPolicy);
    }
}