﻿using BloodflyControl.ControllerInput;

namespace BloodflyControl.PlaneControl.Interfaces
{
    internal interface IPlaneAxisControlSettingsEditor : IPlaneAxisControlSettingsProvider
    {
        void SetAxisSettingsAbsolute(PlaneAxis axis, ControllerAxis controllerAxis, double deadBand, bool inverted,
            IMapping? mapping);

        void SetAxisSettingsDifferential(PlaneAxis axis, ControllerAxis controllerAxis, double deadBand, bool inverted,
            double gain, IMapping? mapping);

        void ClearAxisSettings(PlaneAxis axis);
    }
}