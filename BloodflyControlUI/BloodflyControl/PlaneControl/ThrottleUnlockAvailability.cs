﻿namespace BloodflyControl.PlaneControl
{
    internal enum ThrottleUnlockAvailability
    {
        CannotBeUnlocked,
        CanRemainUnlocked, // throttle can remain in unlocked state if it already is in that state, but cannot get to that state from locked state
        CanBecomeUnlocked // throttle can both be in unlocked state and also is able to transition from locked to unlocked
    }
}