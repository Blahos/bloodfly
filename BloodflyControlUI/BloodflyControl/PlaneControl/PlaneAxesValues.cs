﻿using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Utils;
using System.Collections;

namespace BloodflyControl.PlaneControl
{
    internal class PlaneAxesValues<T> : IEnumerable<KeyValuePair<PlaneAxis, T>>, ICloneable<PlaneAxesValues<T>>
    {
        private static readonly int arraySize = EnumValuesProvider.PlaneAxes.Count;
        private readonly T[] array = new T[arraySize];

        public T this[PlaneAxis axis]
        {
            get => array[(int)axis];
            set => array[(int)axis] = value;
        }

        public int Count { get; } = arraySize;

        public PlaneAxesValues<T> Clone()
        {
            var clone = new PlaneAxesValues<T>();
            foreach (var axis in EnumValuesProvider.PlaneAxes)
            {
                clone[axis] = this[axis];
            }

            return clone;
        }

        public IEnumerator<KeyValuePair<PlaneAxis, T>> GetEnumerator()
        {
            foreach (var axis in EnumValuesProvider.PlaneAxes)
            {
                yield return new KeyValuePair<PlaneAxis, T>(axis, this[axis]);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}