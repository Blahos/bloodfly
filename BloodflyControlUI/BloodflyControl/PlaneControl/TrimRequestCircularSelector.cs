﻿using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class TrimRequestCircularSelector(ITrimRequestManager trimRequestManager)
        : ITrimRequestCircularSelector
    {
        private TrimOption activeTrimOption = 0;
        public TrimOption ActiveTrimOption => activeTrimOption;

        public event Action? ActiveTrimOptionChanged;

        public void CycleActiveTrimNext()
        {
            activeTrimOption = (TrimOption)(((uint)activeTrimOption + 1) % EnumValuesProvider.TrimOptions.Count);
            OnActiveTrimOptionChanged();
        }

        public void CycleActiveTrimPrevious()
        {
            activeTrimOption = (TrimOption)(((uint)activeTrimOption - 1) % EnumValuesProvider.TrimOptions.Count);
            OnActiveTrimOptionChanged();
        }

        private void OnActiveTrimOptionChanged()
        {
            ActiveTrimOptionChanged?.Invoke();
        }

        public void EnqueRequest(TrimDirection direction)
        {
            trimRequestManager.AddRequest(activeTrimOption, direction);
        }
    }
}