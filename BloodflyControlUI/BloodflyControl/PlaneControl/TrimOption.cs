﻿namespace BloodflyControl.PlaneControl
{
    internal enum TrimOption : uint
    {
        Rudder,
        Elevator,
        AileronLeft,
        AileronRight,
        AileronBoth
    }
}