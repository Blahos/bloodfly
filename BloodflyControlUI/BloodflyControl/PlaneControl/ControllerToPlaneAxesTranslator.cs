﻿using BloodflyControl.ControllerInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class ControllerToPlaneAxesTranslator(
        IPlaneAxisControlSettingsProvider planeAxisControlSettingsProvider,
        IActiveControllerSetProvider activeControllerSetProvider) : IControllerToPlaneAxesTranslator
    {
        public PlaneAxesValues<double?> TranslateToPlaneAxes(
            ControllerAxesValues<double> controllerAxesValues,
            PlaneAxesValues<double> referenceAxesValues,
            TimeSpan timeElapsedSinceReferenceValues,
            ControllerRole controllerRole)
        {
            var result = new PlaneAxesValues<double?>();

            foreach (var axis in EnumValuesProvider.PlaneAxes)
            {
                var settings = planeAxisControlSettingsProvider.GetAxisSettings(axis);
                if (settings == null || !IsRelevantController(controllerRole, settings.Value))
                {
                    continue;
                }

                var settingsValue = settings.Value;
                result[axis] = Translate(controllerAxesValues[settingsValue.ControllerAxis], referenceAxesValues[axis],
                    timeElapsedSinceReferenceValues, settingsValue);
            }

            return result;
        }

        private static double Translate(double controllerAxisValue, double referencePlaneAxisValue,
            TimeSpan timeElapsedSinceReferenceValue, PlaneAxisControlSettings settings)
        {
            // controller axis -> plane axis transform pipeline:
            // controller axis value
            // apply dead band
            // invert if enabled
            // apply mapping <-1, 1> -> <-1, 1>
            // scale interval <-1, 1> to <intervalStart, intervalEnd>
            // apply final value assigner
            // plane axis value

            var value = controllerAxisValue;

            if (Math.Abs(value) < settings.ControllerAxisDeadBand)
                value = 0.0;

            if (settings.Inverted)
                value = -value;

            value = settings.IntervalMapping.GetMappedValue(value);

            value = settings.IntervalStart + (value + 1) / 2 * (settings.IntervalEnd - settings.IntervalStart);

            value = settings.FinalValueAssigner.GetValue(value, referencePlaneAxisValue,
                timeElapsedSinceReferenceValue);

            return value;
        }

        private bool IsRelevantController(ControllerRole controllerRole, PlaneAxisControlSettings settings)
        {
            return settings.SupportedControllerRoles.Contains(controllerRole) &&
                   (!settings.SupportedControllerMustBeActive ||
                    activeControllerSetProvider.IsControllerActive(controllerRole));
        }
    }
}