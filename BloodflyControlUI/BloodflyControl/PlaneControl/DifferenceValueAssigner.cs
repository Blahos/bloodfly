﻿using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal class DifferenceValueAssigner : IPlaneAxisValueAssigner
    {
        public double Gain { get; init; }
        public double ClipMin { get; init; }
        public double ClipMax { get; init; }

        public double GetValue(double inputValue, double referenceAxisValue, TimeSpan timeElapsedSinceReference)
        {
            var newValue = referenceAxisValue + inputValue * Gain * timeElapsedSinceReference.TotalSeconds;
            return Math.Clamp(newValue, ClipMin, ClipMax);
        }
    }
}