﻿namespace BloodflyControl.PlaneControl
{
    internal enum ThrottleSafetyDependency
    {
        ControllerState,
        PlaneControlSerialPortState,
        ControlActionSettingsState
    }
}