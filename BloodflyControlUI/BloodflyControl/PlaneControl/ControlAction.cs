﻿namespace BloodflyControl.PlaneControl
{
    internal enum ControlAction
    {
        SetThrottleToZero,
        SetThrottleToFull,
        SwitchThrottleLock,
        SetPrimaryControllerSetAsActive,
        SetAlternativeControllerSetAsActive,
        CycleActiveTrimNext,
        CycleActiveTrimPrevious,
        EnqueueActiveTrimPlus,
        EnqueueActiveTrimMinus
    }
}