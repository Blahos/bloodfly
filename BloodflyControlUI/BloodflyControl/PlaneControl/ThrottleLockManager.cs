﻿using BloodflyControl.Framework.Interfaces;
using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal class ThrottleLockManager : IThrottleLockManager
    {
        private static readonly TimeSpan throttleUnlockDelay = TimeSpan.FromSeconds(3);
        private readonly IThrottleSafetyConditionsProvider throttleSafetyConditionsProvider;
        private readonly ITaskRunner taskRunner;

        private readonly HashSet<ThrottleSafetyDependency> dependenciesLockingThrottleOnChange
            = [ThrottleSafetyDependency.PlaneControlSerialPortState];

        private readonly object throttleLockStateLock = new();
        private readonly ThrottleLockStorage throttleLockStorage = new();
        public bool IsThrottleLocked => throttleLockStorage.IsThrottleLocked;

        public event Action? ThrottleLockedStateChanged;

        public ThrottleLockManager(
            IThrottleSafetyConditionsProvider throttleSafetyConditionsProvider,
            ITaskRunner taskRunner)
        {
            this.throttleSafetyConditionsProvider = throttleSafetyConditionsProvider;
            this.taskRunner = taskRunner;

            throttleSafetyConditionsProvider.ThrottleSafetyDependencyChanged +=
                ThrottleSafetyConditionsProvider_ThrottleSafetyDependencyChanged;
        }

        public void LockThrottle()
        {
            TrySetNewThrottleLockState(ThrottleLockState.Locked);
        }

        public void RequestThrottleUnlock()
        {
            if (!TrySetNewThrottleLockState(ThrottleLockState.UnlockPending))
                return;

            taskRunner.RunTaskWithInitialDelay(() => { TrySetNewThrottleLockState(ThrottleLockState.Unlocked); },
                throttleUnlockDelay);
        }

        private bool TrySetNewThrottleLockState(ThrottleLockState desiredState)
        {
            var raiseEvent = false;
            var desiredStateSet = true;
            lock (throttleLockStateLock)
            {
                var oldState = throttleLockStorage.IsThrottleLocked;
                if (IsThrottleUnlockAttemptInProgress(desiredState)
                    &&
                    throttleSafetyConditionsProvider.GetUnlockAvailability() !=
                    ThrottleUnlockAvailability.CanBecomeUnlocked)
                {
                    desiredStateSet = desiredState == ThrottleLockState.Locked;
                    desiredState = ThrottleLockState.Locked;
                }

                var newStateSet = throttleLockStorage.SetNewState(desiredState);
                desiredStateSet = desiredStateSet && newStateSet;

                raiseEvent = throttleLockStorage.IsThrottleLocked != oldState;
            }

            if (raiseEvent)
                ThrottleLockedStateChanged?.Invoke();

            return desiredStateSet;
        }

        private static bool IsThrottleUnlockAttemptInProgress(ThrottleLockState desiredState)
        {
            return desiredState == ThrottleLockState.UnlockPending || desiredState == ThrottleLockState.Unlocked;
        }

        private void ThrottleSafetyConditionsProvider_ThrottleSafetyDependencyChanged(
            ThrottleSafetyDependency throttleSafetyDependency)
        {
            if (throttleSafetyConditionsProvider.GetUnlockAvailability() == ThrottleUnlockAvailability.CannotBeUnlocked
                ||
                dependenciesLockingThrottleOnChange.Contains(throttleSafetyDependency))
            {
                LockThrottle();
            }
        }
    }
}