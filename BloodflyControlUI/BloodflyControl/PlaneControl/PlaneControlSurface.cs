﻿namespace BloodflyControl.PlaneControl
{
    internal enum PlaneControlSurface
    {
        AileronLeft,
        AileronRight,
        Elevator,
        Rudder
    }
}