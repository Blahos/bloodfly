﻿using BloodflyControl.ControllerInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal class ControlActionExecutionDecider(
        IActiveControllerSetProvider activeControllerSetProvider,
        IControlActionSettingsProvider controlActionSettingsProvider)
        : IControlActionExecutionDecider
    {
        public bool ShouldActionExecute(ControlAction action, ControllerInputStateEx controllerInput,
            ControllerRole sourceController)
        {
            var actionSettings = controlActionSettingsProvider.GetActionSettings(action);
            if (actionSettings == null) return false;

            return
                IsActionTriggered(controllerInput, actionSettings.Value)
                &&
                IsRelevantController(sourceController, actionSettings.Value);
        }

        private static bool IsActionTriggered(
            ControllerInputStateEx controllerInput,
            ControlActionSettings settings)
        {
            return (controllerInput.ButtonsNewlyPressed & settings.TriggeringButtons) == settings.TriggeringButtons;
        }

        private bool IsRelevantController(ControllerRole controllerRole, ControlActionSettings settings)
        {
            return settings.SupportedControllerRoles.Contains(controllerRole) &&
                   (!settings.SupportedControllerMustBeActive ||
                    activeControllerSetProvider.IsControllerActive(controllerRole));
        }
    }
}