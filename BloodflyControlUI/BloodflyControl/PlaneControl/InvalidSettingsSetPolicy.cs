﻿namespace BloodflyControl.PlaneControl
{
    internal enum InvalidSettingsSetPolicy
    {
        Reject,
        ModifyToComplyAndSet
    }
}