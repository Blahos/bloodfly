﻿using BloodflyControl.ControllerInput;

namespace BloodflyControl.PlaneControl
{
    internal readonly struct ControlActionSettings : IEquatable<ControlActionSettings>
    {
        public ControllerButton TriggeringButtons { get; init; }
        public HashSet<ControllerRole> SupportedControllerRoles { get; init; }
        public bool SupportedControllerMustBeActive { get; init; }

        public bool Equals(ControlActionSettings other)
        {
            return TriggeringButtons == other.TriggeringButtons &&
                   SupportedControllerRoles.Equals(other.SupportedControllerRoles) &&
                   SupportedControllerMustBeActive == other.SupportedControllerMustBeActive;
        }

        public override bool Equals(object? obj)
        {
            return obj is ControlActionSettings other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TriggeringButtons, SupportedControllerRoles, SupportedControllerMustBeActive);
        }
    }
}