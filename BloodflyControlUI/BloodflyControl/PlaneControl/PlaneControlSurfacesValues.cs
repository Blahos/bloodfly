﻿using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class PlaneControlSurfacesValues<T> : ICloneable<PlaneControlSurfacesValues<T>>
    {
        private static readonly int arraySize = EnumValuesProvider.PlaneControlSurfaces.Count;
        private readonly T[] array = new T[arraySize];

        public T this[PlaneControlSurface surface]
        {
            get => array[(int)surface];
            set => array[(int)surface] = value;
        }

        public int Count { get; } = arraySize;

        public PlaneControlSurfacesValues<T> Clone()
        {
            var clone = new PlaneControlSurfacesValues<T>();
            foreach (var surface in EnumValuesProvider.PlaneControlSurfaces)
            {
                clone[surface] = this[surface];
            }

            return clone;
        }
    }
}