﻿namespace BloodflyControl.PlaneControl
{
    internal enum ThrottleLockState
    {
        Locked,
        UnlockPending,
        Unlocked
    }
}