﻿using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class SafePlaneAxesValuesProvider : ISafePlaneAxesValuesProvider
    {
        private readonly ITargetPlaneAxesValuesProvider targetPlaneAxesValuesProvider;
        private readonly IThrottleLockProvider throttleLockProvider;
        private readonly IThrottleSafetySettings throttleSafetySettings;

        public SafePlaneAxesValuesProvider(
            ITargetPlaneAxesValuesProvider targetPlaneAxesValuesProvider,
            IThrottleLockProvider throttleLockProvider,
            IThrottleSafetySettings throttleSafetySettings)
        {
            this.targetPlaneAxesValuesProvider = targetPlaneAxesValuesProvider;
            this.throttleLockProvider = throttleLockProvider;
            this.throttleSafetySettings = throttleSafetySettings;

            targetPlaneAxesValuesProvider.ValuesChanged += TargetPlaneAxesValuesProvider_ValuesChanged;
            throttleLockProvider.ThrottleLockedStateChanged += ThrottleLockProvider_ThrottleLockedStateChanged;
        }

        public event Action? ValuesChanged;

        public PlaneAxesValues<double> GetValues()
        {
            var targetValues = targetPlaneAxesValuesProvider.GetValues();
            var safeValues = new PlaneAxesValues<double>();

            foreach (var axis in EnumValuesProvider.PlaneAxes)
            {
                safeValues[axis] = ApplySafetyToAxisValue(axis, targetValues[axis]);
            }

            return safeValues;
        }

        private double ApplySafetyToAxisValue(PlaneAxis axis, double value)
        {
            if (axis == PlaneAxis.Throttle && throttleLockProvider.IsThrottleLocked)
            {
                value = throttleSafetySettings.GetSafeThrottleValue();
            }

            return value;
        }

        private void TargetPlaneAxesValuesProvider_ValuesChanged()
        {
            OnValuesChanged();
        }

        private void ThrottleLockProvider_ThrottleLockedStateChanged()
        {
            OnValuesChanged();
        }

        private void OnValuesChanged()
        {
            ValuesChanged?.Invoke();
        }
    }
}