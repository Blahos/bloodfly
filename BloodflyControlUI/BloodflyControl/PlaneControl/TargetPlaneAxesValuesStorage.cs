﻿using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class TargetPlaneAxesValuesStorage(IPlaneAxesLimitsProvider planeAxesLimitsProvider)
        : ITargetPlaneAxesValuesStorage
    {
        private readonly PlaneAxesValues<double> values = new();

        private readonly object valuesLock = new();
        public event Action? ValuesChanged;

        public void SetValue(PlaneAxis axis, double value)
        {
            if (SetValueInner(value, axis))
            {
                OnValuesChanged();
            }
        }

        public void SetValues(PlaneAxesValues<double?> values)
        {
            var anyValuesChanged = false;
            lock (valuesLock)
            {
                foreach (var axis in EnumValuesProvider.PlaneAxes)
                {
                    var axisValue = values[axis];
                    if (!axisValue.HasValue) continue;
                    var valueChanged = SetValueInner(axisValue.Value, axis);
                    anyValuesChanged = anyValuesChanged || valueChanged;
                }
            }

            if (anyValuesChanged)
                OnValuesChanged();
        }

        public PlaneAxesValues<double> GetValues()
        {
            lock (valuesLock)
            {
                return values.Clone();
            }
        }

        public double GetValue(PlaneAxis axis)
        {
            lock (valuesLock)
            {
                return values[axis];
            }
        }

        private bool SetValueInner(double value, PlaneAxis axis)
        {
            var newValue = ApplyLimitsToAxisValue(value, axis);
            var valueChanged = false;
            lock (valuesLock)
            {
                valueChanged = newValue != values[axis];
                values[axis] = value;
            }

            return valueChanged;
        }

        private double ApplyLimitsToAxisValue(double value, PlaneAxis axis)
        {
            value = Math.Clamp(value,
                planeAxesLimitsProvider.GetAxisMinimum(axis),
                planeAxesLimitsProvider.GetAxisMaximum(axis));

            return value;
        }

        private void OnValuesChanged()
        {
            ValuesChanged?.Invoke();
        }
    }
}