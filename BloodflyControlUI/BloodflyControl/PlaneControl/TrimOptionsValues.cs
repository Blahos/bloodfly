﻿using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Utils;
using System.Collections;

namespace BloodflyControl.PlaneControl
{
    internal class TrimOptionsValues<T> : IEnumerable<KeyValuePair<TrimOption, T>>, ICloneable<TrimOptionsValues<T>>
    {
        private static readonly int arraySize = EnumValuesProvider.TrimOptions.Count;
        private readonly T[] array = new T[arraySize];

        public T this[TrimOption trimOption]
        {
            get { return array[(int)trimOption]; }
            set { array[(int)trimOption] = value; }
        }

        public int Count { get; } = arraySize;

        public TrimOptionsValues<T> Clone()
        {
            var clone = new TrimOptionsValues<T>();
            foreach (var trimOption in EnumValuesProvider.TrimOptions)
            {
                clone[trimOption] = this[trimOption];
            }

            return clone;
        }

        public IEnumerator<KeyValuePair<TrimOption, T>> GetEnumerator()
        {
            foreach (var trimOption in EnumValuesProvider.TrimOptions)
            {
                yield return new KeyValuePair<TrimOption, T>(trimOption, this[trimOption]);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}