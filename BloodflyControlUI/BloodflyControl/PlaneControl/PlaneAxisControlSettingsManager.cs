﻿using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class PlaneAxisControlSettingsManager : IPlaneAxisControlSettingsManager
    {
        private readonly PlaneAxesValues<PlaneAxisControlSettings?> axesSettings = new();

        public event Action? SettingsChanged;

        public PlaneAxisControlSettings? GetAxisSettings(PlaneAxis axis)
        {
            lock (axesSettings)
            {
                return axesSettings[axis];
            }
        }

        public Dictionary<PlaneAxis, PlaneAxisControlSettings> GetAxesSettings()
        {
            var result = new Dictionary<PlaneAxis, PlaneAxisControlSettings>();
            lock (axesSettings)
            {
                foreach (var (axis, settings) in axesSettings)
                {
                    if (settings != null)
                    {
                        result[axis] = settings.Value;
                    }
                }
            }
            return result;
        }

        public void SetAxisSettings(PlaneAxis axis, PlaneAxisControlSettings? settings)
        {
            if (SetAxisSettingsInternal(axis, settings))
            {
                SettingsChanged?.Invoke();
            }
        }

        public void SetAxesSettings(Dictionary<PlaneAxis, PlaneAxisControlSettings> settings)
        {
            var anyChange = false;
            lock (axesSettings)
            {
                foreach (var axis in EnumValuesProvider.PlaneAxes)
                {
                    var change = settings.TryGetValue(axis, out var setting)
                        ? SetAxisSettingsInternal(axis, setting)
                        : SetAxisSettingsInternal(axis, null);

                    anyChange = anyChange || change;
                }
            }

            if (anyChange)
            {
                SettingsChanged?.Invoke();
            }
        }

        private bool SetAxisSettingsInternal(PlaneAxis axis, PlaneAxisControlSettings? settings)
        {
            bool settingsChanged;
            lock (axesSettings)
            {
                settingsChanged = !Nullable.Equals(axesSettings[axis], settings);
                axesSettings[axis] = settings;
            }

            return settingsChanged;
        }
    }
}