﻿using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.ControllerInput;

namespace BloodflyControl.PlaneControl
{
    internal class ControlActionSettingsValidator : IControlActionSettingsValidator
    {
        private readonly Dictionary<ControlAction, bool> supportedControllerMustBeActiveFixed = new()
        {
            { ControlAction.SwitchThrottleLock, true },
            { ControlAction.CycleActiveTrimNext, true },
            { ControlAction.SetThrottleToZero, true },
            { ControlAction.SetThrottleToFull, true },
            { ControlAction.SetPrimaryControllerSetAsActive, false },
            { ControlAction.SetAlternativeControllerSetAsActive, false },
            { ControlAction.CycleActiveTrimPrevious, true },
            { ControlAction.EnqueueActiveTrimPlus, true },
            { ControlAction.EnqueueActiveTrimMinus, true }
        };

        private readonly Dictionary<ControlAction, HashSet<ControllerRole>> supportedControllerRolesFixed = new()
        {
            { ControlAction.SwitchThrottleLock, [ControllerRole.PrimaryFlying, ControllerRole.AlternativeFlying] },
            { ControlAction.CycleActiveTrimNext, [ControllerRole.PrimaryFlying] },
            { ControlAction.SetThrottleToZero, [ControllerRole.PrimaryFlying, ControllerRole.AlternativeFlying] },
            { ControlAction.SetThrottleToFull, [ControllerRole.PrimaryFlying, ControllerRole.AlternativeFlying] },
            { ControlAction.SetPrimaryControllerSetAsActive, [ControllerRole.PrimaryFlying] },
            { ControlAction.SetAlternativeControllerSetAsActive, [ControllerRole.PrimaryFlying] },
            { ControlAction.CycleActiveTrimPrevious, [ControllerRole.PrimaryFlying] },
            { ControlAction.EnqueueActiveTrimPlus, [ControllerRole.PrimaryFlying] },
            { ControlAction.EnqueueActiveTrimMinus, [ControllerRole.PrimaryFlying] }
        };

        private readonly Dictionary<ControlAction, List<ControllerButton>> triggeringButtonsAllowed = new()
        {
            {
                ControlAction.SwitchThrottleLock,
                [
                    ControllerButton.XInput_Start | ControllerButton.XInput_B,
                    ControllerButton.DInput_Two | ControllerButton.DInput_Eight
                ]
            }
        };

        public ControlActionSettings? ValidateSettings(
            ControlAction action,
            ControlActionSettings? desiredSettings,
            ControlActionSettings? currentSettings)
        {
            if (desiredSettings == null)
            {
                return desiredSettings;
            }

            var triggeringButtons = desiredSettings.Value.TriggeringButtons;
            if (triggeringButtonsAllowed.TryGetValue(action, out var allowedTriggeringButtons)
                &&
                !allowedTriggeringButtons.Contains(triggeringButtons))
            {
                if (currentSettings.HasValue &&
                    allowedTriggeringButtons.Contains(currentSettings.Value.TriggeringButtons))
                {
                    triggeringButtons = currentSettings.Value.TriggeringButtons;
                }
                else
                {
                    triggeringButtons = allowedTriggeringButtons.First();
                }
            }

            return new ControlActionSettings
            {
                SupportedControllerMustBeActive =
                    supportedControllerMustBeActiveFixed.GetValueOrDefault(action,
                        desiredSettings.Value.SupportedControllerMustBeActive),
                SupportedControllerRoles =
                    supportedControllerRolesFixed.GetValueOrDefault(action,
                        desiredSettings.Value.SupportedControllerRoles),
                TriggeringButtons = triggeringButtons
            };
        }
    }
}