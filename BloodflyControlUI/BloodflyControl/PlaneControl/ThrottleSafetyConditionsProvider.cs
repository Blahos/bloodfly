﻿using BloodflyControl.ControllerInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.SerialComm.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal class ThrottleSafetyConditionsProvider : IThrottleSafetyConditionsProvider
    {
        private readonly IPlaneControllerProvider planeControllerProvider;
        private readonly ISerialPortStateProvider planeControlSerialPortStateProvider;
        private readonly IThrottleSafetySettings throttleSafetySettings;
        private readonly ITargetPlaneAxesValuesProvider targetPlaneAxesValuesProvider;
        private readonly IControlActionSettingsProvider controlActionSettingsProvider;

        public ThrottleUnlockAvailability GetUnlockAvailability()
        {
            if (!EvaluateUnlockedThrottleAllowed())
            {
                return ThrottleUnlockAvailability.CannotBeUnlocked;
            }

            return IsTargetThrottleInSafeState()
                ? ThrottleUnlockAvailability.CanBecomeUnlocked
                : ThrottleUnlockAvailability.CanRemainUnlocked;
        }

        public event Action<ThrottleSafetyDependency>? ThrottleSafetyDependencyChanged;

        public ThrottleSafetyConditionsProvider(
            IPlaneControllerProvider planeControllerProvider,
            ISerialPortStateProvider planeControlSerialPortStateProvider,
            IThrottleSafetySettings throttleSafetySettings,
            ITargetPlaneAxesValuesProvider targetPlaneAxesValuesProvider,
            IControlActionSettingsProvider controlActionSettingsProvider)
        {
            this.planeControllerProvider = planeControllerProvider;
            this.planeControlSerialPortStateProvider = planeControlSerialPortStateProvider;
            this.throttleSafetySettings = throttleSafetySettings;
            this.targetPlaneAxesValuesProvider = targetPlaneAxesValuesProvider;
            this.controlActionSettingsProvider = controlActionSettingsProvider;

            planeControllerProvider.ControllersChanged += PlaneControllerProvider_ControllersChanged;
            planeControlSerialPortStateProvider.PortStateChanged +=
                PlaneControlSerialPortStateProvider_PortStateChanged;
            this.controlActionSettingsProvider.SettingsChanged += ControlActionSettingsProvider_SettingsChanged;
        }

        private bool EvaluateUnlockedThrottleAllowed()
        {
            return IsAnyFlyingControllerConnected() &&
                   IsPlaneControlSerialPortConnected() &&
                   IsThrottleLockSwitchActionSettingSet();
        }

        private bool IsAnyFlyingControllerConnected()
        {
            var connectedControllers = planeControllerProvider.GetControllersConnectionStatus();
            return connectedControllers.Any(a => a.Key.IsFlyingController() && a.Value);
        }

        private bool IsPlaneControlSerialPortConnected()
        {
            return planeControlSerialPortStateProvider.PortOpen;
        }

        private bool IsThrottleLockSwitchActionSettingSet()
        {
            return controlActionSettingsProvider.GetActionSettings(ControlAction.SwitchThrottleLock) != null;
        }

        private void PlaneControllerProvider_ControllersChanged()
        {
            OnThrottleSafetyConditionChanged(ThrottleSafetyDependency.ControllerState);
        }

        private void PlaneControlSerialPortStateProvider_PortStateChanged()
        {
            OnThrottleSafetyConditionChanged(ThrottleSafetyDependency.PlaneControlSerialPortState);
        }

        private void ControlActionSettingsProvider_SettingsChanged()
        {
            OnThrottleSafetyConditionChanged(ThrottleSafetyDependency.ControlActionSettingsState);
        }

        private void OnThrottleSafetyConditionChanged(ThrottleSafetyDependency throttleSafetyDependency)
        {
            ThrottleSafetyDependencyChanged?.Invoke(throttleSafetyDependency);
        }

        private bool IsTargetThrottleInSafeState()
        {
            return throttleSafetySettings.IsThrottleValueSafe(
                targetPlaneAxesValuesProvider.GetValue(PlaneAxis.Throttle));
        }
    }
}