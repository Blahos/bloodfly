﻿using BloodflyControl.ControllerInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal class ControlActionsProvider(
        ITargetPlaneAxesValuesStorage planeAxesValuesStorage,
        IThrottleLockManager throttleLockManager,
        IActiveControllerSetManager activeControllerSetManager,
        ITrimRequestCircularSelector trimRequestManagerStateful)
        : IControlActionsProvider
    {
        public Action GetControlAction(ControlAction action)
        {
            return action switch
            {
                ControlAction.SetThrottleToZero => () => planeAxesValuesStorage.SetValue(PlaneAxis.Throttle, 0.0),
                ControlAction.SetThrottleToFull => () => planeAxesValuesStorage.SetValue(PlaneAxis.Throttle, 1.0),
                ControlAction.SwitchThrottleLock => SwitchThrottleLockAction,
                ControlAction.SetPrimaryControllerSetAsActive => () =>
                    activeControllerSetManager.DesiredActiveControllerSet = ControllerSet.Primary,
                ControlAction.SetAlternativeControllerSetAsActive => () =>
                    activeControllerSetManager.DesiredActiveControllerSet = ControllerSet.Alternative,
                ControlAction.CycleActiveTrimNext => trimRequestManagerStateful.CycleActiveTrimNext,
                ControlAction.CycleActiveTrimPrevious => trimRequestManagerStateful.CycleActiveTrimPrevious,
                ControlAction.EnqueueActiveTrimPlus => () =>
                    trimRequestManagerStateful.EnqueRequest(TrimDirection.Plus),
                ControlAction.EnqueueActiveTrimMinus => () =>
                    trimRequestManagerStateful.EnqueRequest(TrimDirection.Minus),
                _ => throw new ArgumentException($"Control action not available: {action}"),
            };
        }

        private void SwitchThrottleLockAction()
        {
            if (throttleLockManager.IsThrottleLocked)
            {
                throttleLockManager.RequestThrottleUnlock();
            }
            else
            {
                throttleLockManager.LockThrottle();
            }
        }
    }
}