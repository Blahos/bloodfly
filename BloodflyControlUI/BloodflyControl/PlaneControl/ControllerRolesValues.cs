﻿using BloodflyControl.ControllerInput;
using BloodflyControl.Utils;
using System.Collections;

namespace BloodflyControl.PlaneControl
{
    internal class ControllerRolesValues<T> : IEnumerable<KeyValuePair<ControllerRole, T>>
    {
        private static readonly int arraySize = EnumValuesProvider.ControllerRoles.Count;
        private readonly T[] array = new T[arraySize];

        public T this[ControllerRole role]
        {
            get => array[(int)role];
            set => array[(int)role] = value;
        }

        public int Count { get; } = arraySize;

        public IEnumerator<KeyValuePair<ControllerRole, T>> GetEnumerator()
        {
            foreach (var role in EnumValuesProvider.ControllerRoles)
            {
                yield return new KeyValuePair<ControllerRole, T>(role, this[role]);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}