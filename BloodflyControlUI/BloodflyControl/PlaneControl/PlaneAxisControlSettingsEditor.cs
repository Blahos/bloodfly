﻿using BloodflyControl.ControllerInput;
using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal class PlaneAxisControlSettingsEditor : IPlaneAxisControlSettingsEditor
    {
        private readonly IPlaneAxesLimitsProvider planeAxesLimitsProvider;
        private readonly IPlaneAxisControlSettingsManager planeAxisControlSettingsManager;

        public PlaneAxisControlSettingsEditor(
            IPlaneAxisControlSettingsManager planeAxisControlSettingsManager,
            IPlaneAxesLimitsProvider planeAxesLimitsProvider)
        {
            this.planeAxisControlSettingsManager = planeAxisControlSettingsManager;
            this.planeAxesLimitsProvider = planeAxesLimitsProvider;
            planeAxisControlSettingsManager.SettingsChanged += () => SettingsChanged?.Invoke();
        }

        public Dictionary<PlaneAxis, PlaneAxisControlSettings> GetAxesSettings()
        {
            return planeAxisControlSettingsManager.GetAxesSettings();
        }

        public event Action? SettingsChanged;

        public PlaneAxisControlSettings? GetAxisSettings(PlaneAxis axis)
        {
            return planeAxisControlSettingsManager.GetAxisSettings(axis);
        }

        public void SetAxisSettingsAbsolute(PlaneAxis axis, ControllerAxis controllerAxis, double deadBand,
            bool inverted, IMapping? mapping)
        {
            var settings = CreateSettings(axis, controllerAxis, deadBand, inverted, new DirectValueAssigner(), mapping);
            planeAxisControlSettingsManager.SetAxisSettings(axis, settings);
        }

        public void SetAxisSettingsDifferential(PlaneAxis axis, ControllerAxis controllerAxis, double deadBand,
            bool inverted, double gain, IMapping? mapping)
        {
            var settings = CreateSettings(axis, controllerAxis, deadBand, inverted, new DifferenceValueAssigner
            {
                Gain = gain,
                ClipMin = planeAxesLimitsProvider.GetAxisMinimum(axis),
                ClipMax = planeAxesLimitsProvider.GetAxisMaximum(axis)
            }, mapping);
            planeAxisControlSettingsManager.SetAxisSettings(axis, settings);
        }

        public void ClearAxisSettings(PlaneAxis axis)
        {
            planeAxisControlSettingsManager.SetAxisSettings(axis, null);
        }

        private PlaneAxisControlSettings CreateSettings(PlaneAxis axis, ControllerAxis controllerAxis, double deadBand,
            bool inverted, IPlaneAxisValueAssigner finalValueAssigner, IMapping? mapping)
        {
            return new PlaneAxisControlSettings
            {
                ControllerAxis = controllerAxis,
                ControllerAxisDeadBand = deadBand,
                Inverted = inverted,
                FinalValueAssigner = finalValueAssigner,
                SupportedControllerMustBeActive = true,
                SupportedControllerRoles = GetFlyingControllersAsSupported(),
                IntervalStart = planeAxesLimitsProvider.GetAxisMinimum(axis),
                IntervalEnd = planeAxesLimitsProvider.GetAxisMaximum(axis),
                IntervalMapping = mapping ?? new IdentityMapping()
            };
        }

        private static HashSet<ControllerRole> GetFlyingControllersAsSupported()
        {
            return
            [
                ControllerRole.PrimaryFlying,
                ControllerRole.AlternativeFlying
            ];
        }
    }
}