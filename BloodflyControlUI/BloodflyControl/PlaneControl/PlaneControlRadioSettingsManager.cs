﻿using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal class PlaneControlRadioSettingsManager : IPlaneControlRadioSettingsManager
    {
        private byte radioChannel = 0;
        private byte radioPALevel = 0;
        private bool isPlaneFeedbackEnabled = false;

        public event Action? RadioChannelChanged;
        public event Action? RadioPALevelChanged;
        public event Action? IsPlaneFeedbackEnaledChanged;

        public byte GetRadioChannel()
        {
            return radioChannel;
        }

        public byte GetRadioPALevel()
        {
            return radioPALevel;
        }

        public bool IsPlaneFeedbackEnabled()
        {
            return isPlaneFeedbackEnabled;
        }

        public void SetIsPlaneFeedbackEnabled(bool isPlaneFeedbackEnabled)
        {
            this.isPlaneFeedbackEnabled = isPlaneFeedbackEnabled;
            IsPlaneFeedbackEnaledChanged?.Invoke();
        }

        public void SetRadioChannel(byte channel)
        {
            radioChannel = channel;
            RadioChannelChanged?.Invoke();
        }

        public void SetRadioPALevel(byte paLevel)
        {
            radioPALevel = paLevel;
            RadioPALevelChanged?.Invoke();
        }
    }
}