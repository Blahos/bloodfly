﻿namespace BloodflyControl.PlaneControl
{
    internal enum PlaneAxis
    {
        Throttle,
        Ailerons,
        Rudder,
        Elevator
    }
}