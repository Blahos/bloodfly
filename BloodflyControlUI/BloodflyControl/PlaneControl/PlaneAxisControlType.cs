﻿namespace BloodflyControl.PlaneControl
{
    internal enum PlaneAxisControlType
    {
        Absolute,
        Differential
    }
}