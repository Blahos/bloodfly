﻿using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class TrimRequestManager : ITrimRequestManager
    {
        private readonly TrimOptionsValues<TrimDirection?> trimRequests = new();

        public TrimOptionsValues<TrimDirection?> GetAllAndClear()
        {
            lock (trimRequests)
            {
                var result = trimRequests.Clone();
                foreach (var trimOption in EnumValuesProvider.TrimOptions)
                {
                    trimRequests[trimOption] = null;
                }

                return result;
            }
        }

        public void AddRequest(TrimOption trimOption, TrimDirection direction)
        {
            lock (trimRequests)
            {
                trimRequests[trimOption] = direction;
            }
        }
    }
}