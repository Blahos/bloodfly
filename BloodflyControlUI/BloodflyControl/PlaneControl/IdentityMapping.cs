﻿using BloodflyControl.PlaneControl.Interfaces;

namespace BloodflyControl.PlaneControl
{
    internal class IdentityMapping : IMapping
    {
        public double GetMappedValue(double input)
        {
            return input;
        }
    }
}