﻿namespace BloodflyControl.PlaneControl
{
    internal class ThrottleLockStorage
    {
        private readonly object stateLock = new object();
        private ThrottleLockState state = ThrottleLockState.Locked;
        public bool IsThrottleLocked => ThrottleLockStateToThrottleLocked(state);

        public bool SetNewState(ThrottleLockState newState)
        {
            lock (stateLock)
            {
                if (!IsStateTransitionValid(state, newState))
                {
                    return false;
                }

                state = newState;
                return true;
            }
        }

        private static bool IsStateTransitionValid(ThrottleLockState fromState, ThrottleLockState toState)
        {
            if (toState == ThrottleLockState.Locked) return true;
            if (toState == ThrottleLockState.UnlockPending) return fromState == ThrottleLockState.Locked;
            if (toState == ThrottleLockState.Unlocked) return fromState == ThrottleLockState.UnlockPending;
            return false;
        }

        private static bool ThrottleLockStateToThrottleLocked(ThrottleLockState state)
        {
            return state == ThrottleLockState.Locked || state == ThrottleLockState.UnlockPending;
        }
    }
}