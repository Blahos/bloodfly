﻿using BloodflyControl.ControllerInput;
using BloodflyControl.Logging;
using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;

namespace BloodflyControl.PlaneControl
{
    internal class ControlActionsExecutor(
        IControlActionExecutionDecider controlActionExecutionDecider,
        IControlActionsProvider controlActionProvider,
        ILogger logger) : IControlActionsExecutor
    {
        public void ExecuteActions(ControllerInputStateEx controllerInput, ControllerRole sourceController)
        {
            foreach (var action in EnumValuesProvider.ControlActions)
            {
                ExecuteActionIfRequired(action, controllerInput, sourceController);
            }
        }

        private void ExecuteActionIfRequired(ControlAction action, ControllerInputStateEx controllerInput,
            ControllerRole sourceController)
        {
            if (controlActionExecutionDecider
                .ShouldActionExecute(action, controllerInput, sourceController))
            {
                try
                {
                    controlActionProvider.GetControlAction(action).Invoke();
                }
                catch (Exception ex)
                {
                    logger.LogMessage($"Failed to execute control action \"{action}\": {ex.Message}", LogLevel.Error);
                }
            }
        }
    }
}