﻿using BloodflyControl.PlaneControl;
using BloodflyControl.Utils;
using FluentAssertions;
using FluentAssertions.Execution;

namespace BloodflyControl.Tests
{
    internal class TrimRequestManagerTests
    {
        private TrimRequestManager trimRequestManager;

        [SetUp]
        public void SetUp()
        {
            trimRequestManager = new TrimRequestManager();
        }

        [Test]
        public void AfterCreationThereAreNoTrimRequests()
        {
            var result = trimRequestManager.GetAllAndClear();
            result.Should().OnlyContain(a => a.Value == null);
        }

        [Test, Combinatorial]
        public void AddRequestAddsTrimRequestSingle([Values] TrimOption trimOption, [Values] TrimDirection direction)
        {
            trimRequestManager.AddRequest(trimOption, direction);

            var trimRequests = trimRequestManager.GetAllAndClear();
            trimRequests.Should().Contain(trimOption, direction);
        }

        [TestCaseSource(nameof(TrimRequestSets))]
        public void CorrectlyAddsMultipleRequests(List<(TrimOption TrimOption, TrimDirection Direction)> trimRequests)
        {
            foreach (var trimRequest in trimRequests)
            {
                trimRequestManager.AddRequest(trimRequest.TrimOption, trimRequest.Direction);
            }

            var assembledTrimRequests = trimRequestManager.GetAllAndClear();

            using (new AssertionScope())
            {
                foreach (var trimOption in EnumValuesProvider.TrimOptions)
                {
                    var lastAddedTrimRequest = (TrimDirection?)null;
                    if (trimRequests.Any(a => a.TrimOption == trimOption))
                    {
                        lastAddedTrimRequest = trimRequests.Last(a => a.TrimOption == trimOption).Direction;
                    }

                    assembledTrimRequests[trimOption].Should().Be(lastAddedTrimRequest);
                }
            }
        }

        [TestCaseSource(nameof(TrimRequestSets))]
        public void GettingTrimRequestsClearsThem(List<(TrimOption TrimOption, TrimDirection Direction)> trimRequests)
        {
            foreach (var trimRequest in trimRequests)
            {
                trimRequestManager.AddRequest(trimRequest.TrimOption, trimRequest.Direction);
            }

            var assembledTrimRequests = trimRequestManager.GetAllAndClear();
            var assembledTrimRequests2 = trimRequestManager.GetAllAndClear();
            assembledTrimRequests2.Should().OnlyContain(a => a.Value == null);
        }

        public static object[] TrimRequestSets =
        [
            new object[]
            {
                new List<(TrimOption, TrimDirection)>
                {
                    (TrimOption.AileronRight, TrimDirection.Minus),
                    (TrimOption.Rudder, TrimDirection.Plus),
                }
            },
            new object[]
            {
                new List<(TrimOption, TrimDirection)>
                {
                    (TrimOption.AileronLeft, TrimDirection.Plus),
                    (TrimOption.Rudder, TrimDirection.Minus),
                    (TrimOption.Elevator, TrimDirection.Minus),
                }
            },
            new object[]
            {
                new List<(TrimOption, TrimDirection)>
                {
                    (TrimOption.Elevator, TrimDirection.Plus),
                    (TrimOption.Rudder, TrimDirection.Minus),
                    (TrimOption.Elevator, TrimDirection.Minus),
                    (TrimOption.Elevator, TrimDirection.Minus),
                    (TrimOption.AileronLeft, TrimDirection.Minus),
                    (TrimOption.AileronRight, TrimDirection.Minus),
                    (TrimOption.AileronRight, TrimDirection.Plus),
                }
            },
            new object[]
            {
                new List<(TrimOption, TrimDirection)>
                {
                    (TrimOption.AileronLeft, TrimDirection.Minus),
                    (TrimOption.AileronRight, TrimDirection.Minus),
                    (TrimOption.Rudder, TrimDirection.Plus),
                    (TrimOption.Elevator, TrimDirection.Plus),
                    (TrimOption.AileronBoth, TrimDirection.Minus),
                }
            },
        ];
    }
}