﻿using BloodflyControl.Framework.Interfaces;
using BloodflyControl.PlaneControl;
using BloodflyControl.PlaneControl.Interfaces;
using FluentAssertions;
using NSubstitute;

namespace BloodflyControl.Tests
{
    public class ThrottleLockManagerTest
    {
        private readonly IThrottleSafetyConditionsProvider throttleSafetyConditionsProvider =
            Substitute.For<IThrottleSafetyConditionsProvider>();

        private readonly ITaskRunner taskRunner = Substitute.For<ITaskRunner>();
        private readonly List<Action> taskRunnerCallbacks = [];
        private ThrottleLockManager throttleLockManager;
        private int throttleLockChangedEventRaisedCount = 0;

        [SetUp]
        public void Setup()
        {
            throttleLockManager = new ThrottleLockManager(throttleSafetyConditionsProvider, taskRunner);

            throttleLockManager.ThrottleLockedStateChanged += () => throttleLockChangedEventRaisedCount++;

            throttleSafetyConditionsProvider.GetUnlockAvailability()
                .ReturnsForAnyArgs(ThrottleUnlockAvailability.CanBecomeUnlocked);

            taskRunner.RunTask(Arg.Do<Action>(taskRunnerCallbacks.Add));
            taskRunner.RunTaskWithInitialDelay(Arg.Do<Action>(taskRunnerCallbacks.Add), Arg.Any<TimeSpan>());
        }

        [Test]
        public void InitialStateIsLocked()
        {
            throttleLockManager.IsThrottleLocked.Should().Be(true);
        }

        [Test]
        public void LockingLockedThrottleKeepsThrottleLocked()
        {
            throttleLockManager.LockThrottle();
            throttleLockManager.IsThrottleLocked.Should().Be(true);
            throttleLockChangedEventRaisedCount.Should().Be(0);
        }

        [Test]
        public void UnlockRequestStartsTaskToUnlockWhenConditionsSafe()
        {
            throttleLockManager.RequestThrottleUnlock();
            taskRunner.Received(1).RunTaskWithInitialDelay(Arg.Any<Action>(), TimeSpan.FromSeconds(3));
            taskRunnerCallbacks.Count.Should().Be(1);
            throttleLockChangedEventRaisedCount.Should().Be(0);
        }

        [Test]
        public void UnlockRequestTaskUnlocksThrottleWhenConditionsSafe()
        {
            throttleLockManager.RequestThrottleUnlock();
            taskRunnerCallbacks.Count.Should().Be(1);
            taskRunnerCallbacks.Last().Invoke();
            throttleLockManager.IsThrottleLocked.Should().Be(false);
            throttleLockChangedEventRaisedCount.Should().Be(1);
        }

        [Test]
        public void LockRequestLocksUnlockedThrottle()
        {
            throttleLockManager.RequestThrottleUnlock();
            taskRunnerCallbacks.Count.Should().Be(1);
            taskRunnerCallbacks.Last().Invoke();
            throttleLockManager.LockThrottle();
            throttleLockManager.IsThrottleLocked.Should().Be(true);
            throttleLockChangedEventRaisedCount.Should().Be(2);
        }

        [Test]
        public void UnlockingUnlockedThrottleDoesNotDoAnything()
        {
            throttleLockManager.RequestThrottleUnlock();
            taskRunnerCallbacks.Count.Should().Be(1);
            taskRunnerCallbacks.Last().Invoke();
            throttleLockManager.RequestThrottleUnlock();

            taskRunnerCallbacks.Count.Should().Be(1);
            throttleLockManager.IsThrottleLocked.Should().Be(false);
            throttleLockChangedEventRaisedCount.Should().Be(1);
        }

        [Test]
        public void UnlockingAgainDuringUnlockingDoesNotDoAnything()
        {
            throttleLockManager.RequestThrottleUnlock();
            taskRunnerCallbacks.Count.Should().Be(1);
            throttleLockManager.RequestThrottleUnlock();
            taskRunnerCallbacks.Count.Should().Be(1);
            taskRunner.Received(1).RunTaskWithInitialDelay(Arg.Any<Action>(), TimeSpan.FromSeconds(3));
        }

        [Test]
        public void LockingThrottleDuringUnlockingLocksItAndPreventsTaskFromUnlocking()
        {
            throttleLockManager.RequestThrottleUnlock();
            throttleLockManager.LockThrottle();
            taskRunnerCallbacks.Count.Should().Be(1);
            taskRunnerCallbacks.Last().Invoke();

            taskRunnerCallbacks.Count.Should().Be(1);
            throttleLockManager.IsThrottleLocked.Should().Be(true);
            throttleLockChangedEventRaisedCount.Should().Be(0);
        }

        [TestCase(ThrottleUnlockAvailability.CannotBeUnlocked)]
        [TestCase(ThrottleUnlockAvailability.CanRemainUnlocked)]
        public void CannotBeUnlockedWhenConditionsNotSatisfied(object ńewLockAvailability)
        {
            throttleSafetyConditionsProvider.GetUnlockAvailability().ReturnsForAnyArgs(ńewLockAvailability);
            throttleLockManager.RequestThrottleUnlock();

            taskRunnerCallbacks.Count.Should().Be(0);
            throttleLockManager.IsThrottleLocked.Should().Be(true);
            throttleLockChangedEventRaisedCount.Should().Be(0);
        }

        [TestCase(ThrottleUnlockAvailability.CannotBeUnlocked)]
        [TestCase(ThrottleUnlockAvailability.CanRemainUnlocked)]
        public void UnlockInProgressIsPreventedByFailingConditions(object ńewLockAvailability)
        {
            throttleLockManager.RequestThrottleUnlock();
            taskRunnerCallbacks.Count.Should().Be(1);

            throttleSafetyConditionsProvider.GetUnlockAvailability().ReturnsForAnyArgs(ńewLockAvailability);

            taskRunnerCallbacks.Last().Invoke();
            throttleLockManager.IsThrottleLocked.Should().Be(true);
            throttleLockChangedEventRaisedCount.Should().Be(0);
        }

        [Test]
        public void SerialPortChangeLocksThrottleEvenIfCanRemainUnlocked()
        {
            throttleLockManager.RequestThrottleUnlock();
            taskRunnerCallbacks.Last().Invoke();
            throttleLockManager.IsThrottleLocked.Should().Be(false);

            throttleSafetyConditionsProvider.ThrottleSafetyDependencyChanged +=
                Raise.Event<Action<ThrottleSafetyDependency>>(ThrottleSafetyDependency.PlaneControlSerialPortState);
            throttleLockManager.IsThrottleLocked.Should().Be(true);
            throttleLockChangedEventRaisedCount.Should().Be(2);
        }

        [TestCase(ThrottleUnlockAvailability.CanBecomeUnlocked)]
        [TestCase(ThrottleUnlockAvailability.CanRemainUnlocked)]
        public void ControllerChangeDoesNotLockThrottleIfCanRemainUnlocked(object newLockAvailability)
        {
            throttleLockManager.RequestThrottleUnlock();
            taskRunnerCallbacks.Last().Invoke();
            throttleLockManager.IsThrottleLocked.Should().Be(false);

            throttleSafetyConditionsProvider.GetUnlockAvailability().ReturnsForAnyArgs(newLockAvailability);

            throttleSafetyConditionsProvider.ThrottleSafetyDependencyChanged +=
                Raise.Event<Action<ThrottleSafetyDependency>>(ThrottleSafetyDependency.ControllerState);
            throttleLockManager.IsThrottleLocked.Should().Be(false);
            throttleLockChangedEventRaisedCount.Should().Be(1);
        }

        [TestCase(ThrottleSafetyDependency.ControllerState)]
        [TestCase(ThrottleSafetyDependency.PlaneControlSerialPortState)]
        public void AnyConditionChangeLocksThrottleIfCannotRemainUnlocked(object condition)
        {
            throttleLockManager.RequestThrottleUnlock();
            taskRunnerCallbacks.Last().Invoke();
            throttleLockManager.IsThrottleLocked.Should().Be(false);

            throttleSafetyConditionsProvider.GetUnlockAvailability()
                .ReturnsForAnyArgs(ThrottleUnlockAvailability.CannotBeUnlocked);

            throttleSafetyConditionsProvider.ThrottleSafetyDependencyChanged +=
                Raise.Event<Action<ThrottleSafetyDependency>>(condition);
            throttleLockManager.IsThrottleLocked.Should().Be(true);
            throttleLockChangedEventRaisedCount.Should().Be(2);
        }
    }
}