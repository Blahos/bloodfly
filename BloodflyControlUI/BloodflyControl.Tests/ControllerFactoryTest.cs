﻿using BloodflyControl.ControllerInput;
using BloodflyControl.ControllerInput.DInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.ControllerInput.XInput;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;
using FluentAssertions;
using NSubstitute;

namespace BloodflyControl.Tests
{
    public class ControllerFactoryTest
    {
        private readonly IDInputJoystickAxesParser dInputJoystickAxesParser =
            Substitute.For<IDInputJoystickAxesParser>();

        private readonly IDInputJoystickButtonParser dInputJoystickButtonParser =
            Substitute.For<IDInputJoystickButtonParser>();

        private readonly IDInputJoystickWrapperFactory dInputJoystickWrapperFactory =
            Substitute.For<IDInputJoystickWrapperFactory>();

        private readonly ILogger logger = Substitute.For<ILogger>();
        private readonly ISerialTimerFactory serialTimerFactory = Substitute.For<ISerialTimerFactory>();
        private readonly IXInputButtonConverter xInputButtonConverter = Substitute.For<IXInputButtonConverter>();

        private ControllerFactory controllerFactory;

        [SetUp]
        public void Setup()
        {
            controllerFactory = new ControllerFactory(
                serialTimerFactory,
                dInputJoystickWrapperFactory,
                dInputJoystickButtonParser,
                dInputJoystickAxesParser,
                xInputButtonConverter,
                logger);
        }

        [Test]
        public void CreatesXInputController()
        {
            var controllerId = new XInputControllerId(0);
            var controller = controllerFactory.CreateController(controllerId);
            controller.Should().BeOfType(typeof(XInputController));
        }

        [Test]
        public void CreatesDInputController()
        {
            var controllerId = new DInputControllerId("abc", Guid.NewGuid());
            var controller = controllerFactory.CreateController(controllerId);
            controller.Should().BeOfType(typeof(DInputController));
        }

        [Test]
        public void ThrowsOnUnknownControllerId()
        {
            var controllerId = new DummyIControllerIdImpl();
            controllerFactory.Invoking(a => a.CreateController(controllerId)).Should().Throw<ArgumentException>();
        }

        private class DummyIControllerIdImpl : IControllerId
        {
            public string Description => "dummy";

            public bool Equals(IControllerId? other)
            {
                return true;
            }
        }
    }
}