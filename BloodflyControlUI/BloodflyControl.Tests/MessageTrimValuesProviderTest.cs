﻿using BloodflyControl.ControlCommunication;
using BloodflyControl.PlaneControl;
using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.Utils;
using FluentAssertions;
using FluentAssertions.Execution;
using NSubstitute;

namespace BloodflyControl.Tests
{
    internal class MessageTrimValuesProviderTest
    {
        private readonly ITrimRequestBuffer trimRequestBuffer = Substitute.For<ITrimRequestBuffer>();

        private MessageTrimValuesProvider messageTrimValuesProvider;

        private readonly TrimOptionsValues<TrimDirection?> emptyTrimRequests = new();

        [SetUp]
        public void Setup()
        {
            messageTrimValuesProvider = new MessageTrimValuesProvider(trimRequestBuffer);
        }

        [Test]
        public void EmptyTrimRequestCreatesEmptyMessage()
        {
            trimRequestBuffer.GetAllAndClear().Returns(emptyTrimRequests);
            var message = messageTrimValuesProvider.GetTrimValues();
            trimRequestBuffer.Received(1).GetAllAndClear();

            using (new AssertionScope())
            {
                message[PlaneControlSurface.Elevator].Should().Be(0);
                message[PlaneControlSurface.Rudder].Should().Be(0);
                message[PlaneControlSurface.AileronLeft].Should().Be(0);
                message[PlaneControlSurface.AileronRight].Should().Be(0);
            }
        }

        [TestCaseSource(nameof(TrimMessageCases))]
        public void AssemblesCorrectMessage(
            TrimOptionsValues<TrimDirection?> trimRequest,
            Dictionary<PlaneControlSurface, byte> expectedNonzeroMessageValues)
        {
            trimRequestBuffer.GetAllAndClear().Returns(trimRequest);
            var message = messageTrimValuesProvider.GetTrimValues();
            trimRequestBuffer.Received(1).GetAllAndClear();

            using (new AssertionScope())
            {
                foreach (var surface in EnumValuesProvider.PlaneControlSurfaces)
                {
                    if (expectedNonzeroMessageValues.TryGetValue(surface, out byte value))
                    {
                        message[surface].Should().Be(value);
                    }
                    else
                    {
                        message[surface].Should().Be(0);
                    }
                }
            }
        }

        public static object[] TrimMessageCases =
        [
            new object[]
            {
                new TrimOptionsValues<TrimDirection?>
                {
                    [TrimOption.Rudder] = TrimDirection.Plus
                },
                new Dictionary<PlaneControlSurface, byte>
                {
                    { PlaneControlSurface.Rudder, 255 }
                }
            },
            new object[]
            {
                new TrimOptionsValues<TrimDirection?>
                {
                    [TrimOption.Elevator] = TrimDirection.Plus,
                    [TrimOption.AileronLeft] = TrimDirection.Minus
                },
                new Dictionary<PlaneControlSurface, byte>
                {
                    { PlaneControlSurface.Elevator, 255 },
                    { PlaneControlSurface.AileronLeft, 127 }
                }
            },
            new object[]
            {
                new TrimOptionsValues<TrimDirection?>
                {
                    [TrimOption.Elevator] = TrimDirection.Minus,
                    [TrimOption.AileronLeft] = TrimDirection.Plus,
                    [TrimOption.AileronRight] = TrimDirection.Plus,
                    [TrimOption.Rudder] = TrimDirection.Minus
                },
                new Dictionary<PlaneControlSurface, byte>
                {
                    { PlaneControlSurface.Elevator, 127 },
                    { PlaneControlSurface.AileronLeft, 255 },
                    { PlaneControlSurface.AileronRight, 255 },
                    { PlaneControlSurface.Rudder, 127 }
                }
            },
            new object[]
            {
                new TrimOptionsValues<TrimDirection?>
                {
                    [TrimOption.AileronBoth] = TrimDirection.Plus
                },
                new Dictionary<PlaneControlSurface, byte>
                {
                    { PlaneControlSurface.AileronLeft, 127 },
                    { PlaneControlSurface.AileronRight, 255 }
                }
            },
            new object[]
            {
                new TrimOptionsValues<TrimDirection?>
                {
                    [TrimOption.AileronBoth] = TrimDirection.Minus,
                    [TrimOption.Rudder] = TrimDirection.Plus
                },
                new Dictionary<PlaneControlSurface, byte>
                {
                    { PlaneControlSurface.AileronLeft, 255 },
                    { PlaneControlSurface.AileronRight, 127 },
                    { PlaneControlSurface.Rudder, 255 }
                }
            }
        ];
    }
}