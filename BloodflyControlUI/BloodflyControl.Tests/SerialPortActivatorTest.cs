﻿using BloodflyControl.Framework.Interfaces;
using FluentAssertions;
using BloodflyControl.Logging;
using BloodflyControl.SerialComm;
using BloodflyControl.SerialComm.Interfaces;
using NSubstitute;

namespace BloodflyControl.Tests
{
    public class SerialPortActivatorTest : IDisposable
    {
        private readonly ISerialTimerFactory serialTimerFactory = Substitute.For<ISerialTimerFactory>();
        private readonly ILogger logger = Substitute.For<ILogger>();

        private readonly ISerialTimer serialTimer = Substitute.For<ISerialTimer>();
        private readonly ISerialPort serialPort = Substitute.For<ISerialPort>();
        private readonly ISerialPort serialPort2 = Substitute.For<ISerialPort>();

        private SerialPortActivator serialPortActivator;
        private Action? timerCallback;

        [SetUp]
        public void Setup()
        {
            serialTimerFactory.CreateSerialTimer(Arg.Any<Action>(), Arg.Any<TimeSpan>())
                .ReturnsForAnyArgs(serialTimer);

            serialTimerFactory.CreateSerialTimer(Arg.Do<Action>(a => timerCallback = a), Arg.Any<TimeSpan>());

            serialPortActivator = new SerialPortActivator(
                serialTimerFactory,
                logger);
        }

        [Test]
        public void CorrectSerialTimerIsConstructedInConstructor()
        {
            serialTimerFactory.Received(1).CreateSerialTimer(Arg.Any<Action>(), TimeSpan.FromSeconds(0.5));
            serialTimerFactory.Received(1).CreateSerialTimer(Arg.Any<Action>(), Arg.Any<TimeSpan>());
        }

        [Test]
        public void DisposeTerminatesTimer()
        {
            serialPortActivator.Dispose();
            serialTimer.Received(1).Terminate(true);
        }

        [Test]
        public void AssignedSerialTimerCallbackIsNotNull()
        {
            timerCallback.Should().NotBeNull();
        }

        [Test]
        public void NoEventIsRaisedWhenNoPortIsAssigned()
        {
            var eventCount = 0;
            serialPortActivator.PortOpenedStateChanged += () => eventCount++;
            timerCallback?.Invoke();
            eventCount.Should().Be(0);
        }

        [Test]
        public void ActivateTriesToOpenPortWhenItsClosed()
        {
            serialPortActivator.SetSerialPort(serialPort);
            serialPort.IsOpen.Returns(false);
            timerCallback?.Invoke();
            serialPort.Received(1).Open();
        }

        [Test]
        public void ActivateDoesNotTryToOpenPortWhenItsOpen()
        {
            serialPortActivator.SetSerialPort(serialPort);
            serialPort.IsOpen.Returns(true);
            timerCallback?.Invoke();
            serialPort.Received(0).Open();
        }

        [Test]
        public void FirstTimerCallbackInvokesPortOpenChangedEventWhenPortIsNotOpen()
        {
            serialPortActivator.SetSerialPort(serialPort);
            serialPort.IsOpen.Returns(false);
            var eventCount = 0;
            serialPortActivator.PortOpenedStateChanged += () => eventCount++;
            timerCallback?.Invoke();
            eventCount.Should().Be(1);
        }

        [Test]
        public void FirstTimerCallbackInvokesPortOpenChangedEventWhenPortIsOpen()
        {
            serialPortActivator.SetSerialPort(serialPort);
            serialPort.IsOpen.Returns(true);
            var eventCount = 0;
            serialPortActivator.PortOpenedStateChanged += () => eventCount++;
            timerCallback?.Invoke();
            eventCount.Should().Be(1);
        }

        [Test]
        public void SecondTimerCallbackDoesNotInvokePortOpenChangedEventWhenPortInSameState([Values] bool portOpen)
        {
            serialPortActivator.SetSerialPort(serialPort);
            serialPort.IsOpen.Returns(portOpen);
            var eventCount = 0;
            serialPortActivator.PortOpenedStateChanged += () => eventCount++;
            timerCallback?.Invoke();
            eventCount.Should().Be(1);
            timerCallback?.Invoke();
            eventCount.Should().Be(1);
        }

        [Test]
        public void SecondTimerCallbackInvokesPortOpenChangedEventWhenPortStateChanges([Values] bool portOpenInitial)
        {
            serialPortActivator.SetSerialPort(serialPort);
            serialPort.IsOpen.Returns(portOpenInitial);
            var eventCount = 0;
            serialPortActivator.PortOpenedStateChanged += () => eventCount++;
            timerCallback?.Invoke();
            eventCount.Should().Be(1);
            serialPort.IsOpen.Returns(!portOpenInitial);
            timerCallback?.Invoke();
            eventCount.Should().Be(2);
        }

        [Test]
        public void SecondTimerCallbackInvokesPortOpenChangedEventWhenNewPortIsSet([Values] bool portOpenState)
        {
            serialPortActivator.SetSerialPort(serialPort);
            serialPort.IsOpen.Returns(portOpenState);
            var eventCount = 0;
            serialPortActivator.PortOpenedStateChanged += () => eventCount++;
            timerCallback?.Invoke();
            eventCount.Should().Be(1);
            serialPortActivator.SetSerialPort(serialPort2);
            serialPort2.IsOpen.Returns(portOpenState);
            timerCallback?.Invoke();
            eventCount.Should().Be(2);
        }

        [Test]
        public void SecondTimerCallbackInvokesPortOpenChangedEventWhenNullPortIsSet([Values] bool portOpenState)
        {
            serialPortActivator.SetSerialPort(serialPort);
            serialPort.IsOpen.Returns(portOpenState);
            var eventCount = 0;
            serialPortActivator.PortOpenedStateChanged += () => eventCount++;
            timerCallback?.Invoke();
            eventCount.Should().Be(1);
            serialPortActivator.SetSerialPort(null);
            timerCallback?.Invoke();
            eventCount.Should().Be(2);
        }

        public void Dispose()
        {
            serialPort.Dispose();
            serialPort2.Dispose();
            serialPortActivator.Dispose();
        }
    }
}