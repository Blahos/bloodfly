﻿using BloodflyControl.ControllerInput;
using BloodflyControl.ControllerInput.Interfaces;
using FluentAssertions;
using FluentAssertions.Execution;
using NSubstitute;

namespace BloodflyControl.Tests
{
    public class PlaneControllerManagerTest : IDisposable
    {
        private readonly IControllerFactory controllerFactory = Substitute.For<IControllerFactory>();

        private readonly IControllerInputStateAggregatorFactory controllerInputStateAggregatorFactory =
            Substitute.For<IControllerInputStateAggregatorFactory>();

        private PlaneControllerManager planeControllerManager;

        private readonly IControllerId controller1Id = Substitute.For<IControllerId>();
        private readonly IControllerId controller2Id = Substitute.For<IControllerId>();
        private readonly IControllerId controller3Id = Substitute.For<IControllerId>();

        private readonly IController controller1 = Substitute.For<IController>();
        private readonly IController controller2 = Substitute.For<IController>();
        private readonly IController controller3 = Substitute.For<IController>();

        private readonly IControllerInputStateAggregator controllerInputStateAggregator1 =
            Substitute.For<IControllerInputStateAggregator>();

        private readonly IControllerInputStateAggregator controllerInputStateAggregator2 =
            Substitute.For<IControllerInputStateAggregator>();

        private readonly IControllerInputStateAggregator controllerInputStateAggregator3 =
            Substitute.For<IControllerInputStateAggregator>();

        [SetUp]
        public void Setup()
        {
            planeControllerManager = new PlaneControllerManager(
                controllerFactory,
                controllerInputStateAggregatorFactory);

            controllerFactory.CreateController(Arg.Is(controller1Id)).Returns(controller1);
            controllerFactory.CreateController(Arg.Is(controller2Id)).Returns(controller2);
            controllerFactory.CreateController(Arg.Is(controller3Id)).Returns(controller3);

            controllerInputStateAggregatorFactory.CreateControllerInputStateAggregator(Arg.Is(controller1))
                .Returns(controllerInputStateAggregator1);
            controllerInputStateAggregatorFactory.CreateControllerInputStateAggregator(Arg.Is(controller2))
                .Returns(controllerInputStateAggregator2);
            controllerInputStateAggregatorFactory.CreateControllerInputStateAggregator(Arg.Is(controller3))
                .Returns(controllerInputStateAggregator3);
        }

        [TearDown]
        public void TearDown()
        {
            controller1.Dispose();
            controller2.Dispose();
            controller3.Dispose();
        }

        [Test]
        public void NoConnectedControllersAfterCreation()
        {
            planeControllerManager.GetControllersConnectionStatus().All(a => a.Value).Should().Be(false);
        }

        [Test]
        public void NoAssignedControllersAfterCreation()
        {
            planeControllerManager.GetAssignedControllers().Where(a => a.Value != null).Should().BeEmpty();
        }

        [Test]
        public void AddingControllerCreatesNewControllerAndAggregator()
        {
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, controller1Id);
            controllerFactory.Received(1).CreateController(Arg.Is(controller1Id));
            controllerInputStateAggregatorFactory.Received(1).CreateControllerInputStateAggregator(Arg.Is(controller1));
        }

        [Test]
        public void AddingControllerAssignsItToGivenRole([Values] ControllerRole controllerRole)
        {
            planeControllerManager.SetControllerForRole(controllerRole, controller1Id);
            planeControllerManager.GetAssignedControllers()[controllerRole].Should().Be(controller1Id);
        }

        [Test]
        public void AddingNewControllerOverridesPreviouslyAssignedOne([Values] ControllerRole controllerRole)
        {
            planeControllerManager.SetControllerForRole(controllerRole, controller1Id);
            planeControllerManager.SetControllerForRole(controllerRole, controller2Id);
            planeControllerManager.GetAssignedControllers()[controllerRole].Should().Be(controller2Id);
        }

        [Test]
        public void AddingNullControllerToGivenRoleRemovesIt([Values] ControllerRole controllerRole)
        {
            planeControllerManager.SetControllerForRole(controllerRole, controller1Id);
            planeControllerManager.SetControllerForRole(controllerRole, null);
            planeControllerManager.GetAssignedControllers()[controllerRole].Should().BeNull();
        }

        [Test]
        public void AddingControllerRaisesControllersChangedEvent()
        {
            int eventRaisedCount = 0;
            planeControllerManager.ControllersChanged += () => eventRaisedCount++;
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, controller1Id);
            eventRaisedCount.Should().Be(1);
        }

        [Test]
        public void AddingControllerRaisesControllersChangedEventMultipleAdds()
        {
            int eventRaisedCount = 0;
            planeControllerManager.ControllersChanged += () => eventRaisedCount++;
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, controller1Id);
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, controller2Id);
            planeControllerManager.SetControllerForRole(ControllerRole.AlternativeFlying, controller3Id);
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, null);
            eventRaisedCount.Should().Be(4);
        }

        [Test]
        public void AddingSameControllerAgainDoesNotRaiseEvent()
        {
            int eventRaisedCount = 0;
            planeControllerManager.ControllersChanged += () => eventRaisedCount++;

            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, controller1Id);
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, controller1Id);
            planeControllerManager.SetControllerForRole(ControllerRole.AlternativeFlying, controller3Id);
            planeControllerManager.SetControllerForRole(ControllerRole.AlternativeFlying, controller3Id);
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, null);
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, null);

            eventRaisedCount.Should().Be(3);
        }

        [Test]
        public void ControllerIsConnectedChangedEventRaisesControllersChangedEvent()
        {
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, controller1Id);

            int eventRaisedCount = 0;
            planeControllerManager.ControllersChanged += () => eventRaisedCount++;

            controller1.IsConnectedChanged += Raise.Event<Action>();
            eventRaisedCount.Should().Be(1);

            controller1.IsConnectedChanged += Raise.Event<Action>();
            eventRaisedCount.Should().Be(2);
        }

        [Test]
        public void RemovedControllerIsConnectedChangedEventDoesNotRaiseControllersChangedEvent()
        {
            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, controller1Id);

            int eventRaisedCount = 0;
            planeControllerManager.ControllersChanged += () => eventRaisedCount++;

            controller1.IsConnectedChanged += Raise.Event<Action>();
            eventRaisedCount.Should().Be(1);

            planeControllerManager.SetControllerForRole(ControllerRole.PrimaryFlying, null);
            eventRaisedCount.Should().Be(2);

            controller1.IsConnectedChanged += Raise.Event<Action>();
            eventRaisedCount.Should().Be(2);
        }

        [Test]
        public void ReturnsDefaultInputForUnassignedControllerRole([Values] ControllerRole controllerRole)
        {
            var input = planeControllerManager.GetControllerInput(controllerRole);

            using (new AssertionScope())
            {
                input.ButtonsHeldDown.Should().Be(ControllerButton.None);
                input.ButtonsNewlyPressed.Should().Be(ControllerButton.None);
                input.Axes.All(a => a.Value == 0.0).Should().BeTrue();
                input.TimeElapsedSincePreviousInput.Should().Be(TimeSpan.Zero);
            }
        }

        [Test]
        public void ReturnsDefaultInputForAssignedAndThenUnassignedControllerRole(
            [Values] ControllerRole controllerRole)
        {
            planeControllerManager.SetControllerForRole(controllerRole, controller1Id);
            planeControllerManager.SetControllerForRole(controllerRole, null);
            var input = planeControllerManager.GetControllerInput(controllerRole);

            using (new AssertionScope())
            {
                input.ButtonsHeldDown.Should().Be(ControllerButton.None);
                input.ButtonsNewlyPressed.Should().Be(ControllerButton.None);
                input.Axes.All(a => a.Value == 0.0).Should().BeTrue();
                input.TimeElapsedSincePreviousInput.Should().Be(TimeSpan.Zero);
            }
        }

        [Test]
        public void UsesAppropriateAggregatorForGettingControllerInput([Values] ControllerRole controllerRole)
        {
            planeControllerManager.SetControllerForRole(controllerRole, controller3Id);
            planeControllerManager.GetControllerInput(controllerRole);

            using (new AssertionScope())
            {
                controllerInputStateAggregator1.Received(0).GetInput();
                controllerInputStateAggregator2.Received(0).GetInput();
                controllerInputStateAggregator3.Received(1).GetInput();
            }
        }

        [Test]
        public void ControllerInputIsNotRequestedDirectlyFromControllerWhenGettingInput(
            [Values] ControllerRole controllerRole)
        {
            planeControllerManager.SetControllerForRole(controllerRole, controller3Id);
            planeControllerManager.GetControllerInput(controllerRole);

            using (new AssertionScope())
            {
                controller1.Received(0).GetInputState();
                controller2.Received(0).GetInputState();
                controller3.Received(0).GetInputState();
            }
        }

        public void Dispose()
        {
            controller1.Dispose();
            controller2.Dispose();
            controller3.Dispose();
        }
    }
}