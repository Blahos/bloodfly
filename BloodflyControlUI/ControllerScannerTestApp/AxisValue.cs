﻿using BloodflyControl.ControllerInput;
using LibraryWpf;

namespace ControllerScannerTestApp
{
    public class AxisValue(ControllerAxis axis) : BaseViewModel
    {
        private double mValue = double.NaN;
        public ControllerAxis Axis { get; } = axis;

        public double Value
        {
            get { return mValue; }
            set { SetProperty(ref mValue, value); }
        }
    }
}