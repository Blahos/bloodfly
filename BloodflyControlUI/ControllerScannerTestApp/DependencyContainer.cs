﻿using BloodflyControl.ControllerInput.DInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.ControllerInput.XInput;
using BloodflyControl.Framework;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;
using BloodflyControl.ControllerInput;
using Castle.Windsor;
using Castle.MicroKernel.Registration;

namespace ControllerScannerTestApp
{
    internal static class DependencyContainer
    {
        private static readonly IWindsorContainer container = CreateContainer();

        public static T GetInstance<T>()
        {
            return container.Resolve<T>();
        }

        private static IWindsorContainer CreateContainer()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<ILogger>().ImplementedBy<ConsoleLogger>());
            container.Register(Component.For<ISerialTimerFactory>().ImplementedBy<SerialTimerFactory>());
            container.Register(Component.For<IDInputProvider>().ImplementedBy<DInputProvider>());
            container.Register(Component.For<IDInputJoystickButtonParser>()
                .ImplementedBy<DInputJoystickButtonParser>());
            container.Register(Component.For<IDInputJoystickAxesParser>().ImplementedBy<DInputJoystickAxesParser>());
            container.Register(Component.For<IXInputButtonConverter>().ImplementedBy<XInputButtonConverter>());
            container.Register(Component.For<IDInputJoystickWrapperFactory>()
                .ImplementedBy<DInputJoystickWrapperFactory>());
            container.Register(Component.For<IControllerFactory>().ImplementedBy<ControllerFactory>());
            return container;
        }
    }
}