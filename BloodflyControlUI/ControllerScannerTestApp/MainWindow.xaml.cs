﻿using BloodflyControl.ControllerInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace ControllerScannerTestApp
{
    public partial class MainWindow : Window
    {
        private readonly IControllerScanner controllerScanner;
        private readonly ISerialTimer pressedButtonsScanner;

        private readonly FrequencyCounter controllerUpdateFrequencyCounter =
            new FrequencyCounter(TimeSpan.FromSeconds(3));

        public List<AxisValue> AxesValues { get; } = Enum.GetValues<ControllerAxis>().Cast<ControllerAxis>()
            .Select(a => new AxisValue(a)).ToList();

        private IController? controller = null;

        public MainWindow()
        {
            InitializeComponent();

            controllerScanner = new ControllerScanner(DependencyContainer.GetInstance<ISerialTimerFactory>());
            controllerScanner.SkipDInputForXBoxGamepads = skipDInputForXBoxGamepadsCheckBox.IsChecked ?? false;

            controllerScanner.StateChanged += ControllerScanner_StateChanged;
            UpdateAvailableControllers();
            pressedButtonsScanner = DependencyContainer.GetInstance<ISerialTimerFactory>()
                .CreateSerialTimer(UpdateButtonsAndAxes,
                    TimeSpan.FromMilliseconds(stateReadTimerPeriodTextBox.Value ?? 20));
            stateReadTimerPeriodTextBox.Value = pressedButtonsScanner.Period.TotalMilliseconds;
            UpdateButtonsAndAxes();
        }

        private void ControllerScanner_StateChanged()
        {
            UpdateAvailableControllers();
        }

        private void UpdateAvailableControllers()
        {
            var controllers = controllerScanner.State;
            Dispatcher.Invoke(() =>
            {
                availableControllersListBox.Items.Clear();
                if (controllers == null) return;
                foreach (var controller in controllers.AvailableControllers)
                {
                    availableControllersListBox.Items.Add(controller);
                }
            });
        }

        private void availableControllersListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedController = availableControllersListBox.SelectedItem as IControllerId;

            if (controller != null)
            {
                controller.IsConnectedChanged -= UpdateControllerStatus;
            }

            controller?.Dispose();

            if (selectedController == null) return;

            controller = DependencyContainer.GetInstance<IControllerFactory>().CreateController(selectedController);
            controller.IsConnectedChanged += UpdateControllerStatus;
            UpdateControllerStatus();
            activeControllerTextBlock.Text = selectedController.Description;
        }

        private void UpdateControllerStatus()
        {
            var connectedColor = Colors.Green;
            var disconnectedColor = Colors.Red;
            var controllerState = controller?.IsConnected ?? false;

            Dispatcher.Invoke(() =>
            {
                activeControllerTextBlock.Background =
                    new SolidColorBrush(controllerState ? connectedColor : disconnectedColor);
            });
        }

        private void UpdateButtonsAndAxes()
        {
            controllerUpdateFrequencyCounter.AddOccurance();
            var updateFrequency = controllerUpdateFrequencyCounter.GetFrequency();
            var inputState = controller?.GetInputState();
            Dispatcher.BeginInvoke(() =>
            {
                updateFrequencyTextBlock.Text = updateFrequency.ToString("F1", CultureInfo.InvariantCulture);
                pressedButtonsTextBlock.Text = inputState?.Buttons.ToString() ?? "";

                if (inputState == null) return;
                foreach (var axis in EnumValuesProvider.ControllerAxes)
                {
                    foreach (var axisEntry in AxesValues.Where(a => a.Axis == axis))
                    {
                        axisEntry.Value = inputState.Value.Axes[axis];
                    }
                }
            });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            controller?.Dispose();
            pressedButtonsScanner?.Terminate(true);
            controllerScanner?.Dispose();
        }

        private void skipDInputForXBoxGamepadsCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            controllerScanner.SkipDInputForXBoxGamepads = skipDInputForXBoxGamepadsCheckBox.IsChecked ?? false;
        }

        private void DoubleUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var timer = pressedButtonsScanner;
            var value = stateReadTimerPeriodTextBox.Value;
            if (timer == null) return;
            if (value == null) return;

            timer.Period = TimeSpan.FromMilliseconds((double)value);
        }
    }
}