﻿using BloodflyControl.PlaneControl.Interfaces;

namespace ThrottleLockTestApp
{
    internal class DummyThrottleSafetySettings : IThrottleSafetySettings
    {
        public double GetSafeThrottleValue()
        {
            return 0.0;
        }

        public bool IsThrottleValueSafe(double throttle)
        {
            return Math.Abs(throttle) < 1e-10;
        }
    }
}