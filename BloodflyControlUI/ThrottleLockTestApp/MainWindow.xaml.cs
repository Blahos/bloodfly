﻿using BloodflyControl.ControllerInput;
using BloodflyControl.ControllerInput.Interfaces;
using BloodflyControl.Framework;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.PlaneControl;
using BloodflyControl.PlaneControl.Interfaces;
using BloodflyControl.SerialComm;
using BloodflyControl.SerialComm.Interfaces;
using System.Windows;
using System.Windows.Media;

namespace ThrottleLockTestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    internal partial class MainWindow : Window, IPlaneControllerProvider, ISerialPortStateProvider,
        ITargetPlaneAxesValuesProvider, IControlActionSettingsProvider
    {
        private readonly ITaskRunner taskRunner = new TaskRunner();
        private readonly ThrottleLockManager throttleLockManager;
        private readonly IThrottleSafetyConditionsProvider throttleSafetyConditionsProvider;

        public SerialPortParameters? PortParameters => Dispatcher.Invoke(() =>
            new SerialPortParameters("COM" + serialPortNumberTextBox.Value.ToString(), 38400));

        public bool PortOpen => Dispatcher.Invoke(() => serialPortIsOpenCheckBox.IsChecked ?? false);

        public event Action? ControllersChanged;
        public event Action? PortStateChanged;
        public event Action? ValuesChanged;

        public MainWindow()
        {
            InitializeComponent();

            throttleSafetyConditionsProvider = new ThrottleSafetyConditionsProvider
                (this, this, new DummyThrottleSafetySettings(), this, this);

            throttleLockManager = new ThrottleLockManager(
                throttleSafetyConditionsProvider,
                taskRunner);

            throttleLockManager.ThrottleLockedStateChanged += ThrottleLockManager_ThrottleLockedStateChanged;
            UpdateLockIndicator();
        }

        private void ThrottleLockManager_ThrottleLockedStateChanged()
        {
            Console.WriteLine("Throttle locked state changed");
            Dispatcher.BeginInvoke(UpdateLockIndicator);
        }

        private void LockButton_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Lock requested");
            throttleLockManager.LockThrottle();
        }

        private void UnlockButton_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Unlock requested");
            throttleLockManager.RequestThrottleUnlock();
        }

        private void UpdateLockIndicator()
        {
            stateIndicatorRectangle.Fill = GetLockIndicatorColor(throttleLockManager.IsThrottleLocked);
        }

        private static SolidColorBrush GetLockIndicatorColor(bool lockState)
        {
            return new SolidColorBrush(lockState ? Colors.Red : Colors.Green);
        }

        public ControllerRolesValues<IControllerId?> GetAssignedControllers()
        {
            throw new NotImplementedException("This method should not be called in this test");
        }

        public ControllerRolesValues<bool> GetControllersConnectionStatus()
        {
            return Dispatcher.Invoke(() =>
            {
                var result = new ControllerRolesValues<bool>
                {
                    [ControllerRole.PrimaryFlying] = primaryFlyingControllerConnectedCheckBox.IsChecked ?? false,
                    [ControllerRole.AlternativeFlying] =
                        alternativeFlyingControllerConnectedCheckBox.IsChecked ?? false,
                    [ControllerRole.Assisting] = assistingControllerConnectedCheckBox.IsChecked ?? false
                };
                return result;
            });
        }

        public ControllerInputStateEx GetControllerInput(ControllerRole role)
        {
            throw new NotImplementedException();
        }

        private double GetDesiredThrottle()
        {
            return Dispatcher.Invoke(() => desiredThrottleSlider.Value);
        }

        private void controllerConnectedCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            ControllersChanged?.Invoke();
        }

        private void serialPortNumberTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            PortStateChanged?.Invoke();
        }

        private void serialPortIsOpenCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            PortStateChanged?.Invoke();
        }

        public PlaneAxesValues<double> GetValues()
        {
            var values = new PlaneAxesValues<double>
            {
                [PlaneAxis.Throttle] = GetDesiredThrottle()
            };

            return values;
        }

        public double GetValue(PlaneAxis axis)
        {
            if (axis == PlaneAxis.Throttle)
            {
                return GetDesiredThrottle();
            }

            return 0;
        }

        private void desiredThrottleSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ValuesChanged?.Invoke();
        }

        public event Action? SettingsChanged;

        public ControlActionSettings? GetActionSettings(ControlAction action)
        {
            return Dispatcher.Invoke(() =>
            {
                if (action == ControlAction.SwitchThrottleLock
                    &&
                    (throttleSwitchSettingsCheckBox.IsChecked ?? false))
                {
                    return new ControlActionSettings();
                }

                return (ControlActionSettings?)null;
            });
        }

        public Dictionary<ControlAction, ControlActionSettings> GetActionsSettings()
        {
            return new()
            {
                { ControlAction.SwitchThrottleLock, new ControlActionSettings() }
            };
        }

        private void ThrottleSwitchSettingsCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            SettingsChanged?.Invoke();
        }

        private void ThrottleSwitchSettingsCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            SettingsChanged?.Invoke();
        }

        private void TriggerControlActionSettingsChangedButton_OnClick(object sender, RoutedEventArgs e)
        {
            SettingsChanged?.Invoke();
        }
    }
}