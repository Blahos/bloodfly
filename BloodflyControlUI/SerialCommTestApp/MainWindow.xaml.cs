﻿using BloodflyControl.Logging;
using BloodflyControl.SerialComm;
using BloodflyControl.SerialComm.Interfaces;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace SerialCommTestApp
{
    public partial class MainWindow : Window
    {
        private readonly ILogger logger = DependencyContainer.GetInstance<ILogger>();
        private readonly ISerialPortScanner serialPortScanner = DependencyContainer.GetInstance<ISerialPortScanner>();

        private readonly ISerialPortStateProvider serialPortStateProvider =
            DependencyContainer.GetInstance<ISerialPortStateProvider>();

        private readonly ISerialPortReader serialPortReader = DependencyContainer.GetInstance<ISerialPortReader>();
        private readonly ISerialPortWriter serialPortWriter = DependencyContainer.GetInstance<ISerialPortWriter>();

        private readonly ISerialPortConnectorSettings serialPortConnectorSettings =
            DependencyContainer.GetInstance<ISerialPortConnectorSettings>();

        private readonly int baudRate = 38400;

        public MainWindow()
        {
            InitializeComponent();

            serialPortScanner.StateChanged += SerialPortScanner_AvailablePortsChanged;
            UpdateAvailablePorts();

            serialPortStateProvider.PortStateChanged += SerialPortManager_PortStateChanged;
            MarkActivePortOpenStatus(serialPortStateProvider.PortOpen);
        }

        private void SerialPortManager_PortStateChanged()
        {
            MarkActivePortOpenStatus(serialPortStateProvider.PortOpen);
        }

        private void SerialPortScanner_AvailablePortsChanged()
        {
            UpdateAvailablePorts();
        }

        private void UpdateAvailablePorts()
        {
            var ports = serialPortScanner.State.Ports;
            Dispatcher.Invoke(() =>
            {
                availablePortsListBox.Items.Clear();
                foreach (var port in ports)
                {
                    availablePortsListBox.Items.Add(port);
                }
            });
        }

        private void availablePortsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (availablePortsListBox.SelectedItem is string selectedPort)
            {
                ActivatePort(new(selectedPort, baudRate));
            }
        }

        private void Send_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var message = "Test message";
                logger.LogMessage($"Writing message: {message}", LogLevel.Info);
                serialPortWriter.Write(message);
            }
            catch (Exception ex)
            {
                logger.LogMessage($"Failed to write to port {serialPortStateProvider.PortParameters}: {ex}",
                    LogLevel.Error);
            }
        }

        private void Read_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var message = serialPortReader.Read();
                logger.LogMessage($"Read message from port {serialPortStateProvider.PortParameters}: {message}",
                    LogLevel.Info);
            }
            catch (Exception ex)
            {
                logger.LogMessage($"Failed to read from port {serialPortStateProvider.PortParameters}: {ex}",
                    LogLevel.Error);
            }
        }

        private void MarkActivePortOpenStatus(bool isOpen)
        {
            Dispatcher.Invoke(() =>
            {
                activePortTextBlock.Background = new SolidColorBrush(isOpen ? Colors.Green : Colors.Transparent);
            });
        }

        private void SetActivePortParameters(SerialPortParameters? serialPortParameters)
        {
            Dispatcher.Invoke(() => { activePortTextBlock.Text = serialPortParameters?.ToString(); });
        }

        private void Deactivate_Click(object sender, RoutedEventArgs e)
        {
            DeactivateActivePort();
        }

        private void DeactivateActivePort()
        {
            ActivatePort(null);
        }

        private void ActivatePort(SerialPortParameters? serialPortParameters)
        {
            SetActivePortParameters(serialPortParameters);
            serialPortConnectorSettings.SerialPortParameters = serialPortParameters;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DeactivateActivePort();
            serialPortScanner.StateChanged -= SerialPortScanner_AvailablePortsChanged;
            serialPortStateProvider.PortStateChanged -= SerialPortManager_PortStateChanged;
            serialPortScanner.Dispose();
        }
    }
}