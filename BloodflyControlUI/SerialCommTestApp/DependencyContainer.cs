﻿using BloodflyControl.Framework;
using BloodflyControl.Framework.Interfaces;
using BloodflyControl.Logging;
using BloodflyControl.SerialComm;
using BloodflyControl.SerialComm.Interfaces;
using Castle.Windsor;
using Castle.MicroKernel.Registration;

namespace SerialCommTestApp
{
    internal static class DependencyContainer
    {
        private static readonly IWindsorContainer container = BuildContainer();

        public static T GetInstance<T>()
        {
            return container.Resolve<T>();
        }

        private static IWindsorContainer BuildContainer()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<ILogger>().ImplementedBy<ConsoleLogger>());
            container.Register(Component.For<ISerialTimerFactory>().ImplementedBy<SerialTimerFactory>());
            container.Register(Component.For<ISerialPortFactory>().ImplementedBy<SerialPortFactory>());
            container.Register(Component.For<ISerialPortActivatorFactory>()
                .ImplementedBy<SerialPortActivatorFactory>());
            container.Register(Component.For<ISerialPortConnectorSettings>()
                .ImplementedBy<SerialPortConnectorSettings>());
            container.Register(Component.For<ISerialPortScanner>().ImplementedBy<SerialPortScanner>());
            container.Register(Component.For<ISerialPortProvider, ISerialPortStateProvider>()
                .ImplementedBy<SerialPortConnector>());
            container.Register(Component.For<ISerialPortWriter, ISerialPortReader>()
                .ImplementedBy<SerialPortCommunicator>());
            return container;
        }
    }
}